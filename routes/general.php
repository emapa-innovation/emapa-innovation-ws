<?php

$router->get('/', function () {
    return 'EMAPA EP Daule';
});

$router->group(['middleware' => 'auth'], function () use ($router){
    $router->group(['prefix' => 'api/v1/emapa'], function () use ($router){
        $router->group(['prefix' => 'general', 'namespace' => 'General'], function () use ($router){
            // CICLOS
            $router->group(['prefix' => 'ciclos'], function () use ($router){
                $router->get('index', 'CiclosController@index');
                $router->post('listar', 'CiclosController@listar');
                $router->post('guardar', 'CiclosController@guardar');
            });
            // CAUSAS NO LECTURA
            $router->group(['prefix' => 'causa-no-lectura'], function () use ($router){
                $router->get('index', 'CausaNoLecturaController@index');
                $router->post('listar', 'CausaNoLecturaController@listar');
                $router->post('guardar', 'CausaNoLecturaController@guardar');
            });
            // SECTORES
            $router->group(['prefix' => 'sector'], function () use ($router){
                $router->get('index', 'SectorController@index');
                $router->post('listar', 'SectorController@listar');
                $router->post('guardar', 'SectorController@guardar');
            });
        });
    });
}); // Cierra middleware auth
