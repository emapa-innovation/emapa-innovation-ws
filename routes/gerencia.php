<?php

$router->group(['middleware' => 'auth'], function () use ($router){
    $router->group(['prefix' => 'api/v1/emapa'], function () use ($router){
        $router->group(['prefix' => 'gerencia', 'namespace' => 'Gerencia'], function () use ($router){
            $router->group(['prefix' => 'reportes', 'namespace' => 'Reportes'], function () use ($router){
                // RECAUDACION DE FACTURAS
                $router->group(['prefix' => 'recaudacion-factura'], function () use ($router){
                    $router->get('index', 'RecaudacionFacturaController@index');
                    $router->post('listar', 'RecaudacionFacturaController@listar');
                    $router->post('exportar', 'RecaudacionFacturaController@exportar');
                });
                // AUDITORIA DE ACCIONES
                $router->group(['prefix' => 'auditoria-acciones'], function () use ($router){
                    $router->get('index', 'AuditoriaAccionesController@index');
                    $router->post('listar', 'AuditoriaAccionesController@listar');
                    $router->post('exportar', 'AuditoriaAccionesController@exportar');
                });
            });
        });
    });
}); // Cierra middleware auth
