<?php

$router->group(['prefix' => 'api/v1/emapa'], function () use ($router){
    $router->group(['prefix' => 'seguridad', 'namespace' => 'Seguridad'], function () use ($router){
        // LOGIN
        $router->post('login', 'LoginController@login');
        $router->get('logout', 'LoginController@logout');
        // SERVICIOS PARA ENTIDADES EXTERNAS
        $router->group(['prefix' => 'servicios-externos'], function () use ($router){
            // SERVICIOS PARA BANRED
            $router->group(['prefix' => 'banred'], function () use ($router){
                $router->post('login', 'LoginController@loginBanred');
            });
            // SERVICIOS PARA SERVIPAGOS
            $router->group(['prefix' => 'servipagos'], function () use ($router){
                $router->post('login', 'LoginController@loginServipagos');
            });
            // SERVICIOS PARA FACILITO
            $router->group(['prefix' => 'facilito'], function () use ($router){
                $router->post('login', 'LoginController@loginFacilito');
            });
            // SERVICIOS PARA SOISEM
            $router->group(['prefix' => 'soisem'], function () use ($router){
                $router->post('login', 'LoginController@loginSoisem');
            });
        });
    });
});

$router->group(['middleware' => 'auth'], function () use ($router){
    $router->group(['prefix' => 'api/v1/emapa'], function () use ($router){
        $router->group(['prefix' => 'seguridad', 'namespace' => 'Seguridad'], function () use ($router){
            // PARAMETROS
            $router->group(['prefix' => 'parametros', 'namespace' => 'Parametros'], function () use ($router){
                // BANCOS
                $router->group(['prefix' => 'bancos'], function () use ($router){
                    $router->get('index', 'BancosController@index');
                    $router->post('listar', 'BancosController@listar');
                    $router->post('guardar', 'BancosController@guardar');
                });
                // CONTRATOS Y SERVICIOS
                $router->group(['prefix' => 'contratos-servicios'], function () use ($router){
                    $router->get('index', 'ContratoServicioController@index');
                    $router->post('listar', 'ContratoServicioController@listar');
                    $router->post('guardar', 'ContratoServicioController@guardar');
                });
                // TABLA DE VALORES
                $router->group(['prefix' => 'tabla-valores'], function () use ($router){
                    $router->get('index', 'TablaValoresController@index');
                    $router->post('listar', 'TablaValoresController@listar');
                    $router->post('guardar', 'TablaValoresController@guardar');
                });
                // TARJETA DE CREDITO
                $router->group(['prefix' => 'tarjeta-credito'], function () use ($router){
                    $router->get('index', 'TarjetaCreditoController@index');
                    $router->post('listar', 'TarjetaCreditoController@listar');
                    $router->post('guardar', 'TarjetaCreditoController@guardar');
                });
                // ITEMS
                $router->group(['prefix' => 'items'], function () use ($router){
                    $router->get('index', 'ItemsController@index');
                    $router->post('listar', 'ItemsController@listar');
                    $router->post('guardar', 'ItemsController@guardar');
                });
                // TIPO DE RECLAMOS
                $router->group(['prefix' => 'tipo-reclamos'], function () use ($router){
                    $router->get('index', 'TipoReclamosController@index');
                    $router->post('listar', 'TipoReclamosController@listar');
                    $router->post('guardar', 'TipoReclamosController@guardar');
                });
                // RANGO ESTADISTICA CONSUMO
                $router->group(['prefix' => 'rango-estadistica'], function () use ($router){
                    $router->get('index', 'RangoEstadisticaController@index');
                    $router->post('listar', 'RangoEstadisticaController@listar');
                    $router->post('guardar', 'RangoEstadisticaController@guardar');
                });
                // RANGO ESTADISTICA CONSUMO
                $router->group(['prefix' => 'motivo-multas'], function () use ($router){
                    $router->get('index', 'MotivoMultasController@index');
                    $router->post('listar', 'MotivoMultasController@listar');
                    $router->post('guardar', 'MotivoMultasController@guardar');
                });

                // CALIFICACIÓN DEL CLIENTE
                $router->group(['prefix' => 'calificacion-cliente'], function () use ($router){
                    $router->get('index', 'CalificacionClienteController@index');
                    $router->post('listar', 'CalificacionClienteController@listar');
                    $router->post('guardar', 'CalificacionClienteController@guardar');
                });
                // CLASE DE CLIENTE
                $router->group(['prefix' => 'clase-cliente'], function () use ($router){
                    $router->get('index', 'ClaseClienteController@index');
                    $router->post('listar', 'ClaseClienteController@listar');
                    $router->post('guardar', 'ClaseClienteController@guardar');
                });
                // TIPO DE CLIENTE
                $router->group(['prefix' => 'tipo-cliente'], function () use ($router){
                    $router->get('index', 'TipoClienteController@index');
                    $router->post('listar', 'TipoClienteController@listar');
                    $router->post('guardar', 'TipoClienteController@guardar');
                });
                // TIPO DE RECAUDACION
                $router->group(['prefix' => 'tipo-recaudacion'], function () use ($router){
                    $router->get('index', 'TipoRecaudacionController@index');
                    $router->post('listar', 'TipoRecaudacionController@listar');
                    $router->post('guardar', 'TipoRecaudacionController@guardar');
                });
                // TIPO DE INSTALACION
                $router->group(['prefix' => 'tipo-instalacion'], function () use ($router){
                    $router->get('index', 'TipoInstalacionController@index');
                    $router->post('listar', 'TipoInstalacionController@listar');
                    $router->post('guardar', 'TipoInstalacionController@guardar');
                });
            });

            // USUARIOS
            $router->group(['prefix' => 'usuario'], function () use ($router){
                $router->get('index', 'UsuarioController@index');
                $router->post('listar', 'UsuarioController@listar');
                $router->post('guardar', 'UsuarioController@guardar');
            });
            // PERFIL DE USUARIO
            $router->group(['prefix' => 'perfil-usuario'], function () use ($router){
                $router->get('index', 'PerfilUsuarioController@index');
                $router->post('listar', 'PerfilUsuarioController@listar');
                $router->post('guardar', 'PerfilUsuarioController@guardar');
            });
            // UTILIDADES O FUNCIONES PARA USO GENERAL EN EL APLICATIVO
            $router->group(['prefix' => 'utilities'], function () use ($router){
                // GET FILE STORED IN STORAGE FIELD
                $router->get('getFile/{urlFile}', 'UtilitiesController@getFile');
                $router->get('getFileBase64/{urlFile}', 'UtilitiesController@getFileBase64');
                $router->get('getFileURL/{urlFile}', 'UtilitiesController@getFileURL');
            });
        });
    });
}); // Cierra middleware auth