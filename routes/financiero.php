<?php

$router->group(['middleware' => 'auth'], function () use ($router){
    $router->group(['prefix' => 'api/v1/emapa'], function () use ($router){
        $router->group(['prefix' => 'financiero', 'namespace' => 'Financiero'], function () use ($router){
            // RECAUDACION DE CLIENTES
            $router->group(['prefix' => 'recaudacion-cliente'], function () use ($router){
                $router->get('index', 'RecaudacionClienteController@index');
                $router->post('listar', 'RecaudacionClienteController@listar');
                $router->post('guardar', 'RecaudacionClienteController@guardar');
                $router->post('exportar', 'RecaudacionClienteController@exportar');
            });
        });
    });
}); // Cierra middleware auth
