<?php

// CONSUMO DE APLICACIONES EXTERNAS
$router->group(['prefix' => 'api/v1/emapa'], function () use ($router){
    // BALCON DE SERVICIOS
    $router->group(['prefix' => 'balcon-servicios', 'namespace' => 'BalconServicios'], function () use ($router){
        // CONSULTA DE PLANILLAS
        $router->group(['prefix' => 'consulta-planilla'], function () use ($router){
            $router->get('index', 'ConsultaPlanillaController@index');
            $router->post('listar', 'ConsultaPlanillaController@listar');
            $router->post('exportar', 'ConsultaPlanillaController@exportar');
        });

        // REPORTE DE DAÑOS Y FUGAS
        $router->group(['prefix' => 'reporte-fugas'], function () use ($router){
            $router->get('index', 'ReporteFugasController@index');
            $router->post('listar', 'ReporteFugasController@listar');
            $router->post('guardar', 'ReporteFugasController@guardar');
        });

        // ACTUALIZACION DE DATOS DE INSTALACION
        $router->group(['prefix' => 'actualizacion-datos'], function () use ($router){
            $router->get('index', 'ActualizacionDatosController@index');
            $router->post('listar', 'ActualizacionDatosController@listar');
            $router->post('guardar', 'ActualizacionDatosController@guardar');
        });
    });
});
