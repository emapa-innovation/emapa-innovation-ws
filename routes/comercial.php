<?php

$router->group(['middleware' => 'auth'], function () use ($router){
    $router->group(['prefix' => 'api/v1/emapa'], function () use ($router){
        $router->group(['prefix' => 'comercial', 'namespace' => 'Comercial'], function () use ($router){
            // NOTIFICACION DE PAGOS
            $router->group(['prefix' => 'notificacion-pagos'], function () use ($router){
                $router->get('index', 'NotificacionPagosController@index');
                $router->post('listar', 'NotificacionPagosController@listar');
                $router->post('guardar', 'NotificacionPagosController@guardar');
                $router->post('exportar', 'NotificacionPagosController@exportar');
            });

            // DEPURACION DE CARTERA
            $router->group(['prefix' => 'depuracion-cartera'], function () use ($router){
                $router->get('index', 'DepuracionCarteraController@index');
                $router->post('listar', 'DepuracionCarteraController@listar');
                $router->post('guardar', 'DepuracionCarteraController@guardar');
                $router->post('exportar', 'DepuracionCarteraController@exportar');
            });

            // REESTRUCTURACION DE INTERESES
            $router->group(['prefix' => 'reestructuracion-intereses'], function () use ($router){
                $router->get('index', 'ReestructuracionInteresesController@index');
                $router->post('listar', 'ReestructuracionInteresesController@listar');
                $router->post('guardar', 'ReestructuracionInteresesController@guardar');
                $router->post('exportar', 'ReestructuracionInteresesController@exportar');
            });

            // LECTURAS DE CONSUMO
            $router->group(['prefix' => 'lecturas-consumo'], function () use ($router){
                $router->get('index', 'LecturasConsumoController@index');
                $router->post('listar', 'LecturasConsumoController@listar');
                $router->post('guardar', 'LecturasConsumoController@guardar');
                $router->post('exportar', 'LecturasConsumoController@exportar');
            });

            // MICROMEDIDORES
            $router->group(['prefix' => 'micromedidor'], function () use ($router){
                $router->get('index', 'MicromedidorController@index');
                $router->post('listar', 'MicromedidorController@listar');
                $router->post('guardar', 'MicromedidorController@guardar');
                $router->post('exportar', 'MicromedidorController@exportar');
            });

            // CONEXIONES DE ALCANTARILLADO
            $router->group(['prefix' => 'conexion-alcantarillado'], function () use ($router){
                $router->get('index', 'ConexionAlcantarilladoController@index');
                $router->post('listar', 'ConexionAlcantarilladoController@listar');
                $router->post('guardar', 'ConexionAlcantarilladoController@guardar');
                $router->post('exportar', 'ConexionAlcantarilladoController@exportar');
            });

            // CARTERA
            $router->group(['prefix' => 'cartera'], function () use ($router){
                $router->get('index', 'CarteraController@index');
                $router->post('listar', 'CarteraController@listar');
                $router->post('guardar', 'CarteraController@guardar');
                $router->post('exportar', 'CarteraController@exportar');
            });
            
            // CLIENTES
            $router->group(['prefix' => 'cliente'], function () use ($router){
                $router->get('index', 'ClienteController@index');
                $router->post('listar', 'ClienteController@listar');
                $router->post('guardar', 'ClienteController@guardar');
            });

            // INSTALACION
            $router->group(['prefix' => 'instalacion'], function () use ($router){
                $router->get('index', 'InstalacionController@index');
                $router->post('listar', 'InstalacionController@listar');
                $router->post('guardar', 'InstalacionController@guardar');
            });

            // FACTURAS
            $router->group(['prefix' => 'factura'], function () use ($router){
                $router->get('index', 'FacturaController@index');
                $router->post('listar', 'FacturaController@listar');
                $router->post('listarDetalle', 'FacturaController@listarDetalle');
                $router->post('guardar', 'FacturaController@guardar');
            });
            
            // DOCUMENTOS ELECTRONICOS
            $router->group(['prefix' => 'documento-electronico'], function () use ($router){
                $router->get('index', 'DocumentoElectronicoController@index');
                $router->post('listar', 'DocumentoElectronicoController@listar');
                $router->post('enviarEmailRide', 'DocumentoElectronicoController@enviarEmailRide');
                $router->post('enviarEmailRideXLote', 'DocumentoElectronicoController@enviarEmailRideXLote');
            });

            // CORTES Y RECONEXIONES
            $router->group(['prefix' => 'corte-reconexion'], function () use ($router){
                $router->get('index', 'CorteReconexionController@index');
                $router->post('listar', 'CorteReconexionController@listar');
                $router->post('guardar', 'CorteReconexionController@guardar');
                $router->post('importar', 'CorteReconexionController@importar');
                $router->post('exportar', 'CorteReconexionController@exportar');
            });

            // RECAUDACION EXTERNA DE ENTIDADES FINANCIERAS
            $router->group(['prefix' => 'recaudacion-externa'], function () use ($router){
                $router->get('index', 'RecaudacionExternaController@index');
                $router->post('listar', 'RecaudacionExternaController@listar');
                $router->post('exportar', 'RecaudacionExternaController@exportar');
                $router->post('importar', 'RecaudacionExternaController@importar');
            });

            // REPORTES
            $router->group(['prefix' => 'reportes', 'namespace' => 'Reportes'], function () use ($router){
                // CUENTAS CLIENTE
                $router->group(['prefix' => 'cuentas-cliente'], function () use ($router){
                    $router->get('index', 'CuentasClienteController@index');
                    $router->post('listar', 'CuentasClienteController@listar');
                    $router->post('exportar', 'CuentasClienteController@exportar');
                });
                // COMPROMISOS DE PAGO
                $router->group(['prefix' => 'compromiso-pago'], function () use ($router){
                    $router->get('index', 'CompromisoPagoController@index');
                    $router->post('listar', 'CompromisoPagoController@listar');
                    $router->post('exportar', 'CompromisoPagoController@exportar');
                });
                // CARTERA Y COBRANZA - RECAUDACION DE CARTERA
                $router->group(['prefix' => 'recaudacion-cartera'], function () use ($router){
                    $router->get('index', 'RecaudacionCarteraController@index');
                    $router->post('listar', 'RecaudacionCarteraController@listar');
                    $router->post('exportar', 'RecaudacionCarteraController@exportar');
                });
            });

            // SERVICIOS PARA ENTIDADES EXTERNAS
            $router->group(['prefix' => 'servicios-externos'], function () use ($router){
                // SERVICIOS PARA BANRED
                $router->group(['prefix' => 'banred'], function () use ($router){
                    $router->get('index', 'ServiciosBanredController@index');
                    $router->post('consulta', 'ServiciosBanredController@consulta');
                    $router->post('pago', 'ServiciosBanredController@pago');
                    $router->post('reverso', 'ServiciosBanredController@reverso');
                });
                // SERVICIOS PARA SERVIPAGOS
                $router->group(['prefix' => 'servipagos'], function () use ($router){
                    $router->get('index', 'ServiciosServipagosController@index');
                    $router->post('consulta', 'ServiciosServipagosController@consulta');
                    $router->post('pago', 'ServiciosServipagosController@pago');
                    $router->post('reverso', 'ServiciosServipagosController@reverso');
                });
                // SERVICIOS PARA FACILITO
                $router->group(['prefix' => 'facilito'], function () use ($router){
                    $router->get('index', 'ServiciosFacilitoController@index');
                    $router->post('consulta', 'ServiciosFacilitoController@consulta');
                    $router->post('pago', 'ServiciosFacilitoController@pago');
                    $router->post('reverso', 'ServiciosFacilitoController@reverso');
                });
                // SERVICIOS PARA SOISEM
                $router->group(['prefix' => 'soisem'], function () use ($router){
                    $router->get('index', 'ServiciosSoisemController@index');
                    $router->post('consulta-cortes', 'ServiciosSoisemController@consultaCortes');
                    $router->post('consulta-reconexiones', 'ServiciosSoisemController@consultaReconexiones');
                    $router->post('corte', 'ServiciosSoisemController@corte');
                    $router->post('reconexion', 'ServiciosSoisemController@reconexion');
                    $router->post('consulta-lecturas', 'ServiciosSoisemController@consultaLecturas');
                    $router->post('ingreso-lectura', 'ServiciosSoisemController@ingresoLectura');
                });
            });
        });
    });
}); // Cierra middleware auth
