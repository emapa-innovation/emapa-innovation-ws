<?php

namespace App\Http\Repositories\Financiero;

use Illuminate\Support\Facades\DB;

class RecaudacionClienteRepository
{
	private $opcion, $idRecaudacion, $idCabFactura, $idUsuario, $idAgencia, $numeroRecaudacion, $tipoPago; 
	private $estadoPago, $clasePago, $facturaFisica, $fechaDesde, $fechaHasta, $instalacion, $idCiclo;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       		= $data['opcion']       		?? NULL;
			$this->idRecaudacion   		= $data['idRecaudacion']   		?? NULL;
			$this->idCabFactura   		= $data['idCabFactura']   		?? NULL;
			$this->idUsuario   			= $data['idUsuario']   			?? NULL;
			$this->idAgencia   			= $data['idAgencia']   			?? NULL;
			$this->numeroRecaudacion 	= $data['numeroRecaudacion'] 	?? NULL;
			$this->tipoPago   			= $data['tipoPago']   			?? NULL;
			$this->estadoPago   		= $data['estadoPago']   		?? NULL;
			$this->clasePago   			= $data['clasePago']   			?? NULL;
			$this->facturaFisica   		= $data['facturaFisica']   		?? NULL;
			$this->fechaDesde   		= $data['fechaDesde']   		?? NULL;
			$this->fechaHasta  			= $data['fechaHasta']  			?? NULL;
			$this->instalacion  		= $data['instalacion']  		?? NULL;
			$this->idCiclo  			= $data['idCiclo']  			?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_ListarRecaudacionCliente(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idRecaudacion,
				$this->idCabFactura,
				$this->idUsuario,
				$this->idAgencia,
				$this->numeroRecaudacion,
				$this->tipoPago,
				$this->estadoPago,
				$this->clasePago,
				$this->facturaFisica,
				$this->fechaDesde,
				$this->fechaHasta,
				$this->instalacion,
				$this->idCiclo,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarRecaudacionCliente(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idRecaudacion,
				$this->idCabFactura,
				$this->idUsuario,
				$this->idAgencia,
				$this->numeroRecaudacion,
				$this->tipoPago,
				$this->estadoPago,
				$this->clasePago,
				$this->facturaFisica,
				$this->fechaDesde,
				$this->fechaHasta,
				$this->instalacion,
				$this->idCiclo,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
