<?php

namespace App\Http\Repositories\General;

use Illuminate\Support\Facades\DB;

class SectorRepository
{
    private $opcion, $idSector, $idCiclo, $codigo, $descripcion, $estado;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       = $data['opcion']       ?? NULL;
			$this->idSector 	= $data['idSector'] 	?? NULL;
			$this->idCiclo 		= $data['idCiclo'] 		?? NULL;
			$this->codigo   	= $data['codigo']   	?? NULL;
			$this->descripcion  = $data['descripcion']  ?? NULL;
			$this->estado   	= $data['estado']   	?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_ListarSector(?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idSector,
				$this->idCiclo,
				$this->codigo,
				$this->descripcion,
				$this->estado
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarSector(?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idSector,
				$this->idCiclo,
				$this->codigo,
				$this->descripcion,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
