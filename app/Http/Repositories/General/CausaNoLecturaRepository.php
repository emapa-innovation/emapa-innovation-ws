<?php

namespace App\Http\Repositories\General;

use Illuminate\Support\Facades\DB;

class CausaNoLecturaRepository
{
    private $opcion, $idCausaNoLectura, $codigo, $descripcion, $estado;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       	= $data['opcion']       	?? NULL;
			$this->idCausaNoLectura = $data['idCausaNoLectura'] ?? NULL;
			$this->codigo   		= $data['codigo']   		?? NULL;
			$this->descripcion  	= $data['descripcion']  	?? NULL;
			$this->estado   		= $data['estado']   		?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_ListarCausaNoLectura(?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idCausaNoLectura,
				$this->codigo,
				$this->descripcion,
				$this->estado
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarCausaNoLectura(?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idCausaNoLectura,
				$this->codigo,
				$this->descripcion,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
