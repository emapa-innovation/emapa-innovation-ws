<?php

namespace App\Http\Repositories\General;

use Illuminate\Support\Facades\DB;

class DistritoRepository
{
    private $opcion, $idDistrito, $nombre, $descripcion, $estado;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       = $data['opcion']       ?? NULL;
			$this->idDistrito 	= $data['idDistrito'] 	?? NULL;
			$this->nombre   	= $data['nombre']   	?? NULL;
			$this->descripcion  = $data['descripcion']  ?? NULL;
			$this->estado   	= $data['estado']   	?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_ListarDistrito(?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idDistrito,
				$this->nombre,
				$this->descripcion,
				$this->estado
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarDistrito(?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idDistrito,
				$this->nombre,
				$this->descripcion,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
