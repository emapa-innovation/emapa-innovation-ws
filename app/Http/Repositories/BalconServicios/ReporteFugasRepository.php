<?php

namespace App\Http\Repositories\BalconServicios;

use Illuminate\Support\Facades\DB;

class ReporteFugasRepository
{
	private $opcion, $idFuga, $nombres, $email, $numeroTelefono, $rutaEvidencia, $direccion;
	private $ubicacionLatitud, $ubicacionLongitud, $asunto, $mensaje;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       		= $data['opcion']       		?? NULL;
			$this->idFuga  				= $data['idFuga']  				?? NULL;
			$this->nombres  			= $data['nombres']  			?? NULL;
			$this->email  				= $data['email']  				?? NULL;
			$this->numeroTelefono  		= $data['numeroTelefono']  		?? NULL;
			$this->rutaEvidencia 		= $data['rutaEvidencia'] 		?? NULL;
			$this->direccion 			= $data['direccion'] 			?? NULL;
			$this->ubicacionLatitud 	= $data['ubicacionLatitud'] 	?? NULL;
			$this->ubicacionLongitud 	= $data['ubicacionLongitud'] 	?? NULL;
			$this->asunto 				= $data['asunto'] 				?? NULL;
			$this->mensaje 				= $data['mensaje'] 				?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_ListarReporteFugas(?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idFuga,
				$this->nombres,
				$this->email,
				$this->numeroTelefono,
				$this->rutaEvidencia,
				$this->direccion,
				$this->ubicacionLatitud,
				$this->ubicacionLongitud,
				$this->asunto,
				$this->mensaje
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarReporteFugas(?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idFuga,
				$this->nombres,
				$this->email,
				$this->numeroTelefono,
				$this->rutaEvidencia,
				$this->direccion,
				$this->ubicacionLatitud,
				$this->ubicacionLongitud,
				$this->asunto,
				$this->mensaje,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
