<?php

namespace App\Http\Repositories\BalconServicios;

use Illuminate\Support\Facades\DB;

class ActualizacionDatosRepository
{
	private $opcion, $numeroCuenta, $email, $telefono;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       	= $data['opcion']       	?? NULL;
			$this->numeroCuenta  	= $data['numeroCuenta']  	?? NULL;
			$this->email  			= $data['email']  			?? NULL;
			$this->telefono  		= $data['telefono']  		?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_ListarActualizacionDatos(?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->numeroCuenta,
				$this->email,
				$this->telefono
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarActualizacionDatos(?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->numeroCuenta,
				$this->email,
				$this->telefono,
				$idUsuario
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
