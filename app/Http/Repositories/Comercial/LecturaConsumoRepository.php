<?php

namespace App\Http\Repositories\Comercial;

use Illuminate\Support\Facades\DB;

class LecturaConsumoRepository
{
    private $opcion, $idLecturaConsumo, $idCalendarioProcesos, $idMicromedidor, $idConexionAgua;
	private $idCausaNoLectura, $idLector, $lecturaAnterior, $fechaLecturaAnterior;
	private $lecturaActual, $fechaLecturaActual, $consumo, $tipoCalculo, $promedioMensual;
	private $periodo, $idCiclo, $idSector;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       		= $data['opcion']       		?? NULL;
			$this->idLecturaConsumo 	= $data['idLecturaConsumo'] 	?? NULL;
			$this->idCalendarioProcesos = $data['idCalendarioProcesos'] ?? NULL;
			$this->idMicromedidor  		= $data['idMicromedidor']  		?? NULL;
			$this->idConexionAgua  		= $data['idConexionAgua']  		?? NULL;
			$this->idCausaNoLectura 	= $data['idCausaNoLectura'] 	?? NULL;
			$this->idLector 			= $data['idLector'] 			?? NULL;
			$this->lecturaAnterior 		= $data['lecturaAnterior'] 		?? NULL;
			$this->fechaLecturaAnterior = $data['fechaLecturaAnterior'] ?? NULL;
			$this->lecturaActual 		= $data['lecturaActual'] 		?? NULL;
			$this->fechaLecturaActual 	= $data['fechaLecturaActual'] 	?? NULL;
			$this->consumo 				= $data['consumo'] 				?? NULL;
			$this->tipoCalculo 			= $data['tipoCalculo'] 			?? NULL;
			$this->promedioMensual 		= $data['promedioMensual'] 		?? NULL;
			$this->periodo 				= $data['periodo'] 				?? NULL;
			$this->idCiclo   			= $data['idCiclo']   			?? NULL;
			$this->idSector   			= $data['idSector']   			?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_ListarLecturaConsumo(?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idLecturaConsumo,
				$this->idCalendarioProcesos,
				$this->idMicromedidor,
				$this->idConexionAgua,
				$this->idCausaNoLectura,
				$this->idLector,
				$this->periodo,
				$this->idCiclo,
				$this->idSector,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarLecturaConsumo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idLecturaConsumo,
				$this->idCalendarioProcesos,
				$this->idMicromedidor,
				$this->idConexionAgua,
				$this->idCausaNoLectura,
				$this->idLector,
				$this->lecturaAnterior,
				$this->fechaLecturaAnterior,
				$this->lecturaActual,
				$this->fechaLecturaActual,
				$this->consumo,
				$this->tipoCalculo,
				$this->promedioMensual,
				$this->periodo,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
