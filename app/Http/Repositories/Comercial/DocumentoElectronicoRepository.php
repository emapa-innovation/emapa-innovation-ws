<?php

namespace App\Http\Repositories\Comercial;

use Illuminate\Support\Facades\DB;

class DocumentoElectronicoRepository
{
	private $opcion, $idDocumento, $cliente, $documento, $serie, $numero, $fechaDocumento;
	private $claveAcceso, $generado, $firmado, $recibido, $revisado, $autorizado, $contingencia;
	private $lote, $nuevo, $envioMail, $reg, $tipoIngreso, $periodo, $idCiclo;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       	= $data['opcion']       	?? NULL;
			$this->idDocumento  	= $data['idDocumento']  	?? NULL;
			$this->cliente 			= $data['cliente'] 			?? NULL;
			$this->documento  		= $data['documento']  		?? NULL;
			$this->serie  			= $data['serie']  			?? NULL;
			$this->numero  			= $data['numero']  			?? NULL;
			$this->fechaDocumento  	= $data['fechaDocumento']  	?? NULL;
			$this->claveAcceso  	= $data['claveAcceso']  	?? NULL;
			$this->generado  		= $data['generado']  		?? NULL;
			$this->firmado  		= $data['firmado']  		?? NULL;
			$this->recibido  		= $data['recibido']  		?? NULL;
			$this->revisado  		= $data['revisado']  		?? NULL;
			$this->autorizado  		= $data['autorizado']  		?? NULL;
			$this->contingencia  	= $data['contingencia']  	?? NULL;
			$this->lote  			= $data['lote']  			?? NULL;
			$this->nuevo  			= $data['nuevo']  			?? NULL;
			$this->envioMail  		= $data['envioMail']  		?? NULL;
			$this->reg  			= $data['reg']  			?? NULL;
			$this->tipoIngreso  	= $data['tipoIngreso']  	?? NULL;
			$this->periodo  		= $data['periodo']  		?? NULL;
			$this->idCiclo  		= $data['idCiclo']  		?? NULL;
		}
    }
    
	public function listar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarDocumentoElectronico(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idDocumento,
				$this->cliente,
				$this->documento,
				$this->serie,
				$this->numero,
				$this->fechaDocumento,
				$this->claveAcceso,
				$this->generado,
				$this->firmado,
				$this->recibido,
				$this->revisado,
				$this->autorizado,
				$this->contingencia,
				$this->lote,
				$this->nuevo,
				$this->envioMail,
				$this->reg,
				$this->tipoIngreso,
				$this->periodo,
				$this->idCiclo
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }

	public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarDocumentoElectronico(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idDocumento,
				$this->cliente,
				$this->documento,
				$this->serie,
				$this->numero,
				$this->fechaDocumento,
				$this->claveAcceso,
				$this->generado,
				$this->firmado,
				$this->recibido,
				$this->revisado,
				$this->autorizado,
				$this->contingencia,
				$this->lote,
				$this->nuevo,
				$this->envioMail,
				$this->reg,
				$this->tipoIngreso,
				$idUsuario
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
        }
        
		return $array;
    }
}
