<?php

namespace App\Http\Repositories\Comercial;

use Illuminate\Support\Facades\DB;

class InstalacionRepository
{
    private $opcion, $idInstalacion, $idCatastro, $idLugarEntregaFactura, $idClaseCliente, $idCategoria;
    private $idEstadoLogicoCuenta, $idTipoInstalacion, $idTipoConsumoAgua, $idCliente, $idTipoRecaudacion, $numeroCuenta;
	private $fotoPredio, $consumoPromedio, $cuentaEspecial, $fechaCreacion, $fechaBaja, $observacion, $cuentaAntigua;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       			= $data['opcion']       			?? NULL;
			$this->idInstalacion    		= $data['idInstalacion'] 	    	?? NULL;
			$this->idCatastro  				= $data['idCatastro']  				?? NULL;
			$this->idLugarEntregaFactura  	= $data['idLugarEntregaFactura']  	?? NULL;
			$this->idClaseCliente  			= $data['idClaseCliente']  			?? NULL;
			$this->idCategoria  			= $data['idCategoria']  			?? NULL;
			$this->idEstadoLogicoCuenta  	= $data['idEstadoLogicoCuenta']  	?? NULL;
			$this->idTipoInstalacion  		= $data['idTipoInstalacion']  		?? NULL;
			$this->idTipoConsumoAgua  		= $data['idTipoConsumoAgua']  		?? NULL;
			$this->idCliente  				= $data['idCliente']  				?? NULL;
			$this->idTipoRecaudacion  		= $data['idTipoRecaudacion']  		?? NULL;
			$this->numeroCuenta  			= $data['numeroCuenta']  			?? NULL;
			$this->fotoPredio  				= $data['fotoPredio']  				?? NULL;
			$this->consumoPromedio  		= $data['consumoPromedio']  		?? NULL;
			$this->cuentaEspecial  			= $data['cuentaEspecial']  			?? NULL;
			$this->fechaCreacion  			= $data['fechaCreacion']  			?? NULL;
			$this->fechaBaja  				= $data['fechaBaja']  				?? NULL;
			$this->observacion  			= $data['observacion']  			?? NULL;
			$this->cuentaAntigua  			= $data['cuentaAntigua']  			?? NULL;
		}
    }
    
	public function listar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarInstalacion(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idInstalacion,
				$this->idCatastro,
				$this->idLugarEntregaFactura,
				$this->idClaseCliente,
				$this->idCategoria,
				$this->idEstadoLogicoCuenta,
				$this->idTipoInstalacion,
				$this->idTipoConsumoAgua,
				$this->idCliente,
				$this->idTipoRecaudacion,
				$this->numeroCuenta,
				$this->consumoPromedio,
				$this->cuentaEspecial,
				$this->fechaCreacion,
				$this->fechaBaja,
				$this->cuentaAntigua
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarInstalacion(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idInstalacion,
				$this->idCatastro,
				$this->idLugarEntregaFactura,
				$this->idClaseCliente,
				$this->idCategoria,
				$this->idEstadoLogicoCuenta,
				$this->idTipoInstalacion,
				$this->idTipoConsumoAgua,
				$this->idCliente,
				$this->idTipoRecaudacion,
				$this->numeroCuenta,
				$this->fotoPredio,
				$this->consumoPromedio,
				$this->cuentaEspecial,
				$this->fechaCreacion,
				$this->fechaBaja,
				$this->observacion,
				$this->cuentaAntigua,
				$idUsuario
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
