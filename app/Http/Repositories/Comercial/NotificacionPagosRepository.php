<?php

namespace App\Http\Repositories\Comercial;

use Illuminate\Support\Facades\DB;

class NotificacionPagosRepository
{
    private $opcion, $idNotificacion, $periodoFacturacion, $periodoNotificacion, $idCiclo;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       		= $data['opcion']       		?? NULL;
			$this->idNotificacion   	= $data['idNotificacion']   	?? NULL;
			$this->periodoFacturacion   = $data['periodoFacturacion']   ?? NULL;
			$this->periodoNotificacion  = $data['periodoNotificacion']  ?? NULL;
			$this->idCiclo   			= $data['idCiclo']   			?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_ListarNotificacionPagos(?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idNotificacion,
				$this->periodoFacturacion,
				$this->periodoNotificacion,
				$this->idCiclo
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarNotificacionPagos(?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idNotificacion,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
