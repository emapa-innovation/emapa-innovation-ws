<?php

namespace App\Http\Repositories\Comercial;

use Illuminate\Support\Facades\DB;

class FacturaCabRepository
{
    private $opcion, $idFacturaCab, $idInstalacion, $idTarifa, $idCalendarioProceso, $idLecturaConsumo;
    private $idTasaInteres, $fechaFactura, $numeroFactura, $valorAutorizacion, $valorPagado, $mesesAtrasoPago;
	private $cantidadPagos, $fechaUltimoPago, $vigente, $periodo, $fechaAnulacion, $motivoAnulacion, $tipoCalculo;
	private $consumoFactura, $consumoAtraso, $mesesDeuda, $procesoCreacion, $estadoFacturaElectronica, $estado;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       			= $data['opcion']       			?? NULL;
			$this->idFacturaCab   			= $data['idFacturaCab']   			?? NULL;
			$this->idInstalacion   			= $data['idInstalacion']   			?? NULL;
			$this->idTarifa   				= $data['idTarifa']   				?? NULL;
			$this->idCalendarioProceso   	= $data['idCalendarioProceso']   	?? NULL;
			$this->idLecturaConsumo   		= $data['idLecturaConsumo']   		?? NULL;
			$this->idTasaInteres   			= $data['idTasaInteres']   			?? NULL;
			$this->fechaFactura   			= $data['fechaFactura']   			?? NULL;
			$this->numeroFactura   			= $data['numeroFactura']   			?? NULL;
			$this->valorAutorizacion   		= $data['valorAutorizacion']   		?? NULL;
			$this->valorPagado   			= $data['valorPagado']   			?? NULL;
			$this->mesesAtrasoPago   		= $data['mesesAtrasoPago']   		?? NULL;
			$this->cantidadPagos   			= $data['cantidadPagos']   			?? NULL;
			$this->fechaUltimoPago   		= $data['fechaUltimoPago']   		?? NULL;
			$this->vigente   				= $data['vigente']   				?? NULL;
			$this->periodo   				= $data['periodo']   				?? NULL;
			$this->fechaAnulacion   		= $data['fechaAnulacion']   		?? NULL;
			$this->motivoAnulacion   		= $data['motivoAnulacion']   		?? NULL;
			$this->tipoCalculo   			= $data['tipoCalculo']   			?? NULL;
			$this->consumoFactura   		= $data['consumoFactura']   		?? NULL;
			$this->consumoAtraso   			= $data['consumoAtraso']   			?? NULL;
			$this->mesesDeuda   			= $data['mesesDeuda']   			?? NULL;
			$this->procesoCreacion   		= $data['procesoCreacion']   		?? NULL;
			$this->estadoFacturaElectronica = $data['estadoFacturaElectronica'] ?? NULL;
			$this->estado   				= $data['estado']   				?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarFacturaCab(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idFacturaCab,
				$this->idInstalacion,
				$this->idTarifa,
				$this->idCalendarioProceso,
				$this->idLecturaConsumo,
				$this->idTasaInteres,
				$this->fechaFactura,
				$this->numeroFactura,
				$this->valorAutorizacion,
				$this->valorPagado,
				$this->mesesAtrasoPago,
				$this->cantidadPagos,
				$this->fechaUltimoPago,
				$this->vigente,
				$this->periodo,
				$this->fechaAnulacion,
				$this->motivoAnulacion,
				$this->tipoCalculo,
				$this->consumoFactura,
				$this->consumoAtraso,
				$this->mesesDeuda,
				$this->procesoCreacion,
				$this->estadoFacturaElectronica,
				$this->estado
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarFacturaCab(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idFacturaCab,
				$this->idInstalacion,
				$this->idTarifa,
				$this->idCalendarioProceso,
				$this->idLecturaConsumo,
				$this->idTasaInteres,
				$this->fechaFactura,
				$this->numeroFactura,
				$this->valorAutorizacion,
				$this->valorPagado,
				$this->mesesAtrasoPago,
				$this->cantidadPagos,
				$this->fechaUltimoPago,
				$this->vigente,
				$this->periodo,
				$this->fechaAnulacion,
				$this->motivoAnulacion,
				$this->tipoCalculo,
				$this->consumoFactura,
				$this->consumoAtraso,
				$this->mesesDeuda,
				$this->procesoCreacion,
				$this->estadoFacturaElectronica,
				$this->estado,
				$idUsuario
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
