<?php

namespace App\Http\Repositories\Comercial;

use Illuminate\Support\Facades\DB;

class MicromedidorRepository
{
    private $opcion, $idMicromedidor, $codigo, $fechaInstalacion, $fechaBaja;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       	= $data['opcion']       	?? NULL;
			$this->idMicromedidor  	= $data['idMicromedidor']  	?? NULL;
			$this->codigo  			= $data['codigo']  			?? NULL;
			$this->fechaInstalacion = $data['fechaInstalacion'] ?? NULL;
			$this->fechaBaja 		= $data['fechaBaja'] 		?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_ListarMicromedidor(?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idMicromedidor,
				$this->codigo,
				$this->fechaInstalacion,
				$this->fechaBaja
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarMicromedidor(?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idMicromedidor,
				$this->codigo,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
