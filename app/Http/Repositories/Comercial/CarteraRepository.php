<?php

namespace App\Http\Repositories\Comercial;

use Illuminate\Support\Facades\DB;

class CarteraRepository
{
    private $opcion, $periodo, $idCiclo, $idSector;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       = $data['opcion']       ?? NULL;
			$this->periodo   	= $data['periodo']   	?? NULL;
			$this->idCiclo   	= $data['idCiclo']   	?? NULL;
			$this->idSector   	= $data['idSector']   	?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_ListarCartera(?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->periodo,
				$this->idCiclo,
				$this->idSector
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarCartera(?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->periodo,
				$this->idCiclo,
				$this->idSector,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
