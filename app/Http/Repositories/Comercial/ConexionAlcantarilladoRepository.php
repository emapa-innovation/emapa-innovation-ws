<?php

namespace App\Http\Repositories\Comercial;

use Illuminate\Support\Facades\DB;

class ConexionAlcantarilladoRepository
{
	private $opcion, $idConexionAlcantarillado, $idDiametroAlcantarillado, $idEstadoConexionAlcantarillado;
	private $idColector, $idInstalacion, $fechaConexion, $legalizado, $idCiclo, $idSector;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       					= $data['opcion']       					?? NULL;
			$this->idConexionAlcantarillado  		= $data['idConexionAlcantarillado']  		?? NULL;
			$this->idDiametroAlcantarillado  		= $data['idDiametroAlcantarillado']  		?? NULL;
			$this->idEstadoConexionAlcantarillado  	= $data['idEstadoConexionAlcantarillado']  	?? NULL;
			$this->idColector  						= $data['idColector']  						?? NULL;
			$this->idInstalacion  					= $data['idInstalacion']  					?? NULL;
			$this->fechaConexion 					= $data['fechaConexion'] 					?? NULL;
			$this->legalizado 						= $data['legalizado'] 						?? NULL;
			$this->idCiclo 							= $data['idCiclo'] 							?? NULL;
			$this->idSector 						= $data['idSector'] 						?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_ListarConexionAlcantarillado(?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idConexionAlcantarillado,
				$this->idDiametroAlcantarillado,
				$this->idEstadoConexionAlcantarillado,
				$this->idColector,
				$this->idInstalacion,
				$this->fechaConexion,
				$this->legalizado,
				$this->idCiclo,
				$this->idSector
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarConexionAlcantarillado(?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idConexionAlcantarillado,
				$this->idDiametroAlcantarillado,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
