<?php

namespace App\Http\Repositories\Comercial;

use Illuminate\Support\Facades\DB;

class ClienteRepository
{
    private $opcion, $idCliente, $idCalificacionCliente, $identificacion, $primerNombre, $segundoNombre; 
    private $primerApellido, $segundoApellido, $telefono, $email, $estado;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       			= $data['opcion']       			?? NULL;
			$this->idCliente    			= $data['idCliente'] 	    		?? NULL;
			$this->idCalificacionCliente  	= $data['idCalificacionCliente']  	?? NULL;
			$this->identificacion  			= $data['identificacion']  			?? NULL;
			$this->primerNombre  			= $data['primerNombre']  			?? NULL;
			$this->segundoNombre  			= $data['segundoNombre']  			?? NULL;
			$this->primerApellido  			= $data['primerApellido']  			?? NULL;
			$this->segundoApellido  		= $data['segundoApellido']  		?? NULL;
			$this->telefono  				= $data['telefono']  				?? NULL;
			$this->email  					= $data['email']  					?? NULL;
			$this->estado  					= $data['estado']  					?? NULL;
		}
    }
    
	public function listar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarCliente(?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idCliente,
				$this->idCalificacionCliente,
				$this->identificacion,
				$this->primerNombre,
				$this->segundoNombre,
				$this->primerApellido,
				$this->segundoApellido,
				$this->telefono,
				$this->email,
				$this->estado
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarCliente(?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idCliente,
				$this->idCalificacionCliente,
				$this->identificacion,
				$this->primerNombre,
				$this->segundoNombre,
				$this->primerApellido,
				$this->segundoApellido,
				$this->telefono,
				$this->email,
                $this->estado,
				$idUsuario
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
