<?php

namespace App\Http\Repositories\Comercial;

use Illuminate\Support\Facades\DB;

class RecaudacionExternaRepository
{
    private $opcion, $idFacturaCab, $idInstalacion, $numeroFactura, $valorAutorizacion, $valorPagado, $cantidadPagos;
	private $mesesAtraso, $mesesDeuda, $facturaVigente, $periodo, $idCiclo, $idRecaudacion, $numeroRecaudacion;
	private $tipoPagoRecaudacion, $clasePagoRecaudacion, $valorDeuda, $valorRecaudacion, $estadoRecaudacion;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       			= $data['opcion']       			?? NULL;
			$this->idFacturaCab   			= $data['idFacturaCab']   			?? NULL;
			$this->idInstalacion   			= $data['idInstalacion']   			?? NULL;
			$this->numeroFactura   			= $data['numeroFactura']   			?? NULL;
			$this->valorAutorizacion   		= $data['valorAutorizacion']   		?? NULL;
			$this->valorPagado   			= $data['valorPagado']   			?? NULL;
			$this->cantidadPagos   			= $data['cantidadPagos']   			?? NULL;
			$this->mesesAtraso   			= $data['mesesAtraso']   			?? NULL;
			$this->mesesDeuda   			= $data['mesesDeuda']   			?? NULL;
			$this->facturaVigente   		= $data['facturaVigente']   		?? NULL;
			$this->periodo   				= $data['periodo']   				?? NULL;
			$this->idCiclo   				= $data['idCiclo']   				?? NULL;
			$this->idRecaudacion   			= $data['idRecaudacion']   			?? NULL;
			$this->numeroRecaudacion   		= $data['numeroRecaudacion']   		?? NULL;
			$this->tipoPagoRecaudacion   	= $data['tipoPagoRecaudacion']   	?? NULL;
			$this->clasePagoRecaudacion   	= $data['clasePagoRecaudacion']   	?? NULL;
			$this->valorDeuda   			= $data['valorDeuda']   			?? NULL;
			$this->valorRecaudacion   		= $data['valorRecaudacion']   		?? NULL;
			$this->estadoRecaudacion   		= $data['estadoRecaudacion']   		?? NULL;
		}
    }
    
	public function listar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarRecaudacionExterna(?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idFacturaCab,
				$this->idInstalacion,
				$this->idRecaudacion,
				$this->numeroRecaudacion,
				$this->tipoPagoRecaudacion,
				$this->clasePagoRecaudacion,
				$this->estadoRecaudacion,
				$this->numeroFactura,
				$this->periodo,
				$this->idCiclo,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idAgencia, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarRecaudacionExterna(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$idAgencia,
				$this->idFacturaCab,
				$this->idInstalacion,
				$this->numeroFactura,
				$this->valorAutorizacion,
				$this->valorPagado,
				$this->cantidadPagos,
				$this->mesesAtraso,
				$this->mesesDeuda,
				$this->facturaVigente,
				$this->periodo,
				$this->idRecaudacion,
				$this->numeroRecaudacion,
				$this->tipoPagoRecaudacion,
				$this->clasePagoRecaudacion,
				$this->valorDeuda,
				$this->valorRecaudacion,
				$this->estadoRecaudacion,
				$idUsuario
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
