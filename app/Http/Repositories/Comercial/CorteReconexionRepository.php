<?php

namespace App\Http\Repositories\Comercial;

use Illuminate\Support\Facades\DB;

class CorteReconexionRepository
{
    private $opcion, $idCorte, $idTipoCorte, $idCausaNoCorte, $idConexionAgua, $numeroCorte, $periodoCorte;
	private $fechaIngreso, $fechaCorte, $fechaReconexion, $lecturaCorte, $lecturaReconexion, $idResponsableCorte;
	private $idResponsableReconexion, $estadoCorte, $observacionCorte, $observacionReconexion, $fechaDesde, $fechaHasta;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       			= $data['opcion']       			?? NULL;
			$this->idCorte   				= $data['idCorte']   				?? NULL;
			$this->idTipoCorte   			= $data['idTipoCorte']   			?? NULL;
			$this->idCausaNoCorte   		= $data['idCausaNoCorte']   		?? NULL;
			$this->idConexionAgua   		= $data['idConexionAgua']   		?? NULL;
			$this->numeroCorte   			= $data['numeroCorte']   			?? NULL;
			$this->periodoCorte   			= $data['periodoCorte']   			?? NULL;
			$this->fechaIngreso   			= $data['fechaIngreso']   			?? NULL;
			$this->fechaCorte   			= $data['fechaCorte']   			?? NULL;
			$this->fechaReconexion   		= $data['fechaReconexion']   		?? NULL;
			$this->lecturaCorte   			= $data['lecturaCorte']   			?? NULL;
			$this->lecturaReconexion   		= $data['lecturaReconexion']   		?? NULL;
			$this->idResponsableCorte   	= $data['idResponsableCorte']   	?? NULL;
			$this->idResponsableReconexion  = $data['idResponsableReconexion']  ?? NULL;
			$this->estadoCorte   			= $data['estadoCorte']   			?? NULL;
			$this->observacionCorte   		= $data['observacionCorte']   		?? NULL;
			$this->observacionReconexion   	= $data['observacionReconexion']   	?? NULL;
			$this->fechaDesde   			= $data['fechaDesde']   			?? NULL;
			$this->fechaHasta   			= $data['fechaHasta']   			?? NULL;
		}
    }
    
	public function listar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarCorteReconexion(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idCorte,
				$this->idTipoCorte,
				$this->idCausaNoCorte,
				$this->idConexionAgua,
				$this->numeroCorte,
				$this->periodoCorte,
				$this->fechaIngreso,
				$this->fechaCorte,
				$this->fechaReconexion,
				$this->lecturaCorte,
				$this->lecturaReconexion,
				$this->idResponsableCorte,
				$this->idResponsableReconexion,
				$this->estadoCorte,
				$this->observacionCorte,
				$this->observacionReconexion,
				$this->fechaDesde,
				$this->fechaHasta
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idAgencia, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarCorteReconexion(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$idAgencia,
				$this->idCorte,
				$this->idTipoCorte,
				$this->idCausaNoCorte,
				$this->idConexionAgua,
				$this->numeroCorte,
				$this->periodoCorte,
				$this->fechaIngreso,
				$this->fechaCorte,
				$this->fechaReconexion,
				$this->lecturaCorte,
				$this->lecturaReconexion,
				$this->idResponsableCorte,
				$this->idResponsableReconexion,
				$this->estadoCorte,
				$this->observacionCorte,
				$this->observacionReconexion,
				$idUsuario
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
