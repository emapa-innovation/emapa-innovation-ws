<?php

namespace App\Http\Repositories\Comercial;

use Illuminate\Support\Facades\DB;

class ServicioRecaudacionExternaRepository
{
    private $opcion, $idFacturaCab, $idInstalacion, $fechaFactura, $numeroFactura, $valorAutorizacion, $valorPagado;
	private $cantidadPagos, $tipoBusqueda, $criterioBusqueda, $estado, $idRecaudacion, $numeroRecaudacion, $valorRecaudacion;
	private $idTransaccionEntidad, $tipoOperacionEntidad, $metodoPagoEntidad, $jsonHeaderEntidad, $jsonOperationEntidad;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       			= $data['opcion']       			?? NULL;
			$this->idFacturaCab   			= $data['idFacturaCab']   			?? NULL;
			$this->idInstalacion   			= $data['idInstalacion']   			?? NULL;
			$this->fechaFactura   			= $data['fechaFactura']   			?? NULL;
			$this->numeroFactura   			= $data['numeroFactura']   			?? NULL;
			$this->valorAutorizacion   		= $data['valorAutorizacion']   		?? NULL;
			$this->valorPagado   			= $data['valorPagado']   			?? NULL;
			$this->cantidadPagos   			= $data['cantidadPagos']   			?? NULL;
			$this->tipoBusqueda   			= $data['tipoBusqueda']   			?? NULL;
			$this->criterioBusqueda   		= $data['criterioBusqueda']   		?? NULL;
			$this->estado   				= $data['estado']   				?? NULL;
			$this->idRecaudacion   			= $data['idRecaudacion']   			?? NULL;
			$this->numeroRecaudacion   		= $data['numeroRecaudacion']   		?? NULL;
			$this->valorRecaudacion   		= $data['valorRecaudacion']   		?? NULL;
			$this->idTransaccionEntidad   	= $data['idTransaccionEntidad']   	?? NULL;
			$this->tipoOperacionEntidad   	= $data['tipoOperacionEntidad']   	?? NULL;
			$this->metodoPagoEntidad   		= $data['metodoPagoEntidad']   		?? NULL;
			$this->jsonHeaderEntidad   		= $data['jsonHeaderEntidad']   		?? NULL;
			$this->jsonOperationEntidad   	= $data['jsonOperationEntidad']   	?? NULL;
		}
    }
    
	public function listar($idEmpresa, $idAgencia, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarServicioRecaudacionExterna(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$idAgencia,
				$this->idFacturaCab,
				$this->idRecaudacion,
				$this->idTransaccionEntidad,
				$this->idInstalacion,
				$this->fechaFactura,
				$this->numeroFactura,
				$this->valorAutorizacion,
				$this->valorPagado,
				$this->cantidadPagos,
				$this->tipoBusqueda,
				$this->criterioBusqueda,
				$this->estado
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idAgencia, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarServicioRecaudacionExterna(?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$idAgencia,
				$this->idFacturaCab,
				$this->numeroFactura,
				$this->idRecaudacion,
				$this->numeroRecaudacion,
				$this->valorRecaudacion,
				$this->idTransaccionEntidad,
				$this->tipoOperacionEntidad,
				$this->metodoPagoEntidad,
				$this->jsonHeaderEntidad,
				$this->jsonOperationEntidad,
				$idUsuario
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
