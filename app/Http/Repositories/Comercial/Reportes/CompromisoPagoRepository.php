<?php

namespace App\Http\Repositories\Comercial\Reportes;

use Illuminate\Support\Facades\DB;

class CompromisoPagoRepository
{
    private $opcion, $idCompromisoPago, $idCabeceraFactura, $idCalendarioProceso, $idAgencia;
    private $idUsuarioRegistro, $idJuicioActivo, $idInstalacion, $numeroCompromiso, $fechaInicio;
    private $fechaFin, $plazo, $valorTotal, $estadoCompromiso, $observacion;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       		= $data['opcion']       		?? NULL;
			$this->idCompromisoPago   	= $data['idCompromisoPago']   	?? NULL;
			$this->idCabeceraFactura   	= $data['idCabeceraFactura']   	?? NULL;
			$this->idCalendarioProceso  = $data['idCalendarioProceso']  ?? NULL;
			$this->idAgencia   			= $data['idAgencia']   			?? NULL;
			$this->idUsuarioRegistro   	= $data['idUsuarioRegistro']   	?? NULL;
			$this->idJuicioActivo   	= $data['idJuicioActivo']   	?? NULL;
			$this->idInstalacion   		= $data['idInstalacion']   		?? NULL;
			$this->numeroCompromiso   	= $data['numeroCompromiso']   	?? NULL;
			$this->fechaInicio   		= $data['fechaInicio']   		?? NULL;
			$this->fechaFin   			= $data['fechaFin']   			?? NULL;
			$this->plazo   				= $data['plazo']   				?? NULL;
			$this->valorTotal   		= $data['valorTotal']   		?? NULL;
			$this->estadoCompromiso   	= $data['estadoCompromiso']   	?? NULL;
			$this->observacion   		= $data['observacion']   		?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_RptCompromisoPago(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idCompromisoPago,
				$this->idCabeceraFactura,
				$this->idCalendarioProceso,
				$this->idAgencia,
				$this->idUsuarioRegistro,
				$this->idJuicioActivo,
				$this->idInstalacion,
				$this->numeroCompromiso,
				$this->fechaInicio,
				$this->fechaFin,
				$this->plazo,
				$this->valorTotal,
				$this->estadoCompromiso,
				$this->observacion
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
}
