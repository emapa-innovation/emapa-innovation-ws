<?php

namespace App\Http\Repositories\Comercial\Reportes;

use Illuminate\Support\Facades\DB;

class RecaudacionCarteraRepository
{
    private $opcion, $fechaDesde, $fechaHasta;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       = $data['opcion']       ?? NULL;
			$this->fechaDesde   = $data['fechaDesde']   ?? NULL;
			$this->fechaHasta   = $data['fechaHasta']   ?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_RptRecaudacionCartera(?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->fechaDesde,
				$this->fechaHasta
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
}
