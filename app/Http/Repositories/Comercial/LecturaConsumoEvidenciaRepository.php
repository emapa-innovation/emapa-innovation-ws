<?php

namespace App\Http\Repositories\Comercial;

use Illuminate\Support\Facades\DB;

class LecturaConsumoEvidenciaRepository
{
    private $opcion, $idLecturaConsumoEvidencia, $idLecturaConsumo, $urlEvidencia;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       			 = $data['opcion']       				?? NULL;
			$this->idLecturaConsumoEvidencia = $data['idLecturaConsumoEvidencia']   ?? NULL;
			$this->idLecturaConsumo   		 = $data['idLecturaConsumo']   			?? NULL;
			$this->urlEvidencia   			 = $data['urlEvidencia']   				?? NULL;
		}
    }
    
	public function listar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarLecturaConsumoEvidencia(?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idLecturaConsumoEvidencia,
				$this->idLecturaConsumo,
				$this->urlEvidencia
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarLecturaConsumoEvidencia(?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idLecturaConsumoEvidencia,
				$this->idLecturaConsumo,
				$this->urlEvidencia,
				$idUsuario
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
