<?php

namespace App\Http\Repositories\Comercial;

use Illuminate\Support\Facades\DB;

class CorteReconexionEvidenciaRepository
{
    private $opcion, $idCorteEvidencia, $idCorte, $estadoCorte, $urlEvidencia;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       		= $data['opcion']       		?? NULL;
			$this->idCorteEvidencia   	= $data['idCorteEvidencia']   	?? NULL;
			$this->idCorte   			= $data['idCorte']   			?? NULL;
			$this->estadoCorte   		= $data['estadoCorte']   		?? NULL;
			$this->urlEvidencia   		= $data['urlEvidencia']   		?? NULL;
		}
    }
    
	public function listar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarCorteReconexionEvidencia(?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idCorteEvidencia,
				$this->idCorte,
				$this->estadoCorte,
				$this->urlEvidencia
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarCorteReconexionEvidencia(?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idCorteEvidencia,
				$this->idCorte,
				$this->estadoCorte,
				$this->urlEvidencia,
				$idUsuario
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
