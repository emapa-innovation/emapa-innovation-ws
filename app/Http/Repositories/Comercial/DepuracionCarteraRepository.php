<?php

namespace App\Http\Repositories\Comercial;

use Illuminate\Support\Facades\DB;

class DepuracionCarteraRepository
{
    private $opcion, $instalacion, $idCiclo;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       	= $data['opcion']       	?? NULL;
			$this->instalacion   	= $data['instalacion']   	?? NULL;
			$this->idCiclo   		= $data['idCiclo']   		?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_ListarDepuracionCartera(?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->instalacion,
				$this->idCiclo
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarDepuracionCartera(?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->instalacion,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
