<?php

namespace App\Http\Repositories\Gerencia\Reportes;

use Illuminate\Support\Facades\DB;

class AuditoriaAccionesRepository
{
    private $opcion, $idUsuario, $fechaDesde, $fechaHasta;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       = $data['opcion']       ?? NULL;
			$this->idUsuario   	= $data['idUsuario']   	?? NULL;
			$this->fechaDesde   = $data['fechaDesde']   ?? NULL;
			$this->fechaHasta  	= $data['fechaHasta']  	?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_RptAuditoriaAcciones(?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idUsuario,
				$this->fechaDesde,
				$this->fechaHasta
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
}
