<?php

namespace App\Http\Repositories\Facturacion;

use Illuminate\Support\Facades\DB;

class FacturaRepository
{
	private $opcion, $idFactura, $idInstalacion, $idCalendarioProceso, $idLecturaConsumo, $fechaFacturacion; 
	private $numeroFactura, $periodo, $estado, $numeroInstalacion, $codigoMedidor;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       		= $data['opcion']       		?? NULL;
			$this->idFactura    		= $data['idFactura']    		?? NULL;
			$this->idInstalacion    	= $data['idInstalacion']    	?? NULL;
			$this->idCalendarioProceso 	= $data['idCalendarioProceso'] 	?? NULL;
			$this->idLecturaConsumo 	= $data['idLecturaConsumo'] 	?? NULL;
			$this->fechaFacturacion 	= $data['fechaFacturacion'] 	?? NULL;
			$this->numeroFactura    	= $data['numeroFactura']    	?? NULL;
			$this->periodo    			= $data['periodo']    			?? NULL;
			$this->estado    			= $data['estado']    			?? NULL;
			$this->numeroInstalacion    = $data['numeroInstalacion']    ?? NULL;
			$this->codigoMedidor    	= $data['codigoMedidor']    	?? NULL;
		}
    }
    
	public function listar($idEmpresa)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarFactura(?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idFactura,
				$this->idInstalacion,
				$this->idCalendarioProceso,
				$this->idLecturaConsumo,
				$this->fechaFacturacion,
				$this->numeroFactura,
				$this->periodo,
				$this->estado,
				$this->numeroInstalacion,
				$this->codigoMedidor

			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function insertarActualizar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_SEG_InsertaActualizaFactura(?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idFactura,
				$this->idInstalacion,
				$this->idCalendarioProceso,
				$this->idLecturaConsumo,
				$this->fechaFacturacion,
				$this->numeroFactura,
				$this->periodo,
				$this->estado,
				$this->numeroInstalacion,
				$this->codigoMedidor,
				$idUsuario
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->insertarActualizar : ' . $th->getMessage());
		}
		return $array;
	}
}
