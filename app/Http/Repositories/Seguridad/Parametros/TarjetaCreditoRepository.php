<?php

namespace App\Http\Repositories\Seguridad\Parametros;

use Illuminate\Support\Facades\DB;

class TarjetaCreditoRepository
{
	private $opcion, $idTarjetaCredito, $codigo, $descripcion, $estado;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       	= $data['opcion']       	?? NULL;
			$this->idTarjetaCredito = $data['idTarjetaCredito'] ?? NULL;
			$this->codigo  			= $data['codigo']  			?? NULL;
			$this->descripcion  	= $data['descripcion']  	?? NULL;
			$this->estado 			= $data['estado'] 			?? NULL;
		}
    }
    
	public function listar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarTarjetaCredito(?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idTarjetaCredito,
				$this->codigo,
				$this->descripcion,
				$this->estado,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarTarjetaCredito(?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idTarjetaCredito,
				$this->codigo,
				$this->descripcion,
				$this->estado,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
