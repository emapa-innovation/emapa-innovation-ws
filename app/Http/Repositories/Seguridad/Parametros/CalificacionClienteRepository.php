<?php

namespace App\Http\Repositories\Seguridad\Parametros;

use Illuminate\Support\Facades\DB;

class CalificacionClienteRepository
{
	private $opcion, $idCalificacionCliente, $codigo, $descripcion;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       			= $data['opcion']       			?? NULL;
			$this->idCalificacionCliente 	= $data['idCalificacionCliente'] 	?? NULL;
			$this->codigo  					= $data['codigo']  					?? NULL;
			$this->descripcion  			= $data['descripcion']  			?? NULL;
		}
    }
    
	public function listar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarCalificacionCliente(?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idCalificacionCliente,
				$this->codigo,
				$this->descripcion,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarCalificacionCliente(?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idCalificacionCliente,
				$this->codigo,
				$this->descripcion,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
