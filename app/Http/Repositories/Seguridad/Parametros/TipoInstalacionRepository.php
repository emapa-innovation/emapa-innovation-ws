<?php

namespace App\Http\Repositories\Seguridad\Parametros;

use Illuminate\Support\Facades\DB;

class TipoInstalacionRepository
{
	private $opcion, $idTipoInstalacion, $codigo, $descripcion;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       		= $data['opcion']       		?? NULL;
			$this->idTipoInstalacion 	= $data['idTipoInstalacion'] 	?? NULL;
			$this->codigo  				= $data['codigo']  				?? NULL;
			$this->descripcion  		= $data['descripcion']  		?? NULL;
		}
    }
    
	public function listar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarTipoInstalacion(?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idTipoInstalacion,
				$this->codigo,
				$this->descripcion,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarTipoInstalacion(?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idTipoInstalacion,
				$this->codigo,
				$this->descripcion,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
