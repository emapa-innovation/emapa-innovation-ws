<?php

namespace App\Http\Repositories\Seguridad\Parametros;

use Illuminate\Support\Facades\DB;

class ItemsRepository
{
	private $opcion, $idItem, $numeracion, $codigo, $descripcion, $prioridad;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       = $data['opcion']       ?? NULL;
			$this->idItem 		= $data['idItem'] 		?? NULL;
			$this->numeracion 	= $data['numeracion'] 	?? NULL;
			$this->codigo  		= $data['codigo']  		?? NULL;
			$this->descripcion  = $data['descripcion']  ?? NULL;
			$this->prioridad 	= $data['prioridad'] 	?? NULL;
		}
    }
    
	public function listar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarItems(?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idItem,
				$this->numeracion,
				$this->codigo,
				$this->descripcion,
				$this->prioridad,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarItems(?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idItem,
				$this->numeracion,
				$this->codigo,
				$this->descripcion,
				$this->prioridad,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
