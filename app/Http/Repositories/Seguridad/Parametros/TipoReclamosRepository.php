<?php

namespace App\Http\Repositories\Seguridad\Parametros;

use Illuminate\Support\Facades\DB;

class TipoReclamosRepository
{
	private $opcion, $idTipoReclamo, $codigo, $descripcion, $servicioAsociado, $estado;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       	= $data['opcion']       	?? NULL;
			$this->idTipoReclamo 	= $data['idTipoReclamo'] 	?? NULL;
			$this->codigo  			= $data['codigo']  			?? NULL;
			$this->descripcion  	= $data['descripcion']  	?? NULL;
			$this->servicioAsociado = $data['servicioAsociado'] ?? NULL;
			$this->estado 			= $data['estado'] 			?? NULL;
		}
    }
    
	public function listar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarTipoReclamos(?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idTipoReclamo,
				$this->codigo,
				$this->descripcion,
				$this->servicioAsociado,
				$this->estado,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarTipoReclamos(?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idTipoReclamo,
				$this->codigo,
				$this->descripcion,
				$this->servicioAsociado,
				$this->estado,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
