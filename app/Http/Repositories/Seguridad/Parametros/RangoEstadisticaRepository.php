<?php

namespace App\Http\Repositories\Seguridad\Parametros;

use Illuminate\Support\Facades\DB;

class RangoEstadisticaRepository
{
	private $opcion, $idRangoEstadistica, $rangoInicial, $rangoFinal;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       		= $data['opcion']       		?? NULL;
			$this->idRangoEstadistica 	= $data['idRangoEstadistica'] 	?? NULL;
			$this->rangoInicial  		= $data['rangoInicial']  		?? NULL;
			$this->rangoFinal  			= $data['rangoFinal']  			?? NULL;
		}
    }
    
	public function listar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarRangoEstadistica(?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idRangoEstadistica,
				$this->rangoInicial,
				$this->rangoFinal,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarRangoEstadistica(?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idRangoEstadistica,
				$this->rangoInicial,
				$this->rangoFinal,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
