<?php

namespace App\Http\Repositories\Seguridad\Parametros;

use Illuminate\Support\Facades\DB;

class TablaValoresRepository
{
	private $opcion, $idTablaValores, $codigo, $descripcion, $valor, $observacion, $tipo;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       	= $data['opcion']       	?? NULL;
			$this->idTablaValores  	= $data['idTablaValores']  	?? NULL;
			$this->codigo  			= $data['codigo']  			?? NULL;
			$this->descripcion  	= $data['descripcion']  	?? NULL;
			$this->valor 			= $data['valor'] 			?? NULL;
			$this->observacion 		= $data['observacion'] 		?? NULL;
			$this->tipo 			= $data['tipo'] 			?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_ListarTablaValores(?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idTablaValores,
				$this->codigo,
				$this->descripcion,
				$this->valor,
				$this->observacion,
				$this->tipo,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarTablaValores(?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idTablaValores,
				$this->codigo,
				$this->descripcion,
				$this->valor,
				$this->observacion,
				$this->tipo,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
