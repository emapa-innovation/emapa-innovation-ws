<?php

namespace App\Http\Repositories\Seguridad\Parametros;

use Illuminate\Support\Facades\DB;

class ContratoServicioRepository
{
	private $opcion, $idContratoServicio, $idDistrito, $codigo, $descripcion, $tipo, $facturable;
	private $valor, $presupuesto, $diasPatron, $estado, $clase, $iva, $generaOT;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       		= $data['opcion']       		?? NULL;
			$this->idContratoServicio  	= $data['idContratoServicio']  	?? NULL;
			$this->idDistrito  			= $data['idDistrito']  			?? NULL;
			$this->codigo  				= $data['codigo']  				?? NULL;
			$this->descripcion  		= $data['descripcion']  		?? NULL;
			$this->tipo  				= $data['tipo']  				?? NULL;
			$this->facturable 			= $data['facturable'] 			?? NULL;
			$this->valor 				= $data['valor'] 				?? NULL;
			$this->presupuesto 			= $data['presupuesto'] 			?? NULL;
			$this->diasPatron 			= $data['diasPatron'] 			?? NULL;
			$this->estado 				= $data['estado'] 				?? NULL;
			$this->clase 				= $data['clase'] 				?? NULL;
			$this->iva 					= $data['iva'] 					?? NULL;
			$this->generaOT 			= $data['generaOT'] 			?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_ListarContratosServicios(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idContratoServicio,
				$this->idDistrito,
				$this->codigo,
				$this->descripcion,
				$this->tipo,
				$this->facturable,
				$this->valor,
				$this->presupuesto,
				$this->diasPatron,
				$this->estado,
				$this->clase,
				$this->iva,
				$this->generaOT
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarContratosServicios(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idContratoServicio,
				$this->idDistrito,
				$this->codigo,
				$this->descripcion,
				$this->tipo,
				$this->facturable,
				$this->valor,
				$this->presupuesto,
				$this->diasPatron,
				$this->estado,
				$this->clase,
				$this->iva,
				$this->generaOT,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
