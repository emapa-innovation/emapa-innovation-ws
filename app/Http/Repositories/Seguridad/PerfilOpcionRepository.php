<?php

namespace App\Http\Repositories\Seguridad;

use Illuminate\Support\Facades\DB;

class PerfilOpcionRepository
{
    private $opcion, $idAgencia, $idPerfilOpcion, $idPerfil, $idOpcion, $idUsuario;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion      		= $data['opcion']      		?? NULL;
			$this->idAgencia   		= $data['idAgencia']   		?? NULL;
			$this->idPerfilOpcion   = $data['idPerfilOpcion']   ?? NULL;
			$this->idPerfil    		= $data['idPerfil']    		?? NULL;
			$this->idOpcion    		= $data['idOpcion']    		?? NULL;
			$this->idUsuario    	= $data['idUsuario']    	?? NULL;
		}
    }
    
    public function listar($idEmpresa, $idUsuario){
		try {
			$array = DB::select('CALL SP_CON_ListarPerfilOpcion(?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idAgencia,
				$this->idPerfilOpcion,
				$this->idPerfil,
				$this->idOpcion,
				$this->idUsuario
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }

	public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarPerfilOpcion(?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idAgencia,
				$this->idPerfilOpcion,
				$this->idPerfil,
				$this->idOpcion,
				$idUsuario
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
