<?php

namespace App\Http\Repositories\Seguridad;

use Illuminate\Support\Facades\DB;

class LoginRepository
{
    private $opcion, $username, $password;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion      = $data['opcion']      ?? NULL;
			$this->username    = $data['username']    ?? NULL;
			$this->password    = $data['password']    ?? NULL;
		}
    }
    
    public function login($idEmpresa, $idAgencia){
		try {
			$array = DB::select('CALL SP_CON_Login(?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$idAgencia,
				$this->username,
				$this->password
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->login : ' . $th->getMessage());
        }
        
		return $array;
    }
}
