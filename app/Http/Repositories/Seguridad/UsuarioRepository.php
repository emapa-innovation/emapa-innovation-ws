<?php

namespace App\Http\Repositories\Seguridad;

use Illuminate\Support\Facades\DB;

class UsuarioRepository
{
    private $opcion, $idUsuario, $usuario, $clave, $primerNombre, $segundoNombre, $primerApellido, $segundoApellido; 
    private $identificacion, $direccion, $telefono, $celular, $estado, $numeroIntentos;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       	= $data['opcion']       	?? NULL;
			$this->idUsuario    	= $data['idUsuario'] 	    ?? NULL;
			$this->usuario  		= $data['usuario']  		?? NULL;
			$this->clave  			= $data['clave']  			?? NULL;
			$this->primerNombre  	= $data['primerNombre']  	?? NULL;
			$this->segundoNombre  	= $data['segundoNombre']  	?? NULL;
			$this->primerApellido  	= $data['primerApellido']  	?? NULL;
			$this->segundoApellido  = $data['segundoApellido']  ?? NULL;
			$this->identificacion  	= $data['identificacion']  	?? NULL;
			$this->direccion  		= $data['direccion']  		?? NULL;
			$this->telefono  		= $data['telefono']  		?? NULL;
			$this->celular  		= $data['celular']  		?? NULL;
			$this->estado  			= $data['estado']  			?? NULL;
			$this->numeroIntentos  	= $data['numeroIntentos']  	?? NULL;
		}
    }
    
	public function listar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarUsuario(?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idUsuario,
				$this->usuario,
				$this->clave,
				$this->primerNombre,
				$this->segundoNombre,
				$this->primerApellido,
				$this->segundoApellido,
				$this->identificacion,
				$this->direccion,
				$this->telefono,
				$this->celular,
				$this->estado
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarUsuario(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idUsuario,
				$this->usuario,
				$this->clave,
				$this->primerNombre,
				$this->segundoNombre,
				$this->primerApellido,
				$this->segundoApellido,
				$this->identificacion,
				$this->direccion,
				$this->telefono,
				$this->celular,
                $this->estado,
                $this->numeroIntentos,
				$idUsuario,
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
