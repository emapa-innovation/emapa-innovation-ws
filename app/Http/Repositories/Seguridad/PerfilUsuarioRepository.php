<?php

namespace App\Http\Repositories\Seguridad;

use Illuminate\Support\Facades\DB;

class PerfilUsuarioRepository
{
    private $opcion, $idPerfil, $codigo, $descripcion, $estado, $idUsuario;

	public function __construct(array $data = NULL)
	{
		if(isset($data))
		{
			$this->opcion       = $data['opcion']       ?? NULL;
			$this->idPerfil    	= $data['idPerfil'] 	?? NULL;
			$this->codigo  		= $data['codigo']  		?? NULL;
			$this->descripcion  = $data['descripcion']  ?? NULL;
			$this->estado  		= $data['estado']  		?? NULL;
			$this->idUsuario  	= $data['idUsuario']  	?? NULL;
		}
    }
    
	public function listar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_CON_ListarPerfilUsuario(?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idPerfil,
				$this->codigo,
				$this->descripcion,
				$this->estado,
				$this->idUsuario
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }
        
		return $array;
    }
    
    public function guardar($idEmpresa, $idUsuario)
	{
		try {
			$array = DB::select('CALL SP_MNT_GuardarPerfilUsuario(?,?,?,?,?,?,?,?)', [
				$this->opcion,
				$idEmpresa,
				$this->idPerfil,
				$this->codigo,
				$this->descripcion,
				$this->estado,
				$this->idUsuario,
				$idUsuario
			]);
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
		}
		return $array;
	}
}
