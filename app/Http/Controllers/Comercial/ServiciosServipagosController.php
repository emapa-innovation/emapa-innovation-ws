<?php

namespace App\Http\Controllers\Comercial;

use Illuminate\Http\Request;
use App\Helpers\EstadoTransaccionRecaudacion;
use App\AuthToken\JWToken;
use App\Http\BusinessLayer\Comercial\ServiciosServipagosBLL;
use Illuminate\Support\Facades\Log;

class ServiciosServipagosController
{
    private $et, $idEmpresa, $idAgencia, $idUsuario, $modulo, $opcion, $ServiciosServipagosBLL;

    public function __construct(Request $request)
    {
        $this->et = new EstadoTransaccionRecaudacion();
        $this->ServiciosServipagosBLL = new ServiciosServipagosBLL;
        $userInfo = JWToken::informacionToken($request);
        $this->idUsuario = $userInfo->idUsuario;
        $this->idEmpresa = $userInfo->idEmpresa;
        $this->idAgencia = $userInfo->idAgencia;
        $this->modulo = 'Comercial';
        $this->opcion = 'Servicios Servipagos';
    }

    public function index()
    {
        try {
            $this->et = $this->ServiciosServipagosBLL->index($this->idEmpresa);

            if ($this->et->body['dataResponse']['codigo'] != '00') {
                throw new \Exception($this->et->body['dataResponse']['descripcion'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->body['dataResponse']['descripcion'] = $this->et::$transaccionExitosaDesc;
        } catch (\Throwable $th) {
            if ( empty($this->et->body['dataResponse']['descripcion']) ) {
                $this->et->body['dataResponse']['codigo'] = $this->et::$errorTecnicoCod;
                $this->et->body['dataResponse']['descripcion'] = $this->et::$errorTecnicoDesc;
            }
            Log::error('SERVIPAGOS - INDEX (ERROR): '.$th->getMessage());
        }

        return response()->json($this->et);
    }

    public function consulta(Request $request)
    {
        try{
            $data = json_decode($request->getContent(), true);
            Log::info('SERVIPAGOS - CONSULTA (REQUEST):', $data);
            $this->et = $this->ServiciosServipagosBLL->consulta($this->idEmpresa, $this->idAgencia, $this->idUsuario, $data);

            if($this->et->body['dataResponse']['codigo'] != '00'){
                throw new \Exception($this->et->body['dataResponse']['descripcion'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->body['dataResponse']['descripcion'] = $this->et::$transaccionExitosaDesc;
            Log::info('SERVIPAGOS - CONSULTA (RESPONSE):', $this->et->body);
        } catch (\Throwable $th) {
            if ( empty($this->et->body['dataResponse']['descripcion']) ) {
                $this->et->body['dataResponse']['codigo'] = $this->et::$errorTecnicoCod;
                $this->et->body['dataResponse']['descripcion'] = $this->et::$errorTecnicoDesc;
            }
            Log::error('SERVIPAGOS - CONSULTA (ERROR): '.$th->getMessage());
        }

        return response()->json($this->et);
    }
    
    public function pago(Request $request)
    {
        try{
            $data = json_decode($request->getContent(), true);
            Log::info('SERVIPAGOS - PAGO (REQUEST):', $data);
            $this->et = $this->ServiciosServipagosBLL->pago($this->idEmpresa, $this->idAgencia, $this->idUsuario, $data);

            if($this->et->body['dataResponse']['codigo'] != '00'){
                throw new \Exception($this->et->body['dataResponse']['descripcion'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->body['dataResponse']['descripcion'] = $this->et::$transaccionExitosaDesc;
            Log::info('SERVIPAGOS - PAGO (RESPONSE):', $this->et->body);
        } catch (\Throwable $th) {
            if ( empty($this->et->body['dataResponse']['descripcion']) ) {
                $this->et->body['dataResponse']['codigo'] = $this->et::$errorTecnicoCod;
                $this->et->body['dataResponse']['descripcion'] = $this->et::$errorTecnicoDesc;
            }
            Log::error('SERVIPAGOS - PAGO (ERROR): '.$th->getMessage());
        }

        return response()->json($this->et);
    }
    
    public function reverso(Request $request)
    {
        try{
            $data = json_decode($request->getContent(), true);
            Log::info('SERVIPAGOS - REVERSO (REQUEST):', $data);
            $this->et = $this->ServiciosServipagosBLL->reverso($this->idEmpresa, $this->idAgencia, $this->idUsuario, $data);

            if($this->et->body['dataResponse']['codigo'] != '00'){
                throw new \Exception($this->et->body['dataResponse']['descripcion'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->body['dataResponse']['descripcion'] = $this->et::$transaccionExitosaDesc;
            Log::info('SERVIPAGOS - REVERSO (RESPONSE):', $this->et->body);
        } catch (\Throwable $th) {
            if ( empty($this->et->body['dataResponse']['descripcion']) ) {
                $this->et->body['dataResponse']['codigo'] = $this->et::$errorTecnicoCod;
                $this->et->body['dataResponse']['descripcion'] = $this->et::$errorTecnicoDesc;
            }
            Log::error('SERVIPAGOS - REVERSO (ERROR): '.$th->getMessage());
        }

        return response()->json($this->et);
    }
}
