<?php

namespace App\Http\Controllers\Comercial;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Helpers\EstadoTransaccion;
use App\AuthToken\JWToken;
use App\Http\BusinessLayer\Comercial\CorteReconexionBLL;

class CorteReconexionController
{
    private $et, $idEmpresa, $idAgencia, $idUsuario, $modulo, $opcion, $CorteReconexionBLL;

    public function __construct(Request $request)
    {
        $this->et = new EstadoTransaccion();
        $this->CorteReconexionBLL = new CorteReconexionBLL;
        $userInfo = JWToken::informacionToken($request);
        $this->idUsuario = $userInfo->idUsuario;
        $this->idEmpresa = $userInfo->idEmpresa;
        $this->idAgencia = $userInfo->idAgencia;
        $this->modulo = 'Comercial';
        $this->opcion = 'Cortes y Reconexiones';
    }

    public function index()
    {
        try {
            $this->et = $this->CorteReconexionBLL->index($this->idEmpresa);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }

            if( empty($this->et->data) ){
                $this->et->mensaje = $this->et::$noExistenDatos; 
            }
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }

            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function listar(Request $request){
        try{
            $data = json_decode($request->getContent(), true);
            $this->et = $this->CorteReconexionBLL->listar($this->idEmpresa, $this->idUsuario, $data);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function guardar(Request $request){
        try{
            $data = json_decode($request->getContent(), true);
            $this->et = $this->CorteReconexionBLL->guardar($this->idEmpresa, $this->idAgencia, $this->idUsuario, $data);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }

            $this->et->mensaje = $this->et::$procesoExitoso;
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function importar(Request $request){
        try{
            $this->et = $this->CorteReconexionBLL->importar($this->idEmpresa, $this->idAgencia, $this->idUsuario, $request);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }

            $this->et->mensaje = $this->et::$procesoExitoso;
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function exportar(Request $request){
        try{
            $data = json_decode($request->getContent(), true);
            $this->et = $this->CorteReconexionBLL->exportar($this->idEmpresa, $this->idUsuario, $data);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }

            $contentType = Storage::mimeType($this->et->data['nameFile']);
            $nameFile = basename($this->et->data['nameFile']);
            $content = Storage::get($this->et->data['nameFile']);

            Storage::delete($this->et->data['nameFile']);

            return response($content)
                    ->header('Content-Type', $contentType)
                    ->header('Pragma','public')
                    ->header('Content-Disposition','inline; filename="'.$nameFile.'"')
                    ->header('Cache-Control','max-age=60, must-revalidate');
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }
}
