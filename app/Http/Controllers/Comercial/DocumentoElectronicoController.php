<?php

namespace App\Http\Controllers\Comercial;

use Illuminate\Http\Request;
use App\Helpers\EstadoTransaccion;
use App\AuthToken\JWToken;
use App\Http\BusinessLayer\Comercial\DocumentoElectronicoBLL;
use Illuminate\Support\Facades\Log;

class DocumentoElectronicoController
{
    private $et, $idEmpresa, $idUsuario, $modulo, $opcion, $DocumentoElectronicoBLL;

    public function __construct(Request $request)
    {
        $this->et = new EstadoTransaccion();
        $this->DocumentoElectronicoBLL = new DocumentoElectronicoBLL;
        $userInfo = JWToken::informacionToken($request);
        $this->idUsuario = $userInfo->idUsuario;
        $this->idEmpresa = $userInfo->idEmpresa;
        $this->modulo  = 'Comercial';
        $this->opcion  = 'Documentos Electronicos';
    }

    public function index()
    {
        try {
            $this->et = $this->DocumentoElectronicoBLL->index($this->idEmpresa);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }

            if( empty($this->et->data) ){
                $this->et->mensaje = $this->et::$noExistenDatos; 
            }
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }

            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function listar(Request $request){
        try{
            $data = json_decode($request->getContent(), true);
            $this->et = $this->DocumentoElectronicoBLL->listar($this->idEmpresa, $this->idUsuario, $data);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function enviarEmailRide(Request $request){
        try{
            $data = json_decode($request->getContent(), true);
            $this->et = $this->DocumentoElectronicoBLL->enviarEmailRide($this->idEmpresa, $this->idUsuario, $data);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }

            $this->et->mensaje = $this->et::$procesoExitoso;
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function enviarEmailRideXLote(Request $request){
        try{
            $data = json_decode($request->getContent(), true);
            $this->et = $this->DocumentoElectronicoBLL->enviarEmailRideXLote($this->idEmpresa, $this->idUsuario, $data);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }

            $this->et->mensaje = $this->et::$procesoExitoso;
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }
}
