<?php

namespace App\Http\Controllers\Comercial;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\AuthToken\JWToken;
use App\Helpers\EstadoTransaccion;
use App\Http\BusinessLayer\Comercial\InstalacionBLL;

class InstalacionController
{
    private $et, $idEmpresa, $idUsuario, $modulo, $opcion, $InstalacionBLL;

    public function __construct(Request $request)
    {
        $this->et = new EstadoTransaccion();
        $this->InstalacionBLL = new InstalacionBLL;
        $userInfo = JWToken::informacionToken($request);
        $this->idUsuario = $userInfo->idUsuario;
        $this->idEmpresa = $userInfo->idEmpresa;
        $this->modulo = 'Comercial';
        $this->opcion = 'Instalacion';
    }

    public function index()
    {
        try {
            $this->et = $this->InstalacionBLL->index($this->idEmpresa);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }

            if( empty($this->et->data) ){
                $this->et->mensaje = $this->et::$noExistenDatos; 
            }
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }

            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function listar(Request $request){
        try{
            $data = json_decode($request->getContent(), true);
            $this->et = $this->InstalacionBLL->listar($this->idEmpresa, $this->idUsuario, $data);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function guardar(Request $request){
        try{
            $data = json_decode($request->getContent(), true);
            $this->et = $this->InstalacionBLL->guardar($this->idEmpresa, $this->idUsuario, $data);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }

            $this->et->mensaje = $this->et::$procesoExitoso;
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }
}
