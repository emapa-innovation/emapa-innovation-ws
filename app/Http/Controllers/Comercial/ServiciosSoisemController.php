<?php

namespace App\Http\Controllers\Comercial;

use Illuminate\Http\Request;
use App\Helpers\EstadoTransaccionRecaudacion;
use App\AuthToken\JWToken;
use App\Http\BusinessLayer\Comercial\ServiciosSoisemBLL;
use Illuminate\Support\Facades\Log;

class ServiciosSoisemController
{
    private $et, $idEmpresa, $idAgencia, $idUsuario, $modulo, $opcion, $ServiciosSoisemBLL;

    public function __construct(Request $request)
    {
        $this->et = new EstadoTransaccionRecaudacion();
        $this->ServiciosSoisemBLL = new ServiciosSoisemBLL;
        $userInfo = JWToken::informacionToken($request);
        $this->idUsuario = $userInfo->idUsuario;
        $this->idEmpresa = $userInfo->idEmpresa;
        $this->idAgencia = $userInfo->idAgencia;
        $this->modulo = 'Comercial';
        $this->opcion = 'Servicios Soisem';
    }

    public function index()
    {
        try {
            $this->et = $this->ServiciosSoisemBLL->index($this->idEmpresa);

            if ($this->et->body['dataResponse']['codigo'] != '00') {
                throw new \Exception($this->et->body['dataResponse']['descripcion'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->body['dataResponse']['descripcion'] = $this->et::$transaccionExitosaDesc;
        } catch (\Throwable $th) {
            if ( empty($this->et->body['dataResponse']['descripcion']) ) {
                $this->et->body['dataResponse']['codigo'] = $this->et::$errorTecnicoCod;
                $this->et->body['dataResponse']['descripcion'] = $this->et::$errorTecnicoDesc;
            }
            Log::error('SOISEM - INDEX (ERROR): '.$th->getMessage());
        }

        return response()->json($this->et);
    }

    public function consultaCortes(Request $request)
    {
        try{
            $data = json_decode($request->getContent(), true);
            Log::info('SOISEM - CONSULTA CORTES (REQUEST):', $data);
            $this->et = $this->ServiciosSoisemBLL->consultaCortes($this->idEmpresa, $this->idAgencia, $this->idUsuario, $data);

            if($this->et->body['dataResponse']['codigo'] != '00'){
                throw new \Exception($this->et->body['dataResponse']['descripcion'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->body['dataResponse']['descripcion'] = $this->et::$transaccionExitosaDesc;
            // Log::info('SOISEM - CONSULTA CORTES (RESPONSE):', $this->et->body);
        } catch (\Throwable $th) {
            if ( empty($this->et->body['dataResponse']['descripcion']) ) {
                $this->et->body['dataResponse']['codigo'] = $this->et::$errorTecnicoCod;
                $this->et->body['dataResponse']['descripcion'] = $this->et::$errorTecnicoDesc;
            }
            Log::error('SOISEM - CONSULTA CORTES (ERROR): '.$th->getMessage());
        }

        return response()->json($this->et);
    }

    public function consultaReconexiones(Request $request)
    {
        try{
            $data = json_decode($request->getContent(), true);
            Log::info('SOISEM - CONSULTA RECONEXIONES (REQUEST):', $data);
            $this->et = $this->ServiciosSoisemBLL->consultaReconexiones($this->idEmpresa, $this->idAgencia, $this->idUsuario, $data);

            if($this->et->body['dataResponse']['codigo'] != '00'){
                throw new \Exception($this->et->body['dataResponse']['descripcion'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->body['dataResponse']['descripcion'] = $this->et::$transaccionExitosaDesc;
            // Log::info('SOISEM - CONSULTA RECONEXIONES (RESPONSE):', $this->et->body);
        } catch (\Throwable $th) {
            if ( empty($this->et->body['dataResponse']['descripcion']) ) {
                $this->et->body['dataResponse']['codigo'] = $this->et::$errorTecnicoCod;
                $this->et->body['dataResponse']['descripcion'] = $this->et::$errorTecnicoDesc;
            }
            Log::error('SOISEM - CONSULTA RECONEXIONES (ERROR): '.$th->getMessage());
        }

        return response()->json($this->et);
    }
    
    public function corte(Request $request)
    {
        try{
            $data = json_decode($request->getContent(), true);
            Log::info('SOISEM - CORTE (REQUEST):', $data);
            $this->et = $this->ServiciosSoisemBLL->corte($this->idEmpresa, $this->idAgencia, $this->idUsuario, $data);

            if($this->et->body['dataResponse']['codigo'] != '00'){
                throw new \Exception($this->et->body['dataResponse']['descripcion'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->body['dataResponse']['descripcion'] = $this->et::$transaccionExitosaDesc;
            Log::info('SOISEM - CORTE (RESPONSE):', $this->et->body);
        } catch (\Throwable $th) {
            if ( empty($this->et->body['dataResponse']['descripcion']) ) {
                $this->et->body['dataResponse']['codigo'] = $this->et::$errorTecnicoCod;
                $this->et->body['dataResponse']['descripcion'] = $this->et::$errorTecnicoDesc;
            }
            Log::error('SOISEM - CORTE (ERROR): '.$th->getMessage());
        }

        return response()->json($this->et);
    }
    
    public function reconexion(Request $request)
    {
        try{
            $data = json_decode($request->getContent(), true);
            Log::info('SOISEM - RECONEXION (REQUEST):', $data);
            $this->et = $this->ServiciosSoisemBLL->reconexion($this->idEmpresa, $this->idAgencia, $this->idUsuario, $data);

            if($this->et->body['dataResponse']['codigo'] != '00'){
                throw new \Exception($this->et->body['dataResponse']['descripcion'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->body['dataResponse']['descripcion'] = $this->et::$transaccionExitosaDesc;
            Log::info('SOISEM - RECONEXION (RESPONSE):', $this->et->body);
        } catch (\Throwable $th) {
            if ( empty($this->et->body['dataResponse']['descripcion']) ) {
                $this->et->body['dataResponse']['codigo'] = $this->et::$errorTecnicoCod;
                $this->et->body['dataResponse']['descripcion'] = $this->et::$errorTecnicoDesc;
            }
            Log::error('SOISEM - RECONEXION (ERROR): '.$th->getMessage());
        }

        return response()->json($this->et);
    }

    public function consultaLecturas(Request $request)
    {
        try{
            $data = json_decode($request->getContent(), true);
            Log::info('SOISEM - CONSULTA LECTURAS (REQUEST):', $data);
            $this->et = $this->ServiciosSoisemBLL->consultaLecturas($this->idEmpresa, $this->idAgencia, $this->idUsuario, $data);

            if($this->et->body['dataResponse']['codigo'] != '00'){
                throw new \Exception($this->et->body['dataResponse']['descripcion'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->body['dataResponse']['descripcion'] = $this->et::$transaccionExitosaDesc;
        } catch (\Throwable $th) {
            if ( empty($this->et->body['dataResponse']['descripcion']) ) {
                $this->et->body['dataResponse']['codigo'] = $this->et::$errorTecnicoCod;
                $this->et->body['dataResponse']['descripcion'] = $this->et::$errorTecnicoDesc;
            }
            Log::error('SOISEM - CONSULTA LECTURAS (ERROR): '.$th->getMessage());
        }

        return response()->json($this->et);
    }

    public function ingresoLectura(Request $request)
    {
        try{
            $data = json_decode($request->getContent(), true);
            Log::info('SOISEM - INGRESO LECTURA (REQUEST):', $data);
            $this->et = $this->ServiciosSoisemBLL->ingresoLectura($this->idEmpresa, $this->idAgencia, $this->idUsuario, $data);

            if($this->et->body['dataResponse']['codigo'] != '00'){
                throw new \Exception($this->et->body['dataResponse']['descripcion'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->body['dataResponse']['descripcion'] = $this->et::$transaccionExitosaDesc;
            Log::info('SOISEM - INGRESO LECTURA (RESPONSE):', $this->et->body);
        } catch (\Throwable $th) {
            if ( empty($this->et->body['dataResponse']['descripcion']) ) {
                $this->et->body['dataResponse']['codigo'] = $this->et::$errorTecnicoCod;
                $this->et->body['dataResponse']['descripcion'] = $this->et::$errorTecnicoDesc;
            }
            Log::error('SOISEM - INGRESO LECTURA (ERROR): '.$th->getMessage());
        }

        return response()->json($this->et);
    }
}
