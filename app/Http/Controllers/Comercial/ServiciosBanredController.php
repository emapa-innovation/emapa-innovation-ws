<?php

namespace App\Http\Controllers\Comercial;

use Illuminate\Http\Request;
use App\Helpers\EstadoTransaccionBanred;
use App\AuthToken\JWToken;
use App\Http\BusinessLayer\Comercial\ServiciosBanredBLL;
use Illuminate\Support\Facades\Log;

class ServiciosBanredController
{
    private $et, $idEmpresa, $idAgencia, $idUsuario, $modulo, $opcion, $ServiciosBanredBLL;

    public function __construct(Request $request)
    {
        $this->et = new EstadoTransaccionBanred();
        $this->ServiciosBanredBLL = new ServiciosBanredBLL;
        $userInfo = JWToken::informacionToken($request);
        $this->idUsuario = $userInfo->idUsuario;
        $this->idEmpresa = $userInfo->idEmpresa;
        $this->idAgencia = $userInfo->idAgencia;
        $this->modulo = 'Comercial';
        $this->opcion = 'Servicios Banred';
    }

    public function index()
    {
        try {
            $this->et = $this->ServiciosBanredBLL->index($this->idEmpresa);

            if ($this->et->infoResponse['responseCode'] != '00') {
                throw new \Exception($this->et->infoResponse['descResponse'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->infoResponse['descResponse'] = $this->et::$transaccionExitosaDesc;
        } catch (\Throwable $th) {
            if ( empty($this->et->infoResponse['descResponse']) ) {
                $this->et->infoResponse['responseCode'] = $this->et::$errorTecnicoCod;
                $this->et->infoResponse['descResponse'] = $this->et::$errorTecnicoDesc;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function consulta(Request $request)
    {
        try{
            $data = json_decode($request->getContent(), true);
            Log::info('BANRED - CONSULTA (REQUEST):', $data);
            $this->et = $this->ServiciosBanredBLL->consulta($this->idEmpresa, $this->idAgencia, $this->idUsuario, $data);

            if($this->et->infoResponse['responseCode'] != '00'){
                throw new \Exception($this->et->infoResponse['descResponse'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->infoResponse['descResponse'] = $this->et::$transaccionExitosaDesc;
            Log::info('BANRED - CONSULTA (RESPONSE):', $this->et->infoOperationResponse);
        } catch (\Throwable $th) {
            if ( empty($this->et->infoResponse['descResponse']) ) {
                $this->et->infoResponse['responseCode'] = $this->et::$errorTecnicoCod;
                $this->et->infoResponse['descResponse'] = $this->et::$errorTecnicoDesc;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }
    
    public function pago(Request $request)
    {
        try{
            $data = json_decode($request->getContent(), true);
            Log::info('BANRED - PAGO (REQUEST):', $data);
            $this->et = $this->ServiciosBanredBLL->pago($this->idEmpresa, $this->idAgencia, $this->idUsuario, $data);

            if($this->et->infoResponse['responseCode'] != '00'){
                throw new \Exception($this->et->infoResponse['descResponse'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->infoResponse['descResponse'] = $this->et::$transaccionExitosaDesc;
            Log::info('BANRED - PAGO (RESPONSE):', $this->et->infoOperationResponse);
        } catch (\Throwable $th) {
            if ( empty($this->et->infoResponse['descResponse']) ) {
                $this->et->infoResponse['responseCode'] = $this->et::$errorTecnicoCod;
                $this->et->infoResponse['descResponse'] = $this->et::$errorTecnicoDesc;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }
    
    public function reverso(Request $request)
    {
        try{
            $data = json_decode($request->getContent(), true);
            Log::info('BANRED - REVERSO (REQUEST):', $data);
            $this->et = $this->ServiciosBanredBLL->reverso($this->idEmpresa, $this->idAgencia, $this->idUsuario, $data);

            if($this->et->infoResponse['responseCode'] != '00'){
                throw new \Exception($this->et->infoResponse['descResponse'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->infoResponse['descResponse'] = $this->et::$transaccionExitosaDesc;
            Log::info('BANRED - REVERSO (RESPONSE):', $this->et->infoOperationResponse);
        } catch (\Throwable $th) {
            if ( empty($this->et->infoResponse['descResponse']) ) {
                $this->et->infoResponse['responseCode'] = $this->et::$errorTecnicoCod;
                $this->et->infoResponse['descResponse'] = $this->et::$errorTecnicoDesc;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }
}
