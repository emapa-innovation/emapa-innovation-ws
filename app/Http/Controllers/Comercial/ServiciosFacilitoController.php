<?php

namespace App\Http\Controllers\Comercial;

use Illuminate\Http\Request;
use App\Helpers\EstadoTransaccionRecaudacion;
use App\AuthToken\JWToken;
use App\Http\BusinessLayer\Comercial\ServiciosFacilitoBLL;
use Illuminate\Support\Facades\Log;

class ServiciosFacilitoController
{
    private $et, $idEmpresa, $idAgencia, $idUsuario, $modulo, $opcion, $ServiciosFacilitoBLL;

    public function __construct(Request $request)
    {
        $this->et = new EstadoTransaccionRecaudacion();
        $this->ServiciosFacilitoBLL = new ServiciosFacilitoBLL;
        $userInfo = JWToken::informacionToken($request);
        $this->idUsuario = $userInfo->idUsuario;
        $this->idEmpresa = $userInfo->idEmpresa;
        $this->idAgencia = $userInfo->idAgencia;
        $this->modulo = 'Comercial';
        $this->opcion = 'Servicios Facilito';
    }

    public function index()
    {
        try {
            $this->et = $this->ServiciosFacilitoBLL->index($this->idEmpresa);

            if ($this->et->body['dataResponse']['codigo'] != '00') {
                throw new \Exception($this->et->body['dataResponse']['descripcion'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->body['dataResponse']['descripcion'] = $this->et::$transaccionExitosaDesc;
        } catch (\Throwable $th) {
            if ( empty($this->et->body['dataResponse']['descripcion']) ) {
                $this->et->body['dataResponse']['codigo'] = $this->et::$errorTecnicoCod;
                $this->et->body['dataResponse']['descripcion'] = $this->et::$errorTecnicoDesc;
            }
            Log::error('FACILITO - INDEX (ERROR): '.$th->getMessage());
        }

        return response()->json($this->et);
    }

    public function consulta(Request $request)
    {
        try{
            $data = json_decode($request->getContent(), true);
            Log::info('FACILITO - CONSULTA (REQUEST):', $data);
            $this->et = $this->ServiciosFacilitoBLL->consulta($this->idEmpresa, $this->idAgencia, $this->idUsuario, $data);

            if($this->et->body['dataResponse']['codigo'] != '00'){
                throw new \Exception($this->et->body['dataResponse']['descripcion'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->body['dataResponse']['descripcion'] = $this->et::$transaccionExitosaDesc;
            Log::info('FACILITO - CONSULTA (RESPONSE):', $this->et->body);
        } catch (\Throwable $th) {
            if ( empty($this->et->body['dataResponse']['descripcion']) ) {
                $this->et->body['dataResponse']['codigo'] = $this->et::$errorTecnicoCod;
                $this->et->body['dataResponse']['descripcion'] = $this->et::$errorTecnicoDesc;
            }
            Log::error('FACILITO - CONSULTA (ERROR): '.$th->getMessage());
        }

        return response()->json($this->et);
    }
    
    public function pago(Request $request)
    {
        try{
            $data = json_decode($request->getContent(), true);
            Log::info('FACILITO - PAGO (REQUEST):', $data);
            $this->et = $this->ServiciosFacilitoBLL->pago($this->idEmpresa, $this->idAgencia, $this->idUsuario, $data);

            if($this->et->body['dataResponse']['codigo'] != '00'){
                throw new \Exception($this->et->body['dataResponse']['descripcion'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->body['dataResponse']['descripcion'] = $this->et::$transaccionExitosaDesc;
            Log::info('FACILITO - PAGO (RESPONSE):', $this->et->body);
        } catch (\Throwable $th) {
            if ( empty($this->et->body['dataResponse']['descripcion']) ) {
                $this->et->body['dataResponse']['codigo'] = $this->et::$errorTecnicoCod;
                $this->et->body['dataResponse']['descripcion'] = $this->et::$errorTecnicoDesc;
            }
            Log::error('FACILITO - PAGO (ERROR): '.$th->getMessage());
        }

        return response()->json($this->et);
    }
    
    public function reverso(Request $request)
    {
        try{
            $data = json_decode($request->getContent(), true);
            Log::info('FACILITO - REVERSO (REQUEST):', $data);
            $this->et = $this->ServiciosFacilitoBLL->reverso($this->idEmpresa, $this->idAgencia, $this->idUsuario, $data);

            if($this->et->body['dataResponse']['codigo'] != '00'){
                throw new \Exception($this->et->body['dataResponse']['descripcion'] ?? $this->et::$errorTecnicoDesc);
            }
            $this->et->body['dataResponse']['descripcion'] = $this->et::$transaccionExitosaDesc;
            Log::info('FACILITO - REVERSO (RESPONSE):', $this->et->body);
        } catch (\Throwable $th) {
            if ( empty($this->et->body['dataResponse']['descripcion']) ) {
                $this->et->body['dataResponse']['codigo'] = $this->et::$errorTecnicoCod;
                $this->et->body['dataResponse']['descripcion'] = $this->et::$errorTecnicoDesc;
            }
            Log::error('FACILITO - REVERSO (ERROR): '.$th->getMessage());
        }

        return response()->json($this->et);
    }
}
