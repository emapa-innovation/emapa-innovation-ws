<?php

namespace App\Http\Controllers\Seguridad;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use App\Helpers\EstadoTransaccion;

class UtilitiesController
{
    private $et, $modulo, $opcion;

    public function __construct(Request $request)
    {
        $this->et = new EstadoTransaccion();

        $this->modulo = 'Seguridad';
        $this->opcion = 'Utilidades';
    }

    public function getFile($urlFile){
        try{
            $urlFile = Crypt::decryptString($urlFile);
            $contentType = Storage::mimeType($urlFile);
            $nameFile = basename($urlFile);
            $content = Storage::get($urlFile);

            return response($content)
                    ->header('Content-Type', $contentType)
                    ->header('Pragma','public')
                    ->header('Content-Disposition','inline; filename="'.$nameFile.'"')
                    ->header('Cache-Control','max-age=60, must-revalidate');
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function getFileBase64($urlFile){
        try{
            $urlFile = Crypt::decryptString($urlFile);
            $content = Storage::get($urlFile);

            $this->et->data = base64_encode($content);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }

            $this->et->mensaje = $this->et::$procesoExitoso;
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function getFileURL($urlFile){
        try{
            $this->et->data = Crypt::decryptString($urlFile);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }

            $this->et->mensaje = $this->et::$procesoExitoso;
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }
}
