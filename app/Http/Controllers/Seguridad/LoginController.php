<?php

namespace App\Http\Controllers\Seguridad;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\AuthToken\JWToken;
use App\Http\BusinessLayer\Seguridad\LoginBLL;
use App\Helpers\EstadoTransaccion;
use Illuminate\Support\Facades\Log;
use App\Helpers\EstadoTransaccionBanred;
use App\Helpers\EstadoTransaccionRecaudacion;

class LoginController
{
	private $et, $modulo, $opcion, $etBanred, $etRecaudacion;

	public function __construct()
	{
		$this->et = new EstadoTransaccion();
		$this->etBanred = new EstadoTransaccionBanred();
		$this->etRecaudacion = new EstadoTransaccionRecaudacion();
		$this->modulo = 'seguridad';
		$this->opcion = 'login';
	}

	public function login(Request $request)
	{
		try {
			$LoginBLL = new LoginBLL();
			$objJWToken = new JWToken();
            
            $request = json_decode($request->getContent(), true);
            
            $this->et = $this->validarData('LOGIN', $request);
			if($this->et->existeError) {
				throw new \Exception($this->et->mensaje);
            }
            
			$usrName   = $request['username'];
			$usrPass   = $request['password'];
            $idEmpresa = $request['idEmpresa'];
            $idAgencia = $request['idAgencia'];
            
			$usrPass = $objJWToken->cifrarPassword($usrName, $usrPass);
			
			$this->et = $LoginBLL->login($idEmpresa, $idAgencia, [
				'opcion' => 'LOGIN',
				'username' => $usrName,
				'password' => $usrPass
			]);
			if($this->et->existeError) {
				throw new \Exception($this->et->mensaje);
            }
			$rsUsuario = $this->et->data;
			
			$this->et = $LoginBLL->generarMenu($idEmpresa, $rsUsuario->idUsuario, $idAgencia);
			if($this->et->existeError) {
				throw new \Exception('NO SE PUDO GENERAR MENU DE OPCIONES PARA USUARIO');
            }
			$rsMenu = $this->et->data;
            
			$token = $objJWToken->generarToken($idEmpresa, $rsUsuario->idUsuario, $idAgencia);
			$this->et->data =  [
				'token' => $token,
				'user' 	=> $rsUsuario,
				'menu'  => $rsMenu
			];
		} catch (\Throwable $th) {
            $this->et->existeError = true;
			if( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
			}
			Log::error($th->getMessage());
			// $this->guardarErrorMongo($request, 'login', $th);
		}
		return response()->json($this->et);
	}

	public function logout(Request $request)
	{
		try {
			$obj_JWToken = new JWToken();
			$token = $request->header('Authorization');

			$obj_JWToken->destruirToken($token, NULL, 0);

			$this->et->mensaje = 'Sesión cerrada exitósamente';
		} catch (\Throwable $th) {
			$this->et->existeError = true;
			if( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
			}
            // 'Error: ' . get_class($this) . '->logout : ' . $th->getMessage();
        }
        
		return response()->json($this->et);
	}

	public function loginBanred(Request $request)
	{
		try {
			$LoginBLL = new LoginBLL();
			$objJWToken = new JWToken();
            
            $request = json_decode($request->getContent(), true);
            
            $etValidacion = $this->validarData('LOGIN', $request['infoOperation']);
			if($etValidacion->existeError) {
				$responseArray = [
					'error_code' => $this->etBanred::$datosIncompletosCod,
					'message' => $this->etBanred::$datosIncompletosDesc
				];
				return response()->json($responseArray, 401, ['X-Header-Token' => $this->etBanred::$datosIncompletosDesc]);
            }
            
			$usrName   = $request['infoOperation']['username'];
			$usrPass   = $request['infoOperation']['password'];
            $idEmpresa = $request['infoOperation']['idEmpresa'];
            $idAgencia = $request['infoOperation']['idAgencia'];
            
			$usrPass = $objJWToken->cifrarPassword($usrName, $usrPass);
			
			$etLogin = $LoginBLL->login($idEmpresa, $idAgencia, [
				'opcion' => 'LOGIN',
				'username' => $usrName,
				'password' => $usrPass
			]);
			if($etLogin->existeError) {
				$responseArray = [
					'error_code' => $this->etBanred::$datosIncorrectosCod,
					'message' => $this->etBanred::$datosIncorrectosDesc.' - '.$etLogin->mensaje
				];
				return response()->json($responseArray, 401, ['X-Header-Token' => $this->etBanred::$datosIncorrectosDesc]);
            }
			$rsUsuario = $etLogin->data;
			
			$token = $objJWToken->generarToken($idEmpresa, $rsUsuario->idUsuario, $idAgencia);
			$fechaActual = date_create();

			$responseArray = [
				'access_token' => $token,
				'token_type' => 'Basic',
				'expires_in' => JWToken::getDateExpirationToken($token) - $fechaActual->getTimestamp(),
				'refresh_token' => $token,
				'scope' => 'create'
			];
			return response()->json($responseArray);
		} catch (\Throwable $th) {
			Log::error($th->getMessage());
			$responseArray = [
				'error_code' => $this->etBanred::$errorTecnicoCod,
				'message' => $this->etBanred::$errorTecnicoDesc
			];
			return response()->json($responseArray, 401, ['X-Header-Token' => $this->etBanred::$errorTecnicoDesc]);
		}
	}

	public function loginServipagos(Request $request)
	{
		try {
			$LoginBLL = new LoginBLL();
			$objJWToken = new JWToken();
            
            $request = json_decode($request->getContent(), true);
            
            $etValidacion = $this->validarData('LOGIN', $request['body']['dataOperacion']);
			if($etValidacion->existeError) {
				$this->etRecaudacion->body['dataResponse'] = [
					'codigo' => $this->etRecaudacion::$datosIncompletosCod,
					'descripcion' => $this->etRecaudacion::$datosIncompletosDesc
				];
				return response()->json($this->etRecaudacion, 401, ['X-Header-Token' => $this->etRecaudacion::$datosIncompletosDesc]);
            }

			$usrName   = $request['body']['dataOperacion']['username'];
			$usrPass   = $request['body']['dataOperacion']['password'];
            $idEmpresa = $request['body']['dataOperacion']['idEmpresa'];
            $idAgencia = $request['body']['dataOperacion']['idAgencia'];
            
			$usrPass = $objJWToken->cifrarPassword($usrName, $usrPass);
			
			$etLogin = $LoginBLL->login($idEmpresa, $idAgencia, [
				'opcion' => 'LOGIN',
				'username' => $usrName,
				'password' => $usrPass
			]);
			if($etLogin->existeError) {
				$this->etRecaudacion->body['dataResponse'] = [
					'codigo' => $this->etRecaudacion::$datosIncorrectosCod,
					'descripcion' => $this->etRecaudacion::$datosIncorrectosDesc.' - '.$etLogin->mensaje
				];
				return response()->json($this->etRecaudacion, 401, ['X-Header-Token' => $this->etRecaudacion::$datosIncorrectosDesc]);
            }
			$rsUsuario = $etLogin->data;
			
			$token = $objJWToken->generarToken($idEmpresa, $rsUsuario->idUsuario, $idAgencia);
			$fechaActual = date_create();

			$this->etRecaudacion->body['dataOperacion'] = [
				'token' => $token,
				'token_type' => 'Basic',
				'expires_in' => JWToken::getDateExpirationToken($token) - $fechaActual->getTimestamp(),
				'scope' => 'create'
			];
			$this->etRecaudacion->body['dataResponse']['descripcion'] = $this->etRecaudacion::$transaccionExitosaDesc;
			return response()->json($this->etRecaudacion);
		} catch (\Throwable $th) {
			Log::error($th->getMessage());
			$this->etRecaudacion->body['dataResponse'] = [
				'codigo' => $this->etRecaudacion::$errorTecnicoCod,
				'descripcion' => $this->etRecaudacion::$errorTecnicoDesc
			];
			return response()->json($this->etRecaudacion, 401, ['X-Header-Token' => $this->etRecaudacion::$errorTecnicoDesc]);
		}
	}
	
	public function loginFacilito(Request $request)
	{
		try {
			$LoginBLL = new LoginBLL();
			$objJWToken = new JWToken();
            
            $request = json_decode($request->getContent(), true);
            
            $etValidacion = $this->validarData('LOGIN', $request['body']['dataOperacion']);
			if($etValidacion->existeError) {
				$this->etRecaudacion->body['dataResponse'] = [
					'codigo' => $this->etRecaudacion::$datosIncompletosCod,
					'descripcion' => $this->etRecaudacion::$datosIncompletosDesc
				];
				return response()->json($this->etRecaudacion, 401, ['X-Header-Token' => $this->etRecaudacion::$datosIncompletosDesc]);
            }

			$usrName   = $request['body']['dataOperacion']['username'];
			$usrPass   = $request['body']['dataOperacion']['password'];
            $idEmpresa = $request['body']['dataOperacion']['idEmpresa'];
            $idAgencia = $request['body']['dataOperacion']['idAgencia'];
            
			$usrPass = $objJWToken->cifrarPassword($usrName, $usrPass);
			
			$etLogin = $LoginBLL->login($idEmpresa, $idAgencia, [
				'opcion' => 'LOGIN',
				'username' => $usrName,
				'password' => $usrPass
			]);
			if($etLogin->existeError) {
				$this->etRecaudacion->body['dataResponse'] = [
					'codigo' => $this->etRecaudacion::$datosIncorrectosCod,
					'descripcion' => $this->etRecaudacion::$datosIncorrectosDesc.' - '.$etLogin->mensaje
				];
				return response()->json($this->etRecaudacion, 401, ['X-Header-Token' => $this->etRecaudacion::$datosIncorrectosDesc]);
            }
			$rsUsuario = $etLogin->data;
			
			$token = $objJWToken->generarToken($idEmpresa, $rsUsuario->idUsuario, $idAgencia);
			$fechaActual = date_create();

			$this->etRecaudacion->body['dataOperacion'] = [
				'token' => $token,
				'token_type' => 'Basic',
				'expires_in' => JWToken::getDateExpirationToken($token) - $fechaActual->getTimestamp(),
				'scope' => 'create'
			];
			$this->etRecaudacion->body['dataResponse']['descripcion'] = $this->etRecaudacion::$transaccionExitosaDesc;
			return response()->json($this->etRecaudacion);
		} catch (\Throwable $th) {
			Log::error($th->getMessage());
			$this->etRecaudacion->body['dataResponse'] = [
				'codigo' => $this->etRecaudacion::$errorTecnicoCod,
				'descripcion' => $this->etRecaudacion::$errorTecnicoDesc
			];
			return response()->json($this->etRecaudacion, 401, ['X-Header-Token' => $this->etRecaudacion::$errorTecnicoDesc]);
		}
	}
	
	public function loginSoisem(Request $request)
	{
		try {
			$LoginBLL = new LoginBLL();
			$objJWToken = new JWToken();
            
            $request = json_decode($request->getContent(), true);
            
            $etValidacion = $this->validarData('LOGIN', $request['body']['dataOperacion']);
			if($etValidacion->existeError) {
				$this->etRecaudacion->body['dataResponse'] = [
					'codigo' => $this->etRecaudacion::$datosIncompletosCod,
					'descripcion' => $this->etRecaudacion::$datosIncompletosDesc
				];
				return response()->json($this->etRecaudacion, 401, ['X-Header-Token' => $this->etRecaudacion::$datosIncompletosDesc]);
            }

			$usrName   = $request['body']['dataOperacion']['username'];
			$usrPass   = $request['body']['dataOperacion']['password'];
            $idEmpresa = $request['body']['dataOperacion']['idEmpresa'];
            $idAgencia = $request['body']['dataOperacion']['idAgencia'];
            
			$usrPass = $objJWToken->cifrarPassword($usrName, $usrPass);
			
			$etLogin = $LoginBLL->login($idEmpresa, $idAgencia, [
				'opcion' => 'LOGIN',
				'username' => $usrName,
				'password' => $usrPass
			]);
			if($etLogin->existeError) {
				$this->etRecaudacion->body['dataResponse'] = [
					'codigo' => $this->etRecaudacion::$datosIncorrectosCod,
					'descripcion' => $this->etRecaudacion::$datosIncorrectosDesc.' - '.$etLogin->mensaje
				];
				return response()->json($this->etRecaudacion, 401, ['X-Header-Token' => $this->etRecaudacion::$datosIncorrectosDesc]);
            }
			$rsUsuario = $etLogin->data;
			
			$token = $objJWToken->generarToken($idEmpresa, $rsUsuario->idUsuario, $idAgencia);
			$fechaActual = date_create();

			$this->etRecaudacion->body['dataOperacion'] = [
				'token' => $token,
				'token_type' => 'Basic',
				'expires_in' => JWToken::getDateExpirationToken($token) - $fechaActual->getTimestamp(),
				'scope' => 'create'
			];
			$this->etRecaudacion->body['dataResponse']['descripcion'] = $this->etRecaudacion::$transaccionExitosaDesc;
			return response()->json($this->etRecaudacion);
		} catch (\Throwable $th) {
			Log::error($th->getMessage());
			$this->etRecaudacion->body['dataResponse'] = [
				'codigo' => $this->etRecaudacion::$errorTecnicoCod,
				'descripcion' => $this->etRecaudacion::$errorTecnicoDesc
			];
			return response()->json($this->etRecaudacion, 401, ['X-Header-Token' => $this->etRecaudacion::$errorTecnicoDesc]);
		}
	}

	private function validarData($opcion, $request)
	{
		$reglas = [];
        switch (strtoupper($opcion)) {
            case 'LOGIN':
                $reglas = [
                    'username'  => 'required|max:100',
                    'password'  => 'required|max:20',
                    'idEmpresa' => 'required|numeric',
                    'idAgencia' => 'required|numeric',
                ];
                break;
        }

		$validacion = Validator::make($request, $reglas);
        
        $errores = '';
		if ($validacion->fails()){
			$this->et->existeError = true;
			foreach ($validacion->messages()->all() as $mensaje) {
				$errores .= $mensaje . '<br/>';
			}
			$this->et->mensaje = $errores;
        }
        
		return $this->et;
	}

	// private function guardarErrorMongo($dataUsuario, string $metodo, $exception)
	// {
	// 	if (is_array($this->et->mensaje)) {
	// 		if (!array_key_exists('user', $this->et->mensaje)) {
	// 			$this->et->mensaje = [
	// 				'user' => EstadoTransaccion::$procesoErroneo,
	// 			];
	// 			$this->mensaje = ('Error: ' . className($this) . '->' . $metodo . ' : ' . $exception->getMessage());
	// 		} else {
	// 			$this->et->mensaje = [
	// 				'user' => $exception->getMessage(),
	// 			];
	// 			$this->mensaje = 'Error: ' . className($this) . '->' . $metodo . ' : ' . $exception->getMessage();
	// 		}
	// 	} else {
	// 		$this->mensaje = 'Error: ' . className($this) . '->' . $metodo . ' : ' . $exception->getMessage();
	// 		$this->et->mensaje = [
	// 			'user' => EstadoTransaccion::$procesoErroneo,
	// 		];
	// 	}
	// 	$this->et->existeError = true;
	// 	saveLog([
	// 		'exception'   => $this->mensaje,
	// 		'mensaje'     => $this->et->mensaje,
	// 		'cod_empresa' => $this->idEmpresa,
	// 		'cod_usuario' => $this->idUsuario,
	// 		'modulo'      => $this->modulo,
	// 		'opcion'      => $this->opcion,
	// 		'accion'      => $metodo,
	// 		'data'        => $dataUsuario,
	// 	]);
	// }
}