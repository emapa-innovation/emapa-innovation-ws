<?php

namespace App\Http\Controllers\Seguridad\Parametros;

use Illuminate\Http\Request;
use App\Helpers\EstadoTransaccion;
use App\AuthToken\JWToken;
use App\Http\BusinessLayer\Seguridad\Parametros\CalificacionClienteBLL;
use Illuminate\Support\Facades\Log;

class CalificacionClienteController
{
    private $et, $idEmpresa, $idUsuario, $modulo, $opcion, $CalificacionClienteBLL;

    public function __construct(Request $request)
    {
        $this->et = new EstadoTransaccion();
        $this->CalificacionClienteBLL = new CalificacionClienteBLL;
        $userInfo = JWToken::informacionToken($request);
        $this->idUsuario = $userInfo->idUsuario;
        $this->idEmpresa = $userInfo->idEmpresa;
        $this->modulo = 'Seguridad';
        $this->opcion = 'Calificacion del cliente';
    }

    public function index()
    {
        try {
            $this->et = $this->CalificacionClienteBLL->index($this->idEmpresa);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }

            if( empty($this->et->data) ){
                $this->et->mensaje = $this->et::$noExistenDatos; 
            }
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }

            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function listar(Request $request){
        try{
            $data = json_decode($request->getContent(), true);
            $this->et = $this->CalificacionClienteBLL->listar($this->idEmpresa, $this->idUsuario, $data);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function guardar(Request $request){
        try{
            $data = json_decode($request->getContent(), true);
            $this->et = $this->CalificacionClienteBLL->guardar($this->idEmpresa, $this->idUsuario, $data);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }

            $this->et->mensaje = $this->et::$procesoExitoso;
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }
}
