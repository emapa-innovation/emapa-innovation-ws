<?php

namespace App\Http\Controllers\BalconServicios;

use Illuminate\Http\Request;
use App\Helpers\EstadoTransaccion;
use App\Http\BusinessLayer\BalconServicios\ActualizacionDatosBLL;
use Illuminate\Support\Facades\Log;

class ActualizacionDatosController
{
    private $et, $idEmpresa, $idUsuario, $modulo, $opcion, $ActualizacionDatosBLL;

    public function __construct(Request $request)
    {
        $this->et = new EstadoTransaccion();
        $this->ActualizacionDatosBLL = new ActualizacionDatosBLL;
        $this->modulo = 'Balcon de Servicios';
        $this->opcion = 'Actualizacion de Datos';
    }

    public function index()
    {
        try {
            $this->et = $this->ActualizacionDatosBLL->index($this->idEmpresa);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }

            if( empty($this->et->data) ){
                $this->et->mensaje = $this->et::$noExistenDatos; 
            }
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }

            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function listar(Request $request){
        try {
            $data = json_decode($request->getContent(), true);
            $this->et = $this->ActualizacionDatosBLL->listar($this->idEmpresa, $this->idUsuario, $data);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function guardar(Request $request){
        try{
            $data = json_decode($request->getContent(), true);
            $this->et = $this->ActualizacionDatosBLL->guardar($this->idEmpresa, $this->idUsuario, $data);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }

            $this->et->mensaje = $this->et::$procesoExitoso;
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }
}
