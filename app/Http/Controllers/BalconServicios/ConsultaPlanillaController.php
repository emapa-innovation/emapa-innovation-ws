<?php

namespace App\Http\Controllers\BalconServicios;

use Illuminate\Http\Request;
use App\Helpers\EstadoTransaccion;
use App\Http\BusinessLayer\BalconServicios\ConsultaPlanillaBLL;
use Illuminate\Support\Facades\Log;

class ConsultaPlanillaController
{
    private $et, $idEmpresa, $idUsuario, $modulo, $opcion, $ConsultaPlanillaBLL;

    public function __construct(Request $request)
    {
        $this->et = new EstadoTransaccion();
        $this->ConsultaPlanillaBLL = new ConsultaPlanillaBLL;
        // $userInfo = JWToken::informacionToken($request);
        // $this->idUsuario = $userInfo->idUsuario;
        // $this->idEmpresa = $userInfo->idEmpresa;
        $this->modulo = 'Balcon de Servicios';
        $this->opcion = 'Consulta de Planillas';
    }

    public function index()
    {
        try {
            $this->et = $this->ConsultaPlanillaBLL->index($this->idEmpresa);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }

            if( empty($this->et->data) ){
                $this->et->mensaje = $this->et::$noExistenDatos; 
            }
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }

            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function listar(Request $request){
        try {
            $data = json_decode($request->getContent(), true);
            $this->et = $this->ConsultaPlanillaBLL->listar($this->idEmpresa, $this->idUsuario, $data);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }

    public function exportar(Request $request){
        try {
            $data = json_decode($request->getContent(), true);
            $this->et = $this->ConsultaPlanillaBLL->listar($this->idEmpresa, $this->idUsuario, $data);

            if($this->et->existeError){
                throw new \Exception($this->et->mensaje ?? $this->et::$procesoErroneo);
            }
            
            $pdf = \PDF::loadView('BalconServicios.ConsultaPlanilla.PDF.planilla', [ 
                'data' => $this->et->data[0],
                'chartConsumosBase64' => $data['chartConsumosBase64']
            ]);

            return $pdf->download('planilla.pdf');
        } catch (\Throwable $th) {
            $this->et->existeError = true;
            if ( empty($this->et->mensaje) ) {
                $this->et->mensaje = $this->et::$procesoErroneo;
            }
            Log::error($th->getMessage());
        }

        return response()->json($this->et);
    }
}
