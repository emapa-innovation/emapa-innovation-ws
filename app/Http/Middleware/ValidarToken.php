<?php

namespace App\Http\Middleware;

use App\AuthToken\JWToken;
use App\Helpers\EstadoTransaccion;
use Closure;

class ValidarToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $obj_JWToken = new JWToken;
        $et = new EstadoTransaccion();

        $token = $request->header('Authorization');
        
        if (! $token) {
            $et->existeError = true;
            $et->mensaje = 'No se ha enviado ningún token';
            return response()->json($et, 401, ['X-Header-Token' => 'Invalid Token']);
        } else {
            $datos = $obj_JWToken->validarToken($token);
            if ($datos['valido']) {
               
               return $next($request);
            } else {
                $et->existeError = true;
                $et->mensaje = 'El token enviado no es valido';
                return response()->json($et, 401, ['X-Header-Token' => 'Invalid Token']);
            }
        }
        return response()->json($et);
    }
}