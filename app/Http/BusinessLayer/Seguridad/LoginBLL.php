<?php

namespace App\Http\BusinessLayer\Seguridad;

use App\Helpers\EstadoTransaccion;
use App\Http\Repositories\Seguridad\LoginRepository;
use App\Http\Repositories\Seguridad\PerfilOpcionRepository;

class LoginBLL
{
	public function login($idEmpresa, $idAgencia, $data)
	{
		try {
			$et = new EstadoTransaccion();
			$LoginRepository = new LoginRepository($data);

			$rsLogin = $LoginRepository->login($idEmpresa, $idAgencia);
	        			
			if( empty($rsLogin) ) {
				$et->existeError = true;
				$et->mensaje = $et::$noExistenDatos;
				
				return $et;
			}
			
			if( $rsLogin[0]->error == 1 ) {
				$et->existeError = true;
				$et->mensaje = $rsLogin[0]->mensaje;
				
				return $et;
			}

			$et->data = $rsLogin[0];
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->login : ' . $th->getMessage());
		}

		return $et;
	}

	public function generarMenu($idEmpresa, $idUsuario, $idAgencia)
	{
		try {
			$et = new EstadoTransaccion();

			$PerfilOpcionRepository = new PerfilOpcionRepository([
				'opcion' => 'OPCIONES_USUARIO',
				'idUsuario' => $idUsuario
			]);

			$et->data = getMenuOpciones($PerfilOpcionRepository->listar($idEmpresa, $idUsuario));
		} catch (\Throwable $th) {
			throw new \Exception(' : ' . get_class($this) . '->generarMenu : ' . $th->getMessage());
		}

		return $et;
	}
}
