<?php

namespace App\Http\BusinessLayer\Seguridad\Parametros;

use Illuminate\Support\Facades\DB;
use App\Helpers\EstadoTransaccion;
use App\Http\Repositories\Seguridad\Parametros\ContratoServicioRepository;
use App\Http\Repositories\General\DistritoRepository;

class ContratoServicioBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $DistritoRepository = new DistritoRepository([
                'opcion' => 'LISTAR'
            ]);

            $et->data = [
                'porcentajeIVA' => 0.12,
                'listaTipo' => [ 
                    [ 'valor' => 'AG', 'descripcion' => 'AGUA' ],
                    [ 'valor' => 'AL', 'descripcion' => 'ALCANTARILLADO' ],
                ],
                'listaClase' => [
                    [ 'valor' => 'C', 'descripcion' => 'CONTRATO' ],
                    [ 'valor' => 'S', 'descripcion' => 'SERVICIO' ],
                ],
                'listaSiNo' => [
                    [ 'valor' => 'S', 'descripcion' => 'SI' ],
                    [ 'valor' => 'N', 'descripcion' => 'NO' ],
                ],
                'listaDistrito' => $DistritoRepository->listar($idEmpresa, null),
                'listaEstados' => [
                    [ 'valor' => 'A', 'descripcion' => 'ACTIVO' ],
                    [ 'valor' => 'I', 'descripcion' => 'INACTIVO' ],
                ]
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $ContratoServicioRepository = new ContratoServicioRepository($data);

            $et->data = $ContratoServicioRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }

    public function guardar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            DB::beginTransaction();

            $ContratoServicioRepository = new ContratoServicioRepository($data);
            $et->data = $ContratoServicioRepository->guardar($idEmpresa, $idUsuario);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
        }

        return $et;
    }
}
