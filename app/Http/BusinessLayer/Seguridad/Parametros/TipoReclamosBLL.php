<?php

namespace App\Http\BusinessLayer\Seguridad\Parametros;

use Illuminate\Support\Facades\DB;
use App\Helpers\EstadoTransaccion;
use App\Http\Repositories\Seguridad\Parametros\TipoReclamosRepository;
use App\Http\Repositories\Seguridad\Parametros\ContratoServicioRepository;

class TipoReclamosBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $ContratoServicioRepository = new ContratoServicioRepository([
                'opcion' => 'LISTAR_TIPO_RECLAMOS'
            ]);

            $et->data = [
                'listaServicioAsociado' => $ContratoServicioRepository->listar($idEmpresa, null),
                'listaEstados' => [
                    [ 'valor' => 'A', 'descripcion' => 'ACTIVO' ],
                    [ 'valor' => 'I', 'descripcion' => 'INACTIVO' ],
                ]
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $TipoReclamosRepository = new TipoReclamosRepository($data);

            $et->data = $TipoReclamosRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }

    public function guardar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            DB::beginTransaction();

            $TipoReclamosRepository = new TipoReclamosRepository($data);
            $et->data = $TipoReclamosRepository->guardar($idEmpresa, $idUsuario);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
        }

        return $et;
    }
}
