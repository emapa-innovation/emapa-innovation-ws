<?php

namespace App\Http\BusinessLayer\Seguridad\Parametros;

use Illuminate\Support\Facades\DB;
use App\Helpers\EstadoTransaccion;
use App\Http\Repositories\Seguridad\Parametros\ClaseClienteRepository;

class ClaseClienteBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $et->data = [
                'listaEstados' => [
                    [ 'valor' => 'A', 'descripcion' => 'ACTIVO' ],
                    [ 'valor' => 'I', 'descripcion' => 'INACTIVO' ],
                ]
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $ClaseClienteRepository = new ClaseClienteRepository($data);

            $et->data = $ClaseClienteRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }

    public function guardar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            DB::beginTransaction();

            $ClaseClienteRepository = new ClaseClienteRepository($data);
            $et->data = $ClaseClienteRepository->guardar($idEmpresa, $idUsuario);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
        }

        return $et;
    }
}
