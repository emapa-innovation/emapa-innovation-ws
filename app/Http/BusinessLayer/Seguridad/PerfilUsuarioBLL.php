<?php

namespace App\Http\BusinessLayer\Seguridad;

use Illuminate\Support\Facades\DB;
use App\Helpers\EstadoTransaccion;
use App\Http\Repositories\Seguridad\PerfilUsuarioRepository;
use App\Http\Repositories\Seguridad\PerfilOpcionRepository;

class PerfilUsuarioBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $PerfilOpcionRepository = new PerfilOpcionRepository([
				'opcion' => 'LISTAR_OPCIONES'
			]);

			$rsMenuOpciones = getMenuOpciones($PerfilOpcionRepository->listar($idEmpresa, null));

            $et->data = [
                'listaEstados' => [
                    [ 'valor' => 'A', 'descripcion' => 'ACTIVO' ],
                    [ 'valor' => 'I', 'descripcion' => 'INACTIVO' ],
                    [ 'valor' => 'B', 'descripcion' => 'BLOQUEADO' ],
                ],
                'listaMenuOpciones' => $rsMenuOpciones
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $PerfilUsuarioRepository = new PerfilUsuarioRepository($data);

            $et->data = $PerfilUsuarioRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }

    public function guardar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            DB::beginTransaction();

            $PerfilUsuarioRepository = new PerfilUsuarioRepository($data);
            $et->data = $PerfilUsuarioRepository->guardar($idEmpresa, $idUsuario);

            if( $et->data[0]->respuesta == 0 ) {
                $et->existeError = true;
                $et->mensaje = $et->data[0]->mensaje;
                
                return $et;
            }

            if (isset($data['listaOpcionesPerfil'])) {
                $PerfilOpcionRepository = new PerfilOpcionRepository([
                    'opcion' => 'DELETE_OPCIONES_PERFIL',
                    'idPerfil' => $et->data[0]->id
                ]);
                $PerfilOpcionRepository->guardar($idEmpresa, $idUsuario);
                
                foreach ($data['listaOpcionesPerfil'] as $opcion) {
                    unset($PerfilOpcionRepository);
                    $PerfilOpcionRepository = new PerfilOpcionRepository([
                        'opcion' => 'I',
                        'idPerfil' => $et->data[0]->id,
                        'idOpcion' => $opcion['idOpcion']
                    ]);
                    $rsOpcionPerfil = $PerfilOpcionRepository->guardar($idEmpresa, $idUsuario);
    
                    if ( $rsOpcionPerfil[0]->respuesta == 0 ) {
                        DB::rollBack();
                        $et->existeError = true;
                        $et->mensaje = !empty($rsOpcionPerfil[0]->mensaje) ? $rsOpcionPerfil[0]->mensaje : 'ERROR AL GUARDAR OPCIONES';
                        return $et;
                    }
                }
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
        }

        return $et;
    }
}
