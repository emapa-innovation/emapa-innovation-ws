<?php

namespace App\Http\BusinessLayer\Seguridad;

use Illuminate\Support\Facades\DB;
use App\Helpers\EstadoTransaccion;
use App\Http\Repositories\Seguridad\UsuarioRepository;
use App\Http\Repositories\Seguridad\PerfilUsuarioRepository;
use App\AuthToken\JWToken;

class UsuarioBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $PerfilUsuarioRepository = new PerfilUsuarioRepository([
				'opcion' => 'LISTAR_CMB'
			]);
			$rsListaPerfiles = $PerfilUsuarioRepository->listar($idEmpresa, null);

            $et->data = [
                'listaPerfiles' => $rsListaPerfiles,
                'listaEstados' => [
                    [ 'valor' => 'A', 'descripcion' => 'ACTIVO' ],
                    [ 'valor' => 'I', 'descripcion' => 'INACTIVO' ],
                    [ 'valor' => 'B', 'descripcion' => 'BLOQUEADO' ],
                ]
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $UsuarioRepository = new UsuarioRepository($data);

            $et->data = $UsuarioRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }

    public function guardar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();
        $objJWToken = new JWToken();

        try {
            DB::beginTransaction();

            $UsuarioRepository = new UsuarioRepository($data);
            $et->data = $UsuarioRepository->guardar($idEmpresa, $idUsuario);

            if ( !empty($data['flagCambiarClave']) ) {
                unset($UsuarioRepository);
                $UsuarioRepository = new UsuarioRepository([
                    'opcion' => 'CAMBIAR_CLAVE',
                    'idUsuario' => $data['idUsuario'],
                    'clave' => $objJWToken->cifrarPassword($data['usuario'], $data['clave'])
                ]);
                $et->data = $UsuarioRepository->guardar($idEmpresa, $idUsuario);
            }

            if ( !empty($data['flagModificarPerfiles']) ) {
                $PerfilUsuarioRepository = new PerfilUsuarioRepository([
                    'opcion' => 'ELIMINAR_PERFILES_USUARIO',
                    'idUsuario' => $data['idUsuario']
                ]);
                $PerfilUsuarioRepository->guardar($idEmpresa, $idUsuario);

                foreach ($data['listaPerfilesUsuario'] as $perfil) {
                    unset($PerfilUsuarioRepository);
                    $PerfilUsuarioRepository = new PerfilUsuarioRepository([
                        'opcion' => 'GUARDAR_PERFIL_USUARIO',
                        'idUsuario' => $data['idUsuario'],
                        'idPerfil' => $perfil['valor']
                    ]);
                    $rsPerfilUsuario = $PerfilUsuarioRepository->guardar($idEmpresa, $idUsuario);

                    if ( $rsPerfilUsuario[0]->respuesta == 0 ) {
                        DB::rollBack();
                        $et->existeError = true;
                        $et->mensaje = !empty($rsPerfilUsuario[0]->mensaje) ? $rsPerfilUsuario[0]->mensaje : 'ERROR DE ASIGNACIÓN DE PERFILES';
                        return $et;
                    }
                }
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
        }

        return $et;
    }
}
