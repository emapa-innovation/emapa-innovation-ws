<?php

namespace App\Http\BusinessLayer\BalconServicios;

use App\Helpers\EstadoTransaccion;
use App\Http\Repositories\Facturacion\FacturaRepository;

class ConsultaPlanillaBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $et->data = [
                'listaTipoConsulta' => [
                    [ 'valor' => 'numeroInstalacion', 'descripcion' => 'Cuenta' ],
                    [ 'valor' => 'codigoMedidor', 'descripcion' => 'Medidor' ],
                ]
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $FacturaRepository = new FacturaRepository($data);

            $et->data = $FacturaRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }
}
