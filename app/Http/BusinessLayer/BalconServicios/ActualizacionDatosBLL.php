<?php

namespace App\Http\BusinessLayer\BalconServicios;

use Illuminate\Support\Facades\DB;
use App\Helpers\EstadoTransaccion;
use App\Http\Repositories\BalconServicios\ActualizacionDatosRepository;

class ActualizacionDatosBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $et->data = [];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $ActualizacionDatosRepository = new ActualizacionDatosRepository([
                'opcion' => 'VALIDAR_INSTALACION',
                'numeroCuenta' => $data['numeroCuenta']
            ]);
            $rsValidacionInstalacion = $ActualizacionDatosRepository->listar($idEmpresa, $idUsuario);

            if ($rsValidacionInstalacion[0]->cod_error != '00') {
                $et->existeError = true;
                $et->mensaje = $rsValidacionInstalacion[0]->desc_error;
                return $et;
            }

            $ActualizacionDatosRepository = new ActualizacionDatosRepository($data);
            $et->data = $ActualizacionDatosRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }

    public function guardar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            DB::beginTransaction();

            $ActualizacionDatosRepository = new ActualizacionDatosRepository($data);
            $et->data = $ActualizacionDatosRepository->guardar($idEmpresa, $idUsuario);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
        }

        return $et;
    }
}
