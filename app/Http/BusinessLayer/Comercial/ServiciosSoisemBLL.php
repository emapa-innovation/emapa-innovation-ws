<?php

namespace App\Http\BusinessLayer\Comercial;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use App\Helpers\EstadoTransaccionRecaudacion;
use App\Http\Repositories\Comercial\CorteReconexionRepository;
use App\Http\Repositories\Comercial\CorteReconexionEvidenciaRepository;
use App\Http\Repositories\Comercial\LecturaConsumoRepository;
use App\Http\Repositories\Comercial\LecturaConsumoEvidenciaRepository;
use App\Http\Repositories\General\CiclosRepository;
use App\Http\Repositories\General\SectorRepository;

class ServiciosSoisemBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccionRecaudacion();

        try {
            $et->body['dataOperacion'] = [];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function consultaCortes($idEmpresa, $idAgencia, $idUsuario, $data)
    {
        $et = new EstadoTransaccionRecaudacion();

        try {
            $dataRequest = [
                'opcion' => 'SOISEM_CONSULTA_PARA_CORTES'
            ];
            $CorteReconexionRepository = new CorteReconexionRepository($dataRequest);
            $rsConsulta = $CorteReconexionRepository->listar($idEmpresa, $idAgencia, $idUsuario);

            if (empty($rsConsulta)) {
                $et->body['dataResponse']['codigo'] = $et::$sinValoresPendientesCod;
                $et->body['dataResponse']['descripcion'] = $et::$noExistenDatos;
                
                return $et;
            }

            // $et->header = $data['header'];

            $et->body['dataOperacion']['tipoTransaccion'] = 'CONSULTA';
            $et->body['dataOperacion']['fechaTransaccion'] = date('Y-m-d H:i:s');
            $et->body['dataOperacion']['dataAdicional'] = $rsConsulta;
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->consulta : ' . $th->getMessage());
        }

        return $et;
    }

    public function consultaReconexiones($idEmpresa, $idAgencia, $idUsuario, $data)
    {
        $et = new EstadoTransaccionRecaudacion();

        try {
            // if (!isset($data['body']['dataOperacion']['fechaDesde']) || !isset($data['body']['dataOperacion']['fechaHasta'])) {
            //     $et->body['dataResponse']['codigo'] = $et::$datosIncompletosCod;
            //     $et->body['dataResponse']['descripcion'] = $et::$datosIncompletosDesc;

            //     return $et;
            // }

            $dataRequest = [
                'opcion' => 'SOISEM_CONSULTA_PARA_RECONEXIONES',
                // 'fechaDesde' => $data['body']['dataOperacion']['fechaDesde'] ?? NULL,
                // 'fechaHasta' => $data['body']['dataOperacion']['fechaHasta'] ?? NULL
            ];
            $CorteReconexionRepository = new CorteReconexionRepository($dataRequest);
            $rsConsulta = $CorteReconexionRepository->listar($idEmpresa, $idAgencia, $idUsuario);

            if (empty($rsConsulta)) {
                $et->body['dataResponse']['codigo'] = $et::$sinValoresPendientesCod;
                $et->body['dataResponse']['descripcion'] = $et::$noExistenDatos;
                
                return $et;
            }

            // $et->header = $data['header'];

            $et->body['dataOperacion']['tipoTransaccion'] = 'CONSULTA';
            $et->body['dataOperacion']['fechaTransaccion'] = date('Y-m-d H:i:s');
            $et->body['dataOperacion']['dataAdicional'] = $rsConsulta;
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->consulta : ' . $th->getMessage());
        }

        return $et;
    }
    
    public function corte($idEmpresa, $idAgencia, $idUsuario, $data)
    {
        $et = new EstadoTransaccionRecaudacion();

        try {
            DB::beginTransaction();

            if (!isset($data['body']['dataOperacion']['numeroCorte'])) {
                $et->body['dataResponse']['codigo'] = $et::$datosIncompletosCod;
                $et->body['dataResponse']['descripcion'] = $et::$datosIncompletosDesc;

                return $et;
            }

            $dataRequest = [
                'opcion' => 'SOISEM_INGRESO_CORTE',
                'numeroCorte' => $data['body']['dataOperacion']['numeroCorte'],
                'lecturaCorte' => $data['body']['dataOperacion']['lecturaCorte'] ?? null,
                'observacionCorte' => $data['body']['dataOperacion']['observacionCorte'] ?? null,
                'idCausaNoCorte' => $data['body']['dataOperacion']['codCausaNoCorte'] ?? null,
                'idResponsableCorte' => 10
            ];
            $CorteReconexionRepository = new CorteReconexionRepository($dataRequest);
            $rsCorte = $CorteReconexionRepository->guardar($idEmpresa, $idAgencia, $idUsuario);
            
            if( empty($rsCorte[0]->id) || $rsCorte[0]->respuesta == 0 ) {
                DB::rollback();
                $et->body['dataResponse']['codigo'] = $et::$transaccionErroneaCod;
                $et->body['dataResponse']['descripcion'] = $rsCorte[0]->mensaje ?? $et::$transaccionErroneaDesc;
                
                return $et;
            }

            // Si existen archivos de evidencia, se proceden a almacenar en servidor
            if (isset($data['body']['dataOperacion']['arrayFilesEvidencias']) && is_null($dataRequest['idCausaNoCorte'])) {
                foreach ($data['body']['dataOperacion']['arrayFilesEvidencias'] as $itemFile) {
                    $urlFileEvidencia = $this->guardarFileEvidencia(
                        $itemFile['urlFile'], 
                        'evidencias_corte', 
                        $data['body']['dataOperacion']['numeroCorte']
                    );

                    // Guardar en base de datos la url encriptada de archivo almacenado
                    $CorteReconexionEvidenciaRepository = new CorteReconexionEvidenciaRepository([
                        'opcion' => 'INSERTAR_EVIDENCIA_CORTE',
                        'idCorte' => $rsCorte[0]->id,
                        'estadoCorte' => 'C',
                        'urlEvidencia' => $urlFileEvidencia
                    ]);
                    $CorteReconexionEvidenciaRepository->guardar($idEmpresa, $idUsuario);
                }
            }

            // $et->header = $data['header'];
            $et->body['dataOperacion']['tipoTransaccion'] = 'CORTE';
            $et->body['dataOperacion']['fechaTransaccion'] = date('Y-m-d H:i:s');
            $et->body['dataOperacion']['dataAdicional'] = [
                'numeroCorte' => $data['body']['dataOperacion']['numeroCorte']
            ];

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->corte : ' . $th->getMessage());
        }

        return $et;
    }
    
    public function reconexion($idEmpresa, $idAgencia, $idUsuario, $data)
    {
        $et = new EstadoTransaccionRecaudacion();

        try {
            DB::beginTransaction();

            if (!isset($data['body']['dataOperacion']['numeroCorte'])) {
                $et->body['dataResponse']['codigo'] = $et::$datosIncompletosCod;
                $et->body['dataResponse']['descripcion'] = $et::$datosIncompletosDesc;

                return $et;
            }
            
            $dataRequest = [
                'opcion' => 'SOISEM_INGRESO_RECONEXION',
                'numeroCorte' => $data['body']['dataOperacion']['numeroCorte'],
                'lecturaReconexion' => $data['body']['dataOperacion']['lecturaReconexion'] ?? null,
                'observacionReconexion' => $data['body']['dataOperacion']['observacionReconexion'] ?? null,
                'idResponsableReconexion' => 10
            ];
            $CorteReconexionRepository = new CorteReconexionRepository($dataRequest);
            $rsReconexion = $CorteReconexionRepository->guardar($idEmpresa, $idAgencia, $idUsuario);

            if( empty($rsReconexion[0]->id) || $rsReconexion[0]->respuesta == 0 ) {
                DB::rollback();
                $et->body['dataResponse']['codigo'] = $et::$transaccionErroneaCod;
                $et->body['dataResponse']['descripcion'] = $rsReconexion[0]->mensaje ?? $et::$transaccionErroneaDesc;
                
                return $et;
            }

            // Si existen archivos de evidencia, se proceden a almacenar en servidor
            if (isset($data['body']['dataOperacion']['arrayFilesEvidencias'])) {
                foreach ($data['body']['dataOperacion']['arrayFilesEvidencias'] as $itemFile) {
                    $urlFileEvidencia = $this->guardarFileEvidencia(
                        $itemFile['urlFile'], 
                        'evidencias_reconexion', 
                        $data['body']['dataOperacion']['numeroCorte']
                    );

                    // Guardar en base de datos la url encriptada de archivo almacenado
                    $CorteReconexionEvidenciaRepository = new CorteReconexionEvidenciaRepository([
                        'opcion' => 'INSERTAR_EVIDENCIA_CORTE',
                        'idCorte' => $rsReconexion[0]->id,
                        'estadoCorte' => 'R',
                        'urlEvidencia' => $urlFileEvidencia
                    ]);
                    $CorteReconexionEvidenciaRepository->guardar($idEmpresa, $idUsuario);
                }
            }

            // $et->header = $data['header'];
            $et->body['dataOperacion']['tipoTransaccion'] = 'RECONEXION';
            $et->body['dataOperacion']['fechaTransaccion'] = date('Y-m-d H:i:s');
            $et->body['dataOperacion']['dataAdicional'] = [
                'numeroCorte' => $data['body']['dataOperacion']['numeroCorte']
            ];

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->reconexion : ' . $th->getMessage());
        }

        return $et;
    }

    public function consultaLecturas($idEmpresa, $idAgencia, $idUsuario, $data)
    {
        $et = new EstadoTransaccionRecaudacion();

        try {
            if (!isset($data['body']['dataOperacion']['codCiclo'])) {
                $et->body['dataResponse']['codigo'] = $et::$datosIncompletosCod;
                $et->body['dataResponse']['descripcion'] = $et::$datosIncompletosDesc;

                return $et;
            }

            // CONSULTA DE ID DE CICLO
            $CiclosRepository = new CiclosRepository([
                'opcion' => 'LISTAR',
                'codigo' => $data['body']['dataOperacion']['codCiclo']
            ]);
            $rsConsultaCiclo = $CiclosRepository->listar($idEmpresa, $idAgencia, $idUsuario);

            if (empty($rsConsultaCiclo)) {
                $et->body['dataResponse']['codigo'] = $et::$sinValoresPendientesCod;
                $et->body['dataResponse']['descripcion'] = $et::$noExistenDatos;
                
                return $et;
            }

            // CONSULTA DE ID DE SECTOR
            $idSector = null;
            if (isset($data['body']['dataOperacion']['codSector'])) {
                $SectorRepository = new SectorRepository([
                    'opcion' => 'LISTAR',
                    'codigo' => $data['body']['dataOperacion']['codSector']
                ]);
                $rsConsultaSector = $SectorRepository->listar($idEmpresa, $idAgencia, $idUsuario);
                $idSector = $rsConsultaSector[0]->idSector;
            }

            $dataRequest = [
                'opcion' => 'SOISEM_CONSULTA_LECTURAS',
                'idCiclo' => $rsConsultaCiclo[0]->idCiclo,
                'idSector' => $idSector
            ];

            $LecturaConsumoRepository = new LecturaConsumoRepository($dataRequest);
            $rsConsulta = $LecturaConsumoRepository->listar($idEmpresa, $idAgencia, $idUsuario);

            if (empty($rsConsulta)) {
                $et->body['dataResponse']['codigo'] = $et::$sinValoresPendientesCod;
                $et->body['dataResponse']['descripcion'] = $et::$noExistenDatos;
                
                return $et;
            }

            $et->body['dataOperacion']['tipoTransaccion'] = 'CONSULTA_LECTURAS';
            $et->body['dataOperacion']['fechaTransaccion'] = date('Y-m-d H:i:s');
            $et->body['dataOperacion']['dataAdicional'] = $rsConsulta;
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->consultaLecturas: ' . $th->getMessage());
        }

        return $et;
    }

    public function ingresoLectura($idEmpresa, $idAgencia, $idUsuario, $data)
    {
        $et = new EstadoTransaccionRecaudacion();

        try {
            DB::beginTransaction();

            if (!isset($data['body']['dataOperacion']['idLecturaConsumo'])) {
                $et->body['dataResponse']['codigo'] = $et::$datosIncompletosCod;
                $et->body['dataResponse']['descripcion'] = $et::$datosIncompletosDesc;

                return $et;
            }

            $dataRequest = [
                'opcion' => 'SOISEM_INGRESO_LECTURA',
                'idLecturaConsumo' => $data['body']['dataOperacion']['idLecturaConsumo'],
                'lecturaActual' => $data['body']['dataOperacion']['lectura'] ?? null,
                'fechaLecturaActual' => $data['body']['dataOperacion']['fechaLectura'] ?? null,
                'idCausaNoLectura' => $data['body']['dataOperacion']['codCausaNoLectura'] ?? null,
            ];
            $LecturaConsumoRepository = new LecturaConsumoRepository($dataRequest);
            $rsIngreso = $LecturaConsumoRepository->guardar($idEmpresa, $idAgencia, $idUsuario);
            
            if( empty($rsIngreso[0]->id) || $rsIngreso[0]->respuesta == 0 ) {
                DB::rollback();
                $et->body['dataResponse']['codigo'] = $et::$transaccionErroneaCod;
                $et->body['dataResponse']['descripcion'] = $rsIngreso[0]->mensaje ?? $et::$transaccionErroneaDesc;
                
                return $et;
            }

            // Si existen archivos de evidencia, se proceden a almacenar en servidor
            if (isset($data['body']['dataOperacion']['arrayFilesEvidencias'])) {
                foreach ($data['body']['dataOperacion']['arrayFilesEvidencias'] as $itemFile) {
                    $urlFileEvidencia = $this->guardarFileEvidencia(
                        $itemFile['urlFile'], 
                        'evidencias_lectura', 
                        $data['body']['dataOperacion']['idLecturaConsumo']
                    );

                    // Guardar en base de datos la url encriptada de archivo almacenado
                    $LecturaConsumoEvidenciaRepository = new LecturaConsumoEvidenciaRepository([
                        'opcion' => 'INSERTAR_EVIDENCIA_LECTURA',
                        'idLecturaConsumo' => $rsIngreso[0]->id,
                        'urlEvidencia' => $urlFileEvidencia
                    ]);
                    $LecturaConsumoEvidenciaRepository->guardar($idEmpresa, $idUsuario);
                }
            }

            // $et->header = $data['header'];
            $et->body['dataOperacion']['tipoTransaccion'] = 'INGRESO_LECTURA';
            $et->body['dataOperacion']['fechaTransaccion'] = date('Y-m-d H:i:s');
            $et->body['dataOperacion']['dataAdicional'] = [
                'idLecturaConsumo' => $data['body']['dataOperacion']['idLecturaConsumo']
            ];

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->ingresoLectura : ' . $th->getMessage());
        }

        return $et;
    }


    private function guardarFileEvidencia($urlFile, $nombreCarpetaBase, $codItem)
    {
        // $path_parts = pathinfo($urlFile);
        // $extensionFile = $path_parts['extension'];
        $extensionFile = "jpeg";

        $urlFileDestino = $nombreCarpetaBase."/".date('Y-m-d')."/".$codItem."_".date('YmdHis').".".$extensionFile;
        
        // Storage::put($urlFileDestino, file_get_contents($urlFile));
        Storage::put($urlFileDestino, $this->curl_get_contents($urlFile));
        $urlEncryptedFile = Crypt::encryptString($urlFileDestino); // URL encriptada de destino de File

        return $urlEncryptedFile;
    }

    private function curl_get_contents($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
}
