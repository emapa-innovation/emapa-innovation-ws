<?php

namespace App\Http\BusinessLayer\Comercial;

use Illuminate\Support\Facades\DB;
use App\Helpers\EstadoTransaccion;
use App\Http\Repositories\Comercial\ClienteRepository;
use App\Http\Repositories\Comercial\InstalacionRepository;
use App\Http\Repositories\Seguridad\Parametros\CalificacionClienteRepository;

class ClienteBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $CalificacionClienteRepository = new CalificacionClienteRepository([
                'opcion' => 'LISTAR_COMBO'
            ]);
            $rsListaCalificacionCliente = $CalificacionClienteRepository->listar($idEmpresa, null);
            
            $InstalacionRepository = new InstalacionRepository([
                'opcion' => 'LISTA_ESTADO_CUENTA_COMBO'
            ]);
            $rsListaEstadoCuenta = $InstalacionRepository->listar($idEmpresa, null);

            $et->data = [
                'listaCalificacionCliente' => $rsListaCalificacionCliente,
                'listaEstadoCuenta' => $rsListaEstadoCuenta
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $ClienteRepository = new ClienteRepository($data);

            $et->data = $ClienteRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }

    public function guardar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            DB::beginTransaction();

            $ClienteRepository = new ClienteRepository($data);
            $et->data = $ClienteRepository->guardar($idEmpresa, $idUsuario);

            if ( !empty($data['idEstadoCuenta']) ) {
                $data['opcion'] = 'CAMBIAR_ESTADO_CUENTA';
                $data['idEstadoLogicoCuenta'] = $data['idEstadoCuenta'];
                $data['idInstalacion'] = $data['idInstalacion'];

                $InstalacionRepository = new InstalacionRepository($data);
                $rsInstalacion = $InstalacionRepository->guardar($idEmpresa, $idUsuario);

                if( $rsInstalacion[0]->respuesta == 0 ) {
                    DB::rollback();
                    $et->existeError = true;
                    $et->mensaje = $rsInstalacion[0]->mensaje;
                    
                    return $et;
                }
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
        }

        return $et;
    }
}
