<?php

namespace App\Http\BusinessLayer\Comercial;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Helpers\EstadoTransaccion;
use App\Imports\ExcelImport;
use App\Exports\ReporteGenericoAutoSizeExport;
use App\Http\Repositories\Comercial\CorteReconexionRepository;
use App\Http\Repositories\Comercial\MicromedidorRepository;

class CorteReconexionBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $CorteReconexionRepository = new CorteReconexionRepository([
                'opcion' => 'LISTA_ESTADOS_CORTE_CMB'
            ]);
            $rsListaEstadoCorte = $CorteReconexionRepository->listar($idEmpresa, null);
            
            $CorteReconexionRepository = new CorteReconexionRepository([
                'opcion' => 'TIPOS_CONSULTA_CORTE_EXTERNO_CMB'
            ]);
            $rsListaTiposConsultaCorteExterno = $CorteReconexionRepository->listar($idEmpresa, null);

            $et->data = [
                'listaEstadoCorte' => $rsListaEstadoCorte,
                'listaTiposConsultaCorteExterno' => $rsListaTiposConsultaCorteExterno,
                'tipoConsultaCorteExternoDefault' => array_shift($rsListaTiposConsultaCorteExterno)->valor,
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $CorteReconexionRepository = new CorteReconexionRepository($data);

            $et->data = $CorteReconexionRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }

    public function guardar($idEmpresa, $idAgencia, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            DB::beginTransaction();

            $CorteReconexionRepository = new CorteReconexionRepository($data);
            $et->data = $CorteReconexionRepository->guardar($idEmpresa, $idAgencia, $idUsuario);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
        }

        return $et;
    }

    public function importar($idEmpresa, $idAgencia, $idUsuario, $request)
    {
        $et = new EstadoTransaccion();

        try {
            $data = json_decode(json_encode($request->all()), true);
            $dataItems = Excel::toArray(new ExcelImport, $request->file('archivoItems'));

            DB::beginTransaction();

            // SE ELIMINA PRIMER ROW QUE ES EL ROW DE CABECERA EN EL EXCEL, EN PRIMERA SHEET
            array_shift($dataItems[0]);
            if ( count($dataItems[0]) > 0 ) {
                if ($data['opcion'] == 'IMPORTAR_GENERACION_CORTES') {
                    // SE INGRESAN CORTES GENERADOS
                    foreach($dataItems[0] as $row)
                    {
                        // OBTENER INFORMACION MICROMEDIDOR
                        $MicromedidorRepository = new MicromedidorRepository([
                            'opcion' => 'LISTAR_MICROMEDIDOR_X_CODIGO',
                            'codigo' => trim($row[2])
                        ]);
                        $rsMicromedidor = $MicromedidorRepository->listar($idEmpresa, $idUsuario);

                        if (empty($rsMicromedidor)) {
                            DB::rollBack();
                            $et->existeError = true;
                            $et->mensaje = 'NUMERO DE MICROMEDIDOR NO ENCONTRADO [COLUMNA = C; VALOR = '.$row[2].']';
                            return $et;
                        }

                        // SI NO EXISTE CONEXION DE AGUA DE MICROMEDIDOR
                        if( !isset($rsMicromedidor[0]->idConexionAgua) ) {
                            continue;
                        }
                        // SI YA EXISTE UN CORTE PENDIENTE PARA LA CONEXION DE AGUA
                        if( isset($rsMicromedidor[0]->idCorte) ) {
                            continue;
                        }
                        
                        $CorteReconexionRepository = new CorteReconexionRepository([
                            'opcion'            => 'INSERTAR_CORTES_GENERADOS',
                            'idConexionAgua'    => $rsMicromedidor[0]->idConexionAgua,
                        ]);
                        $rsCorte = $CorteReconexionRepository->guardar($idEmpresa, $idAgencia, $idUsuario);
    
                        if ( $rsCorte[0]->respuesta == 0 ) {
                            DB::rollBack();
                            $et->existeError = true;
                            $et->mensaje = !empty($rsCorte[0]->mensaje) ? $rsCorte[0]->mensaje : 'ERROR DE INGRESO';
                            return $et;
                        }
                    }
                }
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->importar : ' . $th->getMessage());
        }

        return $et;
    }

    public function exportar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {            
            $CorteReconexionRepository = new CorteReconexionRepository($data);
            $rsData = $CorteReconexionRepository->listar($idEmpresa, $idUsuario);

            if (empty($rsData)) {
                $et->existeError = true;
                $et->mensaje = EstadoTransaccion::$noExistenDatos;
                return $et;
            }
            
            $export = new ReporteGenericoAutoSizeExport($rsData);

            $nameFile = 'cortes_'.date('Y-m-d His').'.xlsx';
            if (!Excel::store($export, $nameFile)) {
                $et->existeError = true;
                $et->mensaje = 'ERROR AL EXPORTAR ARCHIVO EXCEL';
                return $et;
            }
            $et->data = [
                'nameFile' => $nameFile
            ];
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->exportar : ' . $th->getMessage());
        }

        return $et;
    }
}
