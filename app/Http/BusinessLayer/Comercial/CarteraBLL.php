<?php

namespace App\Http\BusinessLayer\Comercial;

use Illuminate\Support\Facades\DB;
use App\Helpers\EstadoTransaccion;
use App\Http\Repositories\Comercial\CarteraRepository;
use App\Http\Repositories\Seguridad\Parametros\ItemsRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ReporteCarteraDetalladaExport;

class CarteraBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $CarteraRepository = new CarteraRepository([
                'opcion' => 'CMB_TIPO_REPORTE_CARTERA'
            ]);
            $rsTipoReporteCartera = $CarteraRepository->listar($idEmpresa, null);

            $et->data = [
                'listaTipoReporteCartera' => $rsTipoReporteCartera,
                'tipoReporteCarteraDefault' => 'RPT_CARTERA_DETALLE'
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $CarteraRepository = new CarteraRepository($data);

            $et->data = $CarteraRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }

    public function guardar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            DB::beginTransaction();

            $CarteraRepository = new CarteraRepository($data);
            $et->data = $CarteraRepository->guardar($idEmpresa, $idUsuario);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
        }

        return $et;
    }

    public function exportar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {            
            $CarteraRepository = new CarteraRepository($data);
            $rsCartera = $CarteraRepository->listar($idEmpresa, $idUsuario);
            
            $ItemsRepository = new ItemsRepository([
                'opcion' => 'LISTAR'
            ]);
            $rsItems = $ItemsRepository->listar($idEmpresa, $idUsuario);

            $export = new ReporteCarteraDetalladaExport($rsCartera, $rsItems);

            $nameFile = 'cartera_'.date('Y-m-d His').'.xlsx';
            if (!Excel::store($export, $nameFile)) {
                $et->existeError = true;
                return $et;
            }
            $et->data = [
                'nameFile' => $nameFile
            ];

        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->exportar : ' . $th->getMessage());
        }

        return $et;
    }
}
