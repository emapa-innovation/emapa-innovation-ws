<?php

namespace App\Http\BusinessLayer\Comercial;

use Illuminate\Support\Facades\DB;
use App\Helpers\EstadoTransaccion;
use App\Http\Repositories\Comercial\ConexionAlcantarilladoRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ReporteGenericoExport;

class ConexionAlcantarilladoBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $et->data = [];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $ConexionAlcantarilladoRepository = new ConexionAlcantarilladoRepository($data);

            $et->data = $ConexionAlcantarilladoRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }

    public function guardar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            DB::beginTransaction();

            $ConexionAlcantarilladoRepository = new ConexionAlcantarilladoRepository($data);
            $et->data = $ConexionAlcantarilladoRepository->guardar($idEmpresa, $idUsuario);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
        }

        return $et;
    }

    public function exportar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {            
            $ConexionAlcantarilladoRepository = new ConexionAlcantarilladoRepository($data);
            $rsData = $ConexionAlcantarilladoRepository->listar($idEmpresa, $idUsuario);

            if (empty($rsData)) {
                $et->existeError = true;
                $et->mensaje = EstadoTransaccion::$noExistenDatos;
                return $et;
            }

            $export = new ReporteGenericoExport($rsData);

            $nameFile = 'lecturas_'.date('Y-m-d His').'.xlsx';
            if (!Excel::store($export, $nameFile)) {
                $et->existeError = true;
                $et->mensaje = 'ERROR AL EXPORTAR ARCHIVO EXCEL';
                return $et;
            }
            $et->data = [
                'nameFile' => $nameFile
            ];

        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->exportar : ' . $th->getMessage());
        }

        return $et;
    }
}
