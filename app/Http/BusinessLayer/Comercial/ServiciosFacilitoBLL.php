<?php

namespace App\Http\BusinessLayer\Comercial;

use Illuminate\Support\Facades\DB;
use App\Helpers\EstadoTransaccionRecaudacion;
use App\Http\Repositories\Comercial\ServicioRecaudacionExternaRepository;
use App\Http\Repositories\Comercial\RecaudacionExternaRepository;

class ServiciosFacilitoBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccionRecaudacion();

        try {
            $et->body['dataOperacion'] = [];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function consulta($idEmpresa, $idAgencia, $idUsuario, $data)
    {
        $et = new EstadoTransaccionRecaudacion();

        try {
            if (!isset($data['body']['dataOperacion']['tipoConsulta']) || !isset($data['body']['dataOperacion']['criterioConsulta'])) {
                $et->body['dataResponse']['codigo'] = $et::$datosIncompletosCod;
                $et->body['dataResponse']['descripcion'] = $et::$datosIncompletosDesc;

                return $et;
            }

            $dataRequest = [
                'opcion' => 'VERIFICAR_EXISTENCIA_CUENTA',
                'tipoBusqueda' => $data['body']['dataOperacion']['tipoConsulta'],
                'criterioBusqueda' => $data['body']['dataOperacion']['criterioConsulta']
            ];
            $ServicioRecaudacionExternaRepository = new ServicioRecaudacionExternaRepository($dataRequest);
            $rsContrapartida = $ServicioRecaudacionExternaRepository->listar($idEmpresa, $idAgencia, $idUsuario);

            if (empty($rsContrapartida)) {
                $et->body['dataResponse']['codigo'] = $et::$contrapartidaIncorrectaCod;
                $et->body['dataResponse']['descripcion'] = $et::$contrapartidaIncorrectaDesc;
                
                return $et;
            }

            $dataRequest = [
                'opcion' => 'FACILITO_CONSULTA_CONTRAPARTIDA',
                'tipoBusqueda' => $data['body']['dataOperacion']['tipoConsulta'],
                'criterioBusqueda' => $data['body']['dataOperacion']['criterioConsulta']
            ];
            $ServicioRecaudacionExternaRepository = new ServicioRecaudacionExternaRepository($dataRequest);
            $rsConsulta = $ServicioRecaudacionExternaRepository->listar($idEmpresa, $idAgencia, $idUsuario);

            if (empty($rsConsulta)) {
                $et->body['dataResponse']['codigo'] = $et::$sinValoresPendientesCod;
                $et->body['dataResponse']['descripcion'] = $et::$sinValoresPendientesDesc;
                
                return $et;
            }

            $et->header = $data['header'];

            $et->body['dataOperacion']['tipoTransaccion'] = 'CONSULTA';
            $et->body['dataOperacion']['fechaTransaccion'] = date('Y-m-d H:i:s');
            $et->body['dataOperacion']['dataAdicional'] = $rsConsulta;
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->consulta : ' . $th->getMessage());
        }

        return $et;
    }
    
    public function pago($idEmpresa, $idAgencia, $idUsuario, $data)
    {
        $et = new EstadoTransaccionRecaudacion();

        try {
            DB::beginTransaction();

            if (!isset($data['body']['dataOperacion']['referencia']) || !isset($data['body']['dataOperacion']['monto']) ||
                !isset($data['body']['dataOperacion']['cuenta'])) {
                $et->body['dataResponse']['codigo'] = $et::$datosIncompletosCod;
                $et->body['dataResponse']['descripcion'] = $et::$datosIncompletosDesc;

                return $et;
            }

            $dataRequest = [
                'opcion' => 'VERIFICAR_CUENTA_FACTURA',
                'tipoBusqueda' => 'CUENTA',
                'criterioBusqueda' => $data['body']['dataOperacion']['cuenta'],
                'numeroFactura' => $data['body']['dataOperacion']['referencia']
            ];
            $ServicioRecaudacionExternaRepository = new ServicioRecaudacionExternaRepository($dataRequest);
            $rsTransaccion = $ServicioRecaudacionExternaRepository->listar($idEmpresa, $idAgencia, $idUsuario);

            if( !isset($rsTransaccion[0]->idFactura) || empty($rsTransaccion[0]->idFactura) ) {
                $et->body['dataResponse']['codigo'] = $et::$datosIncorrectosCod;
                $et->body['dataResponse']['descripcion'] = $et::$datosIncorrectosDesc.' - NRO. DE FACTURA Y NRO. DE CUENTA NO COINCIDEN';
                
                return $et;
            }

            $dataRequest = [
                'opcion' => 'EJECUTAR_PAGO_FACTURA',
                'numeroFactura' => $data['body']['dataOperacion']['referencia'],
                'valorRecaudacion' => $data['body']['dataOperacion']['monto']
            ];
            $RecaudacionExternaRepository = new RecaudacionExternaRepository($dataRequest);
            $rsPago = $RecaudacionExternaRepository->guardar($idEmpresa, $idAgencia, $idUsuario);
            
            if( empty($rsPago[0]->id) || $rsPago[0]->respuesta == 0 ) {
                DB::rollback();
                $et->body['dataResponse']['codigo'] = $et::$transaccionErroneaCod;
                $et->body['dataResponse']['descripcion'] = $rsPago[0]->mensaje ?? $et::$transaccionErroneaDesc;
                
                return $et;
            }

            $dataRequest = [
                'opcion' => 'FACILITO_GUARDAR_TRANSACCION_PAGO',
                'idRecaudacion' => $rsPago[0]->id,
                'idTransaccionEntidad' => $data['header']['dataEntidad']['idTransaccion'],
                'tipoOperacionEntidad' => $data['body']['dataOperacion']['tipoTransaccion'],
                'metodoPagoEntidad' => $data['body']['dataOperacion']['metodoPago'],
                'jsonHeaderEntidad' => json_encode($data['header']),
                'jsonOperationEntidad' => json_encode($data['body']['dataOperacion'])
            ];
            $ServicioRecaudacionExternaRepository = new ServicioRecaudacionExternaRepository($dataRequest);
            $rsTransaccion = $ServicioRecaudacionExternaRepository->guardar($idEmpresa, $idAgencia, $idUsuario);

            if( empty($rsTransaccion[0]->idRecaudacion) ) {
                DB::rollback();
                $et->body['dataResponse']['codigo'] = $et::$transaccionErroneaCod;
                $et->body['dataResponse']['descripcion'] = $rsTransaccion[0]->mensaje ?? $et::$transaccionErroneaDesc;
                
                return $et;
            }

            $et->header = $data['header'];
            $et->body['dataOperacion']['tipoTransaccion'] = 'PAGO';
            $et->body['dataOperacion']['fechaTransaccion'] = date('Y-m-d H:i:s');
            $et->body['dataOperacion']['dataAdicional'] = $rsTransaccion;

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->pago : ' . $th->getMessage());
        }

        return $et;
    }
    
    public function reverso($idEmpresa, $idAgencia, $idUsuario, $data)
    {
        $et = new EstadoTransaccionRecaudacion();

        try {
            DB::beginTransaction();

            if (!isset($data['body']['dataOperacion']['idTransaccionEntidad']) || 
                 empty($data['body']['dataOperacion']['idTransaccionEntidad'])) {
                $et->body['dataResponse']['codigo'] = $et::$datosIncompletosCod;
                $et->body['dataResponse']['descripcion'] = $et::$datosIncompletosDesc;

                return $et;
            }
            
            $dataRequest = [
                'opcion' => 'FACILITO_CONSULTA_TRANSACCION_ENTIDAD',
                'idTransaccionEntidad' => $data['body']['dataOperacion']['idTransaccionEntidad']
            ];
            $ServicioRecaudacionExternaRepository = new ServicioRecaudacionExternaRepository($dataRequest);
            $rsTransaccion = $ServicioRecaudacionExternaRepository->listar($idEmpresa, $idAgencia, $idUsuario);

            if( !isset($rsTransaccion[0]->idRecaudacion) || empty($rsTransaccion[0]->idRecaudacion) ) {
                $et->body['dataResponse']['codigo'] = $et::$datosIncorrectosCod;
                $et->body['dataResponse']['descripcion'] = $et::$datosIncorrectosDesc.' - NO SE ENCONTRO REGISTRO DE TRANSACCION';
                
                return $et;
            }

            $dataRequest = [
                'opcion' => 'EJECUTAR_REVERSO_PAGO',
                'idRecaudacion' => $rsTransaccion[0]->idRecaudacion
            ];
            $RecaudacionExternaRepository = new RecaudacionExternaRepository($dataRequest);
            $rsReverso = $RecaudacionExternaRepository->guardar($idEmpresa, $idAgencia, $idUsuario);

            if( empty($rsReverso[0]->id) || $rsReverso[0]->respuesta == 0 ) {
                DB::rollback();
                $et->body['dataResponse']['codigo'] = $et::$transaccionErroneaCod;
                $et->body['dataResponse']['descripcion'] = $rsReverso[0]->mensaje ?? $et::$transaccionErroneaDesc;
                
                return $et;
            }

            $dataRequest = [
                'opcion' => 'FACILITO_GUARDAR_TRANSACCION_REVERSO',
                'idRecaudacion' => $rsReverso[0]->id,
                'idTransaccionEntidad' => $data['header']['dataEntidad']['idTransaccion'],
                'tipoOperacionEntidad' => $data['body']['dataOperacion']['tipoTransaccion'],
                'metodoPagoEntidad' => $data['body']['dataOperacion']['metodoPago'],
                'jsonHeaderEntidad' => json_encode($data['header']),
                'jsonOperationEntidad' => json_encode($data['body']['dataOperacion'])
            ];
            $ServicioRecaudacionExternaRepository = new ServicioRecaudacionExternaRepository($dataRequest);
            $rsTransaccion = $ServicioRecaudacionExternaRepository->guardar($idEmpresa, $idAgencia, $idUsuario);

            if( empty($rsTransaccion[0]->idRecaudacion) ) {
                DB::rollback();
                $et->body['dataResponse']['codigo'] = $et::$transaccionErroneaCod;
                $et->body['dataResponse']['descripcion'] = $rsTransaccion[0]->mensaje ?? $et::$transaccionErroneaDesc;
                
                return $et;
            }

            $et->header = $data['header'];
            $et->body['dataOperacion']['tipoTransaccion'] = 'REVERSO';
            $et->body['dataOperacion']['fechaTransaccion'] = date('Y-m-d H:i:s');
            $et->body['dataOperacion']['dataAdicional'] = $rsTransaccion;

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->pago : ' . $th->getMessage());
        }

        return $et;
    }
}
