<?php

namespace App\Http\BusinessLayer\Comercial\Reportes;

use Illuminate\Support\Facades\DB;
use App\Helpers\EstadoTransaccion;
use App\Http\Repositories\Comercial\Reportes\RecaudacionCarteraRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ReporteGenericoAutoSizeExport;

class RecaudacionCarteraBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $RecaudacionCarteraRepository = new RecaudacionCarteraRepository([
                'opcion' => 'CMB_TIPO_REPORTES'
            ]);
            $rsTipoReporte = $RecaudacionCarteraRepository->listar($idEmpresa, null);
            
            $et->data = [
                'listaTipoReporte' => $rsTipoReporte,
                'tipoReporteDefault' => 'RECAUDACION_CARTERA_X_SECTORES'
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $RecaudacionCarteraRepository = new RecaudacionCarteraRepository($data);

            $et->data = $RecaudacionCarteraRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }

    public function exportar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {            
            $RecaudacionCarteraRepository = new RecaudacionCarteraRepository($data);
            $rsData = $RecaudacionCarteraRepository->listar($idEmpresa, $idUsuario);

            if (empty($rsData)) {
                $et->existeError = true;
                $et->mensaje = EstadoTransaccion::$noExistenDatos;
                return $et;
            }
            
            $export = new ReporteGenericoAutoSizeExport($rsData);

            $nameFile = 'Reporte_RecaudacionCartera_'.date('YmdHis').'.xlsx';
            if (!Excel::store($export, $nameFile)) {
                $et->existeError = true;
                $et->mensaje = 'ERROR AL EXPORTAR ARCHIVO EXCEL';
                return $et;
            }
            $et->data = [
                'nameFile' => $nameFile
            ];
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->exportar : ' . $th->getMessage());
        }

        return $et;
    }
}
