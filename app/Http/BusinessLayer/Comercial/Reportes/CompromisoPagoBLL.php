<?php

namespace App\Http\BusinessLayer\Comercial\Reportes;

use Illuminate\Support\Facades\DB;
use App\Helpers\EstadoTransaccion;
use App\Http\Repositories\Comercial\Reportes\CompromisoPagoRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ReporteGenericoExport;

class CompromisoPagoBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $CompromisoPagoRepository = new CompromisoPagoRepository([
                'opcion' => 'CMB_ESTADOS_COMPROMISO'
            ]);
            $rsEstadosCompromiso = $CompromisoPagoRepository->listar($idEmpresa, null);
            
            $et->data = [
                'listaEstadosCompromiso' => $rsEstadosCompromiso
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $CompromisoPagoRepository = new CompromisoPagoRepository($data);

            $et->data = $CompromisoPagoRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }

    public function exportar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {            
            $CompromisoPagoRepository = new CompromisoPagoRepository($data);
            $rsData = $CompromisoPagoRepository->listar($idEmpresa, $idUsuario);

            if (empty($rsData)) {
                $et->existeError = true;
                $et->mensaje = EstadoTransaccion::$noExistenDatos;
                return $et;
            }
            
            $export = new ReporteGenericoExport($rsData);

            $nameFile = 'reporte_compromisopago_'.date('Y-m-d His').'.xlsx';
            if (!Excel::store($export, $nameFile)) {
                $et->existeError = true;
                $et->mensaje = 'ERROR AL EXPORTAR ARCHIVO EXCEL';
                return $et;
            }
            $et->data = [
                'nameFile' => $nameFile
            ];

        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->exportar : ' . $th->getMessage());
        }

        return $et;
    }
}
