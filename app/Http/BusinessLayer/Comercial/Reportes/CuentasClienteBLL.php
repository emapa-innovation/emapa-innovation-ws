<?php

namespace App\Http\BusinessLayer\Comercial\Reportes;

use Illuminate\Support\Facades\DB;
use App\Helpers\EstadoTransaccion;
use App\Http\Repositories\Comercial\Reportes\CuentasClienteRepository;
use App\Http\Repositories\General\CiclosRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ReporteGenericoExport;

class CuentasClienteBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $CuentasClienteRepository = new CuentasClienteRepository([
                'opcion' => 'CMB_TIPO_REPORTES'
            ]);
            $rsTipoReporte = $CuentasClienteRepository->listar($idEmpresa, null);
            
            $CiclosRepository = new CiclosRepository([
                'opcion' => 'LISTAR'
            ]);
            $rsCiclos = $CiclosRepository->listar($idEmpresa, null);

            $et->data = [
                'listaCiclos' => $rsCiclos,
                'listaTipoReporte' => $rsTipoReporte,
                'tipoReporteDefault' => 'RPT_LISTA_CUENTAS_CLIENTE'
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $CuentasClienteRepository = new CuentasClienteRepository($data);

            $et->data = $CuentasClienteRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }

    public function exportar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {            
            $CuentasClienteRepository = new CuentasClienteRepository($data);
            $rsData = $CuentasClienteRepository->listar($idEmpresa, $idUsuario);

            if (empty($rsData)) {
                $et->existeError = true;
                $et->mensaje = EstadoTransaccion::$noExistenDatos;
                return $et;
            }
            
            $export = new ReporteGenericoExport($rsData);

            $nameFile = 'reporte_cuentas_'.date('Y-m-d His').'.xlsx';
            if (!Excel::store($export, $nameFile)) {
                $et->existeError = true;
                $et->mensaje = 'ERROR AL EXPORTAR ARCHIVO EXCEL';
                return $et;
            }
            $et->data = [
                'nameFile' => $nameFile
            ];

        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->exportar : ' . $th->getMessage());
        }

        return $et;
    }
}
