<?php

namespace App\Http\BusinessLayer\Comercial;

use App\Helpers\EstadoTransaccion;
use App\Helpers\ExtendedFPDF;
use App\Http\Repositories\Comercial\DocumentoElectronicoRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;

class DocumentoElectronicoBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $et->data = [];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $DocumentoElectronicoRepository = new DocumentoElectronicoRepository($data);

            $et->data = $DocumentoElectronicoRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }

    public function enviarEmailRideXLote($idEmpresa, $idUsuario, $dataLote)
    {
        $et = new EstadoTransaccion();

        try {
            $dataLote = json_decode(json_encode($dataLote), false);
            // -------------------------------------------------- //
            //                    CONEXION FTP
            // -------------------------------------------------- //
            // set up basic connection
            $ftp = ftp_connect(env('FTP_HOST'));

            // login with username and password
            $login_result = ftp_login($ftp, env('FTP_USERNAME'), env('FTP_PASSWORD'));
            if (!$login_result) {
                $et->existeError = true;
                $et->mensaje = 'ERROR AL CONECTAR CON SERVIDOR FTP DE XML DE DOCUMENTOS ELECTRONICOS';
                return $et;
            }

            // activar modo pasivo
            ftp_pasv($ftp, true);

            $totalRegistrosProcesados = 0; $totalRegistrosNoProcesados = 0; $detalleRegistrosNoProcesados = '';
            foreach ($dataLote as $documentoElectronico) {
                $result = $this->rutinaEnviarEmailRide($idEmpresa, $idUsuario, $documentoElectronico, $ftp);
                if ($result->existeError) {
                    $totalRegistrosNoProcesados++;
                    $detalleRegistrosNoProcesados .= ' | '.$result->mensaje;
                } else {
                    $totalRegistrosProcesados++;
                }
            }

            // close the connection ftp
            ftp_close($ftp);

            $et->data = [
                'totalRegistrosProcesados' => $totalRegistrosProcesados,
                'totalRegistrosNoProcesados' => $totalRegistrosNoProcesados,
                'detalleRegistrosNoProcesados' => $detalleRegistrosNoProcesados
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->enviarEmailRideXLote : ' . $th->getMessage());
        }

        return $et;
    }
    
    public function enviarEmailRide($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $DocumentoElectronicoRepository = new DocumentoElectronicoRepository([
                'opcion' => 'LISTAR_DOCUMENTOS_ENVIO_MAIL',
                'periodo' => $data['periodo'],
                'idCiclo' => $data['idCiclo']
            ]);
            $rsListaDocElectronicos = $DocumentoElectronicoRepository->listar($idEmpresa, $idUsuario);

            if (empty($rsListaDocElectronicos)) {
                $et->existeError = true;
                $et->mensaje = 'NO EXISTEN DOCUMENTOS ELECTRÓNICOS PENDIENTES DE ENVÍO PARA EL PERIODO Y CICLO SELECCIONADO';
                return $et;
            }

            // -------------------------------------------------- //
            //                    CONEXION FTP
            // -------------------------------------------------- //
            // set up basic connection
            $ftp = ftp_connect(env('FTP_HOST'));

            // login with username and password
            $login_result = ftp_login($ftp, env('FTP_USERNAME'), env('FTP_PASSWORD'));
            if (!$login_result) {
                $et->existeError = true;
                $et->mensaje = 'ERROR AL CONECTAR CON SERVIDOR FTP DE XML DE DOCUMENTOS ELECTRONICOS';
                return $et;
            }

            // activar modo pasivo
            ftp_pasv($ftp, true);

            $totalRegistrosProcesados = 0; $totalRegistrosNoProcesados = 0; $detalleRegistrosNoProcesados = '';
            foreach ($rsListaDocElectronicos as $documentoElectronico) {
                $result = $this->rutinaEnviarEmailRide($idEmpresa, $idUsuario, $documentoElectronico, $ftp);
                if ($result->existeError) {
                    $totalRegistrosNoProcesados++;
                    $detalleRegistrosNoProcesados .= ' | '.$result->mensaje;
                } else {
                    $totalRegistrosProcesados++;
                }
            }

            // close the connection ftp
            ftp_close($ftp);

            $et->data = [
                'totalRegistros' => count($rsListaDocElectronicos),
                'totalRegistrosProcesados' => $totalRegistrosProcesados,
                'totalRegistrosNoProcesados' => $totalRegistrosNoProcesados,
                'detalleRegistrosNoProcesados' => $detalleRegistrosNoProcesados
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->enviarEmailRide : ' . $th->getMessage());
        }

        return $et;
    }

    private function rutinaEnviarEmailRide($idEmpresa, $idUsuario, $documentoElectronico, $ftp)
    {
        $et = new EstadoTransaccion();

        DB::beginTransaction();
        try {
            $pdf = new ExtendedFPDF;

            $pdf->AddPage();

            $pdf->SetFont('Arial','B',10);

            $claveAcceso = $documentoElectronico->claveAcceso;
            $claveAccesoXml = $claveAcceso.'.xml';
            
            // try to download $server_file and save to $local_file
            if (!ftp_get($ftp, Storage::path('documento_electronico/xml').'/'.$claveAccesoXml, 'xml/Autorizados/'.$claveAccesoXml, FTP_BINARY)) {
                $et->existeError = true;
                $et->mensaje = 'CLIENTE:'.$documentoElectronico->cliente.',ERROR:XML NO ENCONTRADO';

                return $et;
            }

            $xmlstr = Storage::get('documento_electronico/xml/'.$claveAccesoXml);

            $autorizacion = new \SimpleXMLElement($xmlstr);
            //SACO LOS DATOS DEL XML...
            $dat_comprobante = new \SimpleXMLElement($autorizacion->comprobante);
            
            $pdf->Image(url('/image/emapa-logo.png'),25,10,65,30,'','');

            $pdf->SetLineWidth(0.35);

            $pdf->RoundedRect(116, 5, 84, 110, 4, '');
            
            $pdf->SetFont('Arial','', 12);
            $pdf->Text(119,13,"R.U.C.:  ");
            $pdf->SetFont('Arial','B', 13);
            $pdf->Text(140,13,$dat_comprobante->infoTributaria->ruc);
            $pdf->SetFont('Arial','', 13);
            if($dat_comprobante->infoTributaria->codDoc=="01")
                $pdf->Text(119,22,"F A C T U R A");
            elseif($dat_comprobante->infoTributaria->codDoc=="04")
                $pdf->Text(119,22,utf8_decode("N O T A   D E   C R É D I T O"));
            elseif($dat_comprobante->infoTributaria->codDoc=="05")
                $pdf->Text(119,22,utf8_decode("N O T A   D E   D É B I T O"));

            $pdf->Text(120,31,"No.");
            $pdf->SetTextColor(255, 0, 0);
            $pdf->SetFont('Arial','B', 14);
            $secuencialComprobante = $dat_comprobante->infoTributaria->estab."-".$dat_comprobante->infoTributaria->ptoEmi."-".$dat_comprobante->infoTributaria->secuencial;
            $pdf->Text(132,31,$secuencialComprobante);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont('Arial','', 8);
            $pdf->Text(119,40,utf8_decode("NÚMERO DE AUTORIZACIÓN"));
            $pdf->Text(120,47,$autorizacion->numeroAutorizacion);

            $pdf->SetLeftMargin(119);
            $pdf->Ln(45);
            $pdf->SetBorder(false);
            //190
            $pdf->SetWidths(array(35,40));
            $pdf->SetFont('Arial','', 8);
            //un arreglo con alineacion para la Cabecera
            $pdf->SetAligns(array('L','L'));
            $pdf->Row(array(utf8_decode('FECHA Y HORA DE AUTORIZACIÓN'),$autorizacion->fechaAutorizacion));

            $pdf->SetFont('Arial','', 10);
            $pdf->Text(119,72,"AMBIENTE:  ".utf8_decode($dat_comprobante->infoTributaria->ambiente == 2 ? 'PRODUCCIÓN' : 'PRUEBAS'));
            $pdf->Text(119,81,utf8_decode("EMISIÓN:  ").($dat_comprobante->infoTributaria->tipoEmision == 1 ? 'NORMAL' : ''));
            
            $pdf->SetFont('Arial','', 12);
            $pdf->Text(119,90,"CLAVE DE ACCESO");

            // -------------------------------------------------- //
            //                      BARCODE
            // -------------------------------------------------- //
            $pdf->SetFont('Arial','B',8);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->Code128(120,93,$claveAcceso,77,12);
            $pdf->Text(120,110,$claveAcceso);

            $pdf->SetLeftMargin(10);  
            $pdf->RoundedRect(10, 45, 103, 70, 4, '');
            
            $pdf->SetFont('Arial','', 8);
            $pdf->SetLeftMargin(13);
            $pdf->Ln(-17);
            $pdf->SetBorder(false);
            //190
            $pdf->SetWidths(array(100));
            //un arreglo con alineacion para la Cabecera
            $pdf->SetAligns(array('L'));
            $pdf->Row(array(utf8_decode($dat_comprobante->infoTributaria->razonSocial)));

            $pdf->Ln(5);
            $pdf->SetWidths(array(20,80));
            //un arreglo con alineacion para la Cabecera
            $pdf->SetAligns(array('L','L'));
            $pdf->Row(array(utf8_decode('Dirección Matriz:'), utf8_decode($dat_comprobante->infoTributaria->dirMatriz)));
            $direccionMatrizEmpresa = $dat_comprobante->infoTributaria->dirMatriz;

            $pdf->Ln(5);
            $pdf->SetWidths(array(20,80));
            //un arreglo con alineacion para la Cabecera
            $pdf->SetAligns(array('L','L'));
            if($dat_comprobante->infoTributaria->codDoc=="01")
                $pdf->Row(array(utf8_decode('Dirección Sucursal:'), utf8_decode($dat_comprobante->infoFactura->dirEstablecimiento)));
            elseif($dat_comprobante->infoTributaria->codDoc=="04")
                $pdf->Row(array(utf8_decode('Dirección Sucursal:'), utf8_decode($dat_comprobante->infoNotaCredito->dirEstablecimiento)));
            elseif($dat_comprobante->infoTributaria->codDoc=="05")
                $pdf->Row(array(utf8_decode('Dirección Sucursal:'), utf8_decode($dat_comprobante->infoNotaDebito->dirEstablecimiento)));
            
            $pdf->Ln(4);
            $pdf->SetWidths(array(55,40));
            //un arreglo con alineacion para la Cabecera
            $pdf->SetAligns(array('L','L'));
            if($dat_comprobante->infoTributaria->codDoc=="01" && isset($dat_comprobante->infoFactura->contribuyenteEspecial))
                $pdf->Row(array('Contribuyente Especial Nro.', $dat_comprobante->infoFactura->contribuyenteEspecial));
            elseif($dat_comprobante->infoTributaria->codDoc=="04" && isset($dat_comprobante->infoNotaCredito->contribuyenteEspecial))
                $pdf->Row(array('Contribuyente Especial Nro.', $dat_comprobante->infoNotaCredito->contribuyenteEspecial));
            elseif($dat_comprobante->infoTributaria->codDoc=="05" && isset($dat_comprobante->infoNotaDebito->contribuyenteEspecial))
                $pdf->Row(array('Contribuyente Especial Nro.', $dat_comprobante->infoNotaDebito->contribuyenteEspecial));

            $pdf->Ln(2);
            $pdf->SetWidths(array(65,35));
            //un arreglo con alineacion para la Cabecera
            $pdf->SetAligns(array('L','L'));
            if($dat_comprobante->infoTributaria->codDoc=="01")
                $pdf->Row(array('OBLIGADO A LLEVAR CONTABILIDAD',$dat_comprobante->infoFactura->obligadoContabilidad));
            elseif($dat_comprobante->infoTributaria->codDoc=="04")
                $pdf->Row(array('OBLIGADO A LLEVAR CONTABILIDAD',$dat_comprobante->infoNotaCredito->obligadoContabilidad));
            elseif($dat_comprobante->infoTributaria->codDoc=="05")
                $pdf->Row(array('OBLIGADO A LLEVAR CONTABILIDAD',$dat_comprobante->infoNotaDebito->obligadoContabilidad));
            
            // ------------------------------------------------------------------------------------------------------------
            // Agente de Retencion 24-Oct-2020
            // ------------------------------------------------------------------------------------------------------------
            $DocumentoElectronicoRepository = new DocumentoElectronicoRepository([
                'opcion' => 'LISTAR_PARAMETROS_COMERCIO'
            ]);
            $rsListaParametros = $DocumentoElectronicoRepository->listar($idEmpresa, null);
            if ($rsListaParametros[0]->agente_retencion == 'SI') {
                $pdf->Ln(2);
                $pdf->Text(14,110, utf8_decode('Agente de Retención Resolución No. NAC-DNCRASC20-0000000'.$rsListaParametros[0]->num_resolucion_age_rete));
            }
            // ------------------------------------------------------------------------------------------------------------

            $pdf->SetLeftMargin(10);
            $pdf->SetLineWidth(0.4);
            $pdf->Ln(16);
            $pdf->SetFont('Arial','', 8);
            $x_pos=$pdf->GetX();
            $y_pos=$pdf->GetY();
            $pdf->SetBorder(false);
            //190
            $pdf->SetWidths(array(50,88,26,24));
            //un arreglo con alineacion para la Cabecera
            $pdf->SetAligns(array('L','L','L','L'));
            $razonSocialCliente = ''; $tipoComprobante = '';
            if($dat_comprobante->infoTributaria->codDoc=="01") {
                $razonSocialCliente = $dat_comprobante->infoFactura->razonSocialComprador;
                $tipoComprobante = "FACTURA";
                $pdf->Row(array(utf8_decode('Razón Social / Nombres y Apellidos:'), utf8_decode($razonSocialCliente),
                                utf8_decode('Identificación:'), $dat_comprobante->infoFactura->identificacionComprador));
            } elseif($dat_comprobante->infoTributaria->codDoc=="04") {
                $razonSocialCliente = $dat_comprobante->infoNotaCredito->razonSocialComprador;
                $tipoComprobante = "NOTA DE CRÉDITO";
                $pdf->Row(array(utf8_decode('Razón Social / Nombres y Apellidos:'), utf8_decode($razonSocialCliente),
                                utf8_decode('Identificación:'), $dat_comprobante->infoNotaCredito->identificacionComprador));
            } elseif($dat_comprobante->infoTributaria->codDoc=="05") {
                $razonSocialCliente = $dat_comprobante->infoNotaDebito->razonSocialComprador;
                $tipoComprobante = "NOTA DE DÉBITO";
                $pdf->Row(array(utf8_decode('Razón Social / Nombres y Apellidos:'), utf8_decode($razonSocialCliente),
                                utf8_decode('Identificación:'), $dat_comprobante->infoNotaDebito->identificacionComprador));
            }

            $pdf->SetAligns(array('L','L','L','L'));
            $fechaEmisionComprobante = ''; $valorComprobante = 0;
            if($dat_comprobante->infoTributaria->codDoc=="01"){
                $fechaEmisionComprobante = $dat_comprobante->infoFactura->fechaEmision;
                $pdf->Row(array(utf8_decode('Fecha Emisión:    '.$fechaEmisionComprobante), '', utf8_decode('Guía Remisión:'), ''));
                $pdf->SetWidths(array(16,100));
                $pdf->SetAligns(array('L','L'));
                $pdf->Row(array(utf8_decode('Dirección:'), utf8_decode($dat_comprobante->infoFactura->direccionComprador)));
            } elseif($dat_comprobante->infoTributaria->codDoc=="04"){
                $fechaEmisionComprobante = $dat_comprobante->infoNotaCredito->fechaEmision;
                $pdf->Row(array(utf8_decode('Fecha Emisión:   '.$fechaEmisionComprobante), '', '', ''));
                $yi=$pdf->GetY();
                $pdf->Line($x_pos+10,$yi,190,$yi);
                $pdf->SetWidths(array(95,95));
                switch($dat_comprobante->infoNotaCredito->codDocModificado){
                    case '01': $desc_doc="FACTURA"; break;
                    case '04': $desc_doc="NOTA DE CRÉDITO"; break;
                    case '05': $desc_doc="NOTA DE DÉBITO"; break;
                }
                $pdf->Row(array('Comprobante que se modifica:', utf8_decode($desc_doc.'      '.$dat_comprobante->infoNotaCredito->numDocModificado)));
                $pdf->Row(array(utf8_decode('Fecha emisión (Comprobante a modificar):'), $dat_comprobante->infoNotaCredito->fechaEmisionDocSustento));
                $pdf->Row(array(utf8_decode('Razón de Modificación:'), utf8_decode($dat_comprobante->infoNotaCredito->motivo)));
            } elseif($dat_comprobante->infoTributaria->codDoc=="05"){
                $fechaEmisionComprobante = $dat_comprobante->infoNotaDebito->fechaEmision;
                $pdf->Row(array(utf8_decode('Fecha Emisión:   '.$fechaEmisionComprobante), '', '', ''));
                $yi=$pdf->GetY();
                $pdf->Line($x_pos+10,$yi,190,$yi);
                $pdf->SetWidths(array(95,95));
                switch($dat_comprobante->infoNotaDebito->codDocModificado){
                    case '01': $desc_doc="FACTURA"; break;
                    case '04': $desc_doc="NOTA DE CRÉDITO"; break;
                    case '05': $desc_doc="NOTA DE DÉBITO"; break;
                }
                $pdf->Row(array('Comprobante que se modifica:', utf8_decode($desc_doc.'      '.$dat_comprobante->infoNotaDebito->numDocModificado)));
                $pdf->Row(array(utf8_decode('Fecha emisión (Comprobante a modificar):'), $dat_comprobante->infoNotaDebito->fechaEmisionDocSustento));
            }

            $y_pos1=$pdf->GetY();
            $pdf->Rect($x_pos,$y_pos,190,$y_pos1-$y_pos,'');
            $pdf->Ln(5);
            $pdf->SetBorder(true);
            if($dat_comprobante->infoTributaria->codDoc=="01" || $dat_comprobante->infoTributaria->codDoc=="04"){
                //190
                $pdf->SetWidths(array(20,15,64,15,19,19,19,19));
                //un arreglo con alineacion para la Cabecera
                $pdf->SetAligns(array('C','C','C','C','C','C','C','C'));
                $pdf->Row(array('Cod. Principal','Cantidad',utf8_decode('Descripción'),'Precio Unitario','Subsidio','Precio Sin Subsidio','Descuento','Precio Total'));
                
                $pdf->SetFont('Arial','', 8);
                //un arreglo con alineacion de cada celda
                $pdf->SetAligns(array('C','C','L','R','R','R','R','R'));
                
                foreach($dat_comprobante->detalles->detalle as $itemdetalle){ 
                    if($dat_comprobante->infoTributaria->codDoc=="01") {
                        if ((float)$itemdetalle->precioSinSubsidio > 0.00) $subsidio = (float)$itemdetalle->precioSinSubsidio - (float)$itemdetalle->precioTotalSinImpuesto;
                        else $subsidio = 0;
                        $pdf->Row(array($itemdetalle->codigoPrincipal,$itemdetalle->cantidad,utf8_decode($itemdetalle->descripcion),number_format((float) $itemdetalle->precioUnitario,2,'.',','),number_format((float) $subsidio,2,'.',','),number_format((float) $itemdetalle->precioSinSubsidio,2,'.',','),number_format((float) $itemdetalle->descuento,2,'.',','),number_format((float) $itemdetalle->precioTotalSinImpuesto,2,'.',',')));
                    }
                    elseif($dat_comprobante->infoTributaria->codDoc=="04")
                        $pdf->Row(array($itemdetalle->codigoInterno,$itemdetalle->cantidad,utf8_decode($itemdetalle->descripcion),number_format((float) $itemdetalle->precioUnitario,2,'.',','), ' ', ' ', number_format((float) $itemdetalle->descuento,2,'.',','),number_format((float) $itemdetalle->precioTotalSinImpuesto,2,'.',',')));
                }
            } elseif($dat_comprobante->infoTributaria->codDoc=="05"){
                //190
                $pdf->SetWidths(array(95,95));
                //un arreglo con alineacion para la Cabecera
                $pdf->SetAligns(array('C','C'));
                $pdf->Row(array(utf8_decode('RAZÓN DE LA MODIFICACIÓN'), utf8_decode('VALOR DE LA MODIFICACIÓN')));
                $pdf->SetFont('Arial','', 8);
                //un arreglo con alineacion de cada celda
                $pdf->SetAligns(array('L','R'));
                $total=0;
                foreach($dat_comprobante->motivos->motivo as $itemdetalle){
                    $pdf->Row(array($itemdetalle->razon,number_format((float) $itemdetalle->valor,2,'.',',')));
                    $total+=floatval($itemdetalle->valor);
                }
                $valorComprobante = $total;
            }

            $pdf->SetLeftMargin(136);
            //un arreglo con alineacion para los valores
            $subt_12=0;
            $subt_14=0;
            $subt_0=0;
            $subt_no_ob=0;
            $ice=0;
            $iva12=0; 
            $iva14=0;
            if($dat_comprobante->infoTributaria->codDoc=="01"){
                foreach($dat_comprobante->infoFactura->totalConImpuestos->totalImpuesto as $totalConImpuestos){
                    $codigo=$totalConImpuestos->codigo;
                    $prctje=$totalConImpuestos->codigoPorcentaje;
                    if($codigo==2){
                        if($prctje==0)
                            $subt_0=$totalConImpuestos->baseImponible;
                        else
                            if($prctje==2){
                                $subt_12=$totalConImpuestos->baseImponible;
                                $iva12=$totalConImpuestos->valor;
                            }
                        else
                            if($prctje==3){
                                $subt_14=$totalConImpuestos->baseImponible;
                                $iva14=$totalConImpuestos->valor;
                            }
                        else
                            $subt_no_ob=$totalConImpuestos->baseImponible;
                    } else {
                        if($codigo==3)
                            $ice=$totalConImpuestos->baseImponible;
                    }
                }
            } else {
                if($dat_comprobante->infoTributaria->codDoc=="04") {
                    foreach($dat_comprobante->infoNotaCredito->totalConImpuestos->totalImpuesto as $totalConImpuestos){
                        $codigo=$totalConImpuestos->codigo;
                        $prctje=$totalConImpuestos->codigoPorcentaje;
                        if($codigo==2){
                            if($prctje==0)
                                $subt_0=$totalConImpuestos->baseImponible;
                            else
                                if($prctje==2){
                                    $subt_12=$totalConImpuestos->baseImponible;
                                    $iva12=$totalConImpuestos->valor;
                                }
                            else
                                if($prctje==3){
                                    $subt_14=$totalConImpuestos->baseImponible;
                                    $iva14=$totalConImpuestos->valor;
                                }
                                else
                                    $subt_no_ob=$totalConImpuestos->baseImponible;
                        } else {
                            if($codigo==3)
                                $ice=$totalConImpuestos->baseImponible;
                        }
                    }
                }
            }

            $pdf->Ln(3);
            $pdf->SetWidths(array(45,19));
            $pdf->SetAligns(array('L','R'));
            if($dat_comprobante->infoTributaria->codDoc=="01"||$dat_comprobante->infoTributaria->codDoc=="04"){
                // $pdf->Row(array('SUBTOTAL 14%',number_format((float) $subt_14,2,'.',',')));
                $pdf->Row(array('SUBTOTAL 12%',number_format((float) $subt_12,2,'.',',')));
                
                $pdf->Row(array('SUBTOTAL 0%',number_format((float) $subt_0,2,'.',',')));
                $pdf->Row(array('SUBTOTAL NO OBJETO IVA',number_format((float) $subt_no_ob,2,'.',',')));
            
                if($dat_comprobante->infoTributaria->codDoc=="01"){
                    $pdf->Row(array('SUBTOTAL EXENTO IVA',"0.00"));
                    $pdf->Row(array('SUBTOTAL SIN IMPUESTOS',number_format((float) $dat_comprobante->infoFactura->totalSinImpuestos,2,'.',',')));
                    $pdf->Row(array('DESCUENTO',number_format((float) $dat_comprobante->infoFactura->totalDescuento,2,'.',',')));
                } else if($dat_comprobante->infoTributaria->codDoc=="04"){
                    $pdf->Row(array('SUBTOTAL',number_format((float) $dat_comprobante->infoNotaCredito->totalSinImpuestos,2,'.',',')));
                    $pdf->Row(array('TOTAL DESCUENTO',number_format((float) $dat_comprobante->infoNotaCredito->totalDescuento,2,'.',',')));
                    $pdf->Row(array('TOTAL EXENTO IVA',"0.00"));
                }
                $pdf->Row(array('ICE',number_format((float) $ice,2,'.',',')));
                // $pdf->Row(array('IVA 14%',number_format((float) $iva14,2,'.',',')));
                $pdf->Row(array('IVA 12%',number_format((float) $iva12,2,'.',',')));
                $pdf->Row(array('IRBPNR',"0.00"));
                if($dat_comprobante->infoTributaria->codDoc=="01"){
                    $valorComprobante = $dat_comprobante->infoFactura->importeTotal;
                    $pdf->Row(array('PROPINA',number_format((float) floatval($dat_comprobante->infoFactura->propina),2,'.',',')));
                    $pdf->Row(array('VALOR TOTAL',number_format((float) floatval($dat_comprobante->infoFactura->importeTotal),2,'.',',')));
                } elseif($dat_comprobante->infoTributaria->codDoc=="04"){
                    $valorComprobante = $dat_comprobante->infoNotaCredito->valorModificacion;
                    $pdf->Row(array('VALOR TOTAL',number_format((float) floatval($dat_comprobante->infoNotaCredito->valorModificacion),2,'.',',')));
                }
            } elseif($dat_comprobante->infoTributaria->codDoc=="05"){
                // $pdf->Row(array('SUBTOTAL 14%',"0,00"));
                $pdf->Row(array('SUBTOTAL 12%',"0,00"));
                $pdf->Row(array('SUBTOTAL 0%',"0.00"));
                $pdf->Row(array('SUBTOTAL NO OBJETO IVA',"0.00"));
                $pdf->Row(array('SUBTOTAL EXENTO IVA',number_format((float) $total,2,'.',',')));
                $pdf->Row(array('SUBTOTAL SIN IMPUESTOS',number_format((float) $total,2,'.',',')));
                $pdf->Row(array('ICE',"0.00"));
                // $pdf->Row(array('IVA 14%',"0.00"));
                $pdf->Row(array('IVA 12%',"0.00"));
                $pdf->Row(array('VALOR TOTAL',number_format((float) $total,2,'.',',')));
            }

            // **********************************************************************************************************************
            // ***   SUBSIDIOS   ***
            // **********************************************************************************************************************
            if($dat_comprobante->infoTributaria->codDoc=="01") {
                $pdf->Ln(3);
                
                $pdf->SetLeftMargin(136);
                
                $xi=$pdf->GetX(); 
                $yi=$pdf->GetY(); 
                
                $pdf->Line($xi,$yi,$xi+64,$yi); 
                $pdf->SetBorder(true);
                //190
                $pdf->SetWidths(array(49,15));
                $pdf->SetTpBorder(array('L','R'));
                
                $pdf->SetAligns(array('L','R'));
                
                $sinSubsidio = (float)$dat_comprobante->infoFactura->importeTotal + (float)$dat_comprobante->infoFactura->totalSubsidio;
                $pdf->Row(array('VALOR TOTAL SIN SUBSIDIO',number_format((float) $sinSubsidio,2,'.',',')));
                $pdf->Row(array('AHORRO POR SUBSIDIO',number_format((float) $dat_comprobante->infoFactura->totalSubsidio,2,'.',',')));
                
                $pdf->Row(array('(Incluye IVA cuando corresponda)',''));
                $yf=$pdf->GetY(); 
                $pdf->Line($xi,$yf,$xi+64,$yf);
                
                // prepara para informacion adicional
                $pdf->SetLeftMargin(10);
                
                $pdf->Ln(-73);
            } else {
                $pdf->SetLeftMargin(10);
                
                if($dat_comprobante->infoTributaria->codDoc=="04")
                    $pdf->Ln(-55);
                elseif($dat_comprobante->infoTributaria->codDoc=="05")
                    $pdf->Ln(-45);
                else
                    $pdf->Ln(-50);
            }
            
            // **********************************************************************************************************************
            $xi=$pdf->GetX();
            $yi=$pdf->GetY();
            $pdf->Line($xi,$yi,$xi+122,$yi);
            $pdf->SetBorder(true);
            //190
            $pdf->SetWidths(array(40,82));
            $pdf->SetTpBorder(array('L','R'));
            //un arreglo con alineacion para la Cabecera
            $pdf->SetAligns(array('R','L'));
            $pdf->Row(array('',utf8_decode('      Información Adicional')));

            $pdf->SetAligns(array('L','L'));
            foreach($dat_comprobante->infoAdicional->campoAdicional as $info_adic){
                $pdf->Row(array(utf8_decode($info_adic->attributes().':'), utf8_decode($info_adic[0])));
            }
            
            $pdf->Row(array('',''));
            $yf=$pdf->GetY(); 
            $pdf->Line($xi,$yf,$xi+122,$yf);

            // **********************************************************************************************************************
            // ***   PAGOS   ***
            // **********************************************************************************************************************
            if($dat_comprobante->infoTributaria->codDoc=="01") {
                $pdf->Ln(4);
            
                $xi=$pdf->GetX(); 
                $yi=$pdf->GetY(); 
                $pdf->Line($xi,$yi,$xi+115,$yi); //Line($x1, $y1, $x2, $y2)
                $pdf->SetBorder(true);
                
                //190
                $pdf->SetWidths(array(85,30));
                $pdf->SetTpBorder(array('L','R'));
                //un arreglo con alineacion para la Cabecera
                $pdf->SetAligns(array('C','C'));
                $pdf->Row(array('FORMA DE PAGO','VALOR'));
            
                $TOTAL = (float)$dat_comprobante->infoFactura->importeTotal;
                $pdf->Row(array('20 - OTROS CON UTILIZACION DEL SISTEMA FINANCIERO',number_format($TOTAL,2,'.',',')));
                //190
                $yf=$pdf->GetY(); 
                $pdf->Line($xi,$yf,$xi+115,$yf);
            }

            $pdf->Output('F', Storage::path('documento_electronico/ride').'/'.$claveAcceso.'.pdf', true);

            // -------------------------------------------------- //
            //                      ENVIO CORREO
            // -------------------------------------------------- //
            $DocumentoElectronicoRepository = new DocumentoElectronicoRepository([
                'opcion' => 'LISTAR_INFORMACION_EMPRESA'
            ]);
            $rsInformacionEmpresa = $DocumentoElectronicoRepository->listar($idEmpresa, null);

            $dataMail = array(
                'nombreEmpresa' => $rsInformacionEmpresa[0]->nombre,
                'razonSocialCliente' => $razonSocialCliente,
                'tipoComprobante' => $tipoComprobante,
                'secuencialComprobante' => $secuencialComprobante,
                'fechaEmisionComprobante' => $fechaEmisionComprobante,
                'valorComprobante' => $valorComprobante,
                'direccionMatrizEmpresa' => $direccionMatrizEmpresa,
                'ciudadEmpresa' => $rsInformacionEmpresa[0]->municipio
            );

            $validator = Validator::make(['email' => $documentoElectronico->emailCliente], ['email' => 'email:rfc,dns']);
            if ($validator->fails()){
                $et->existeError = true;
                $et->mensaje = 'CLIENTE:'.$documentoElectronico->cliente.',ERROR:EMAIL NO VALIDO';

                return $et;
            }

            Mail::send('Mail.Comercial.documentoElectronico', $dataMail, function($message) use ($documentoElectronico) {
                $message->to($documentoElectronico->emailCliente, $documentoElectronico->nombreCliente)->subject('Comprobante  Electrónico');
                $message->attach(Storage::path('documento_electronico/ride').'/'.$documentoElectronico->claveAcceso.'.pdf');
                $message->attach(Storage::path('documento_electronico/xml').'/'.$documentoElectronico->claveAcceso.'.xml');
                $message->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
            });
            // -------------------------------------------------- //

            Storage::delete(['documento_electronico/ride/'.$claveAcceso.'.pdf', 'documento_electronico/xml/'.$claveAcceso.'.xml']);

            // Guardar estado de correo enviado en comprobante electronico
            $DocumentoElectronicoRepository = new DocumentoElectronicoRepository([
                'opcion' => 'ACTUALIZAR_ESTADO_ENVIO_MAIL',
                'claveAcceso' => $claveAcceso,
                'envioMail' => 'S'
            ]);
            $DocumentoElectronicoRepository->guardar($idEmpresa, $idUsuario);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();

            $et->existeError = true;
            $et->mensaje = 'CLIENTE:'.$documentoElectronico->cliente.',ERROR:'.$th->getMessage();

            return $et;
        }

        return $et;
    }
}
