<?php

namespace App\Http\BusinessLayer\Comercial;

use Illuminate\Support\Facades\DB;
use App\Helpers\EstadoTransaccionBanred;
use App\Http\Repositories\Comercial\ServicioRecaudacionExternaRepository;
use App\Http\Repositories\Comercial\RecaudacionExternaRepository;

class ServiciosBanredBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccionBanred();

        try {
            $et->infoOperationResponse['infoAditional'] = [];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function consulta($idEmpresa, $idAgencia, $idUsuario, $data)
    {
        $et = new EstadoTransaccionBanred();

        try {
            if (!isset($data['header']['infoPerson']['documentType']) || !isset($data['header']['infoPerson']['counterpart'])) {
                $et->infoResponse['responseCode'] = $et::$datosIncompletosCod;
                $et->infoResponse['descResponse'] = $et::$datosIncompletosDesc;

                return $et;
            }

            $dataRequest = [
                'opcion' => 'VERIFICAR_EXISTENCIA_CUENTA',
                'tipoBusqueda' => $data['header']['infoPerson']['documentType'],
                'criterioBusqueda' => $data['header']['infoPerson']['counterpart']
            ];
            $ServicioRecaudacionExternaRepository = new ServicioRecaudacionExternaRepository($dataRequest);
            $rsContrapartida = $ServicioRecaudacionExternaRepository->listar($idEmpresa, $idAgencia, $idUsuario);

            if (empty($rsContrapartida)) {
                $et->infoResponse['responseCode'] = $et::$contrapartidaIncorrectaCod;
                $et->infoResponse['descResponse'] = $et::$contrapartidaIncorrectaDesc;
                
                return $et;
            }

            $dataRequest = [
                'opcion' => 'BANRED_CONSULTA_CONTRAPARTIDA',
                'tipoBusqueda' => $data['header']['infoPerson']['documentType'],
                'criterioBusqueda' => $data['header']['infoPerson']['counterpart']
            ];
            $ServicioRecaudacionExternaRepository = new ServicioRecaudacionExternaRepository($dataRequest);
            $rsConsulta = $ServicioRecaudacionExternaRepository->listar($idEmpresa, $idAgencia, $idUsuario);

            if (empty($rsConsulta)) {
                $et->infoResponse['responseCode'] = $et::$sinValoresPendientesCod;
                $et->infoResponse['descResponse'] = $et::$sinValoresPendientesDesc;
                
                return $et;
            }

            $et->headerResponse = $data['header'];

            $et->infoResponse['descCounterpart'] = $idUsuario;

            $et->infoOperationResponse['type'] = 'CONSULTA';
            $et->infoOperationResponse['transactionDate'] = date('Y-m-d H:i:s');
            $et->infoOperationResponse['paymentDueDate'] = date('Y-m-d');
            $et->infoOperationResponse['infoAditional'] = $rsConsulta;
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->consulta : ' . $th->getMessage());
        }

        return $et;
    }
    
    public function pago($idEmpresa, $idAgencia, $idUsuario, $data)
    {
        $et = new EstadoTransaccionBanred();

        try {
            DB::beginTransaction();

            if (!isset($data['infoOperation']['infoAmountDetail']) || empty($data['infoOperation']['infoAmountDetail'])) {
                $et->infoResponse['responseCode'] = $et::$datosIncompletosCod;
                $et->infoResponse['descResponse'] = $et::$datosIncompletosDesc;

                return $et;
            }

            $dataRequest = [
                'opcion' => 'VERIFICAR_CUENTA_FACTURA',
                'tipoBusqueda' => $data['header']['infoPerson']['documentType'],
                'criterioBusqueda' => $data['header']['infoPerson']['counterpart'],
                'numeroFactura' => $data['infoOperation']['infoAmountDetail']['reference']
            ];
            $ServicioRecaudacionExternaRepository = new ServicioRecaudacionExternaRepository($dataRequest);
            $rsTransaccion = $ServicioRecaudacionExternaRepository->listar($idEmpresa, $idAgencia, $idUsuario);

            if( !isset($rsTransaccion[0]->idFactura) || empty($rsTransaccion[0]->idFactura) ) {
                $et->infoResponse['responseCode'] = $et::$datosIncorrectosCod;
                $et->infoResponse['descResponse'] = $et::$datosIncorrectosDesc.' - NRO. DE FACTURA Y NRO. DE CUENTA NO COINCIDEN';
                
                return $et;
            }

            $dataRequest = [
                'opcion' => 'EJECUTAR_PAGO_FACTURA',
                'numeroFactura' => $data['infoOperation']['infoAmountDetail']['reference'],
                'valorRecaudacion' => $data['infoOperation']['infoAmountDetail']['value']
            ];
            $RecaudacionExternaRepository = new RecaudacionExternaRepository($dataRequest);
            $rsPago = $RecaudacionExternaRepository->guardar($idEmpresa, $idAgencia, $idUsuario);
            
            if( empty($rsPago[0]->id) || $rsPago[0]->respuesta == 0 ) {
                DB::rollback();
                $et->infoResponse['responseCode'] = $et::$transaccionErroneaCod;
                $et->infoResponse['descResponse'] = $rsPago[0]->mensaje ?? $et::$transaccionErroneaDesc;
                
                return $et;
            }

            $dataRequest = [
                'opcion' => 'BANRED_GUARDAR_TRANSACCION_PAGO',
                'idRecaudacion' => $rsPago[0]->id,
                'idTransaccionEntidad' => $data['header']['infoTouchPoint']['transactionID'],
                'tipoOperacionEntidad' => $data['infoOperation']['type'],
                'metodoPagoEntidad' => $data['infoPaymentMethod'],
                'jsonHeaderEntidad' => json_encode($data['header']),
                'jsonOperationEntidad' => json_encode($data['infoOperation'])
            ];
            $ServicioRecaudacionExternaRepository = new ServicioRecaudacionExternaRepository($dataRequest);
            $rsTransaccion = $ServicioRecaudacionExternaRepository->guardar($idEmpresa, $idAgencia, $idUsuario);

            if( empty($rsTransaccion[0]->transactionAuthorizerID) ) {
                DB::rollback();
                $et->infoResponse['responseCode'] = $et::$transaccionErroneaCod;
                $et->infoResponse['descResponse'] = $rsTransaccion[0]->mensaje ?? $et::$transaccionErroneaDesc;
                
                return $et;
            }

            $et->headerResponse = $data['header'];
            $et->infoResponse['descCounterpart'] = $idUsuario;
            $et->infoOperationResponse['type'] = $data['infoOperation']['type'];
            $et->infoOperationResponse['transactionDate'] = $data['infoOperation']['transactionDate'];
            $et->infoOperationResponse['businessDate'] = $data['infoOperation']['businessDate'];
            $et->infoOperationResponse['infoAditional'] = $rsTransaccion;

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->pago : ' . $th->getMessage());
        }

        return $et;
    }
    
    public function reverso($idEmpresa, $idAgencia, $idUsuario, $data)
    {
        $et = new EstadoTransaccionBanred();

        try {
            DB::beginTransaction();

            if (!isset($data['infoOperation']['originalTransactionID']) || empty($data['infoOperation']['originalTransactionID'])) {
                $et->infoResponse['responseCode'] = $et::$datosIncompletosCod;
                $et->infoResponse['descResponse'] = $et::$datosIncompletosDesc;

                return $et;
            }
            
            $dataRequest = [
                'opcion' => 'BANRED_CONSULTA_TRANSACCION_ENTIDAD',
                'idTransaccionEntidad' => $data['infoOperation']['originalTransactionID']
            ];
            $ServicioRecaudacionExternaRepository = new ServicioRecaudacionExternaRepository($dataRequest);
            $rsTransaccion = $ServicioRecaudacionExternaRepository->listar($idEmpresa, $idAgencia, $idUsuario);

            if( !isset($rsTransaccion[0]->idRecaudacion) || empty($rsTransaccion[0]->idRecaudacion) ) {
                $et->infoResponse['responseCode'] = $et::$datosIncorrectosCod;
                $et->infoResponse['descResponse'] = $et::$datosIncorrectosDesc.' - NO SE ENCONTRO REGISTRO DE TRANSACCION';
                
                return $et;
            }

            $dataRequest = [
                'opcion' => 'EJECUTAR_REVERSO_PAGO',
                'idRecaudacion' => $rsTransaccion[0]->idRecaudacion
            ];
            $RecaudacionExternaRepository = new RecaudacionExternaRepository($dataRequest);
            $rsReverso = $RecaudacionExternaRepository->guardar($idEmpresa, $idAgencia, $idUsuario);

            if( empty($rsReverso[0]->id) || $rsReverso[0]->respuesta == 0 ) {
                DB::rollback();
                $et->infoResponse['responseCode'] = $et::$transaccionErroneaCod;
                $et->infoResponse['descResponse'] = $rsReverso[0]->mensaje ?? $et::$transaccionErroneaDesc;
                
                return $et;
            }

            $dataRequest = [
                'opcion' => 'BANRED_GUARDAR_TRANSACCION_REVERSO',
                'idRecaudacion' => $rsReverso[0]->id,
                'idTransaccionEntidad' => $data['header']['infoTouchPoint']['transactionID'],
                'tipoOperacionEntidad' => $data['infoOperation']['type'],
                'metodoPagoEntidad' => $data['infoPaymentMethod'],
                'jsonHeaderEntidad' => json_encode($data['header']),
                'jsonOperationEntidad' => json_encode($data['infoOperation'])
            ];
            $ServicioRecaudacionExternaRepository = new ServicioRecaudacionExternaRepository($dataRequest);
            $rsTransaccion = $ServicioRecaudacionExternaRepository->guardar($idEmpresa, $idAgencia, $idUsuario);

            if( empty($rsTransaccion[0]->transactionAuthorizerID) ) {
                DB::rollback();
                $et->infoResponse['responseCode'] = $et::$transaccionErroneaCod;
                $et->infoResponse['descResponse'] = $rsTransaccion[0]->mensaje ?? $et::$transaccionErroneaDesc;
                
                return $et;
            }

            $et->headerResponse = $data['header'];
            $et->infoResponse['descCounterpart'] = $idUsuario;
            $et->infoOperationResponse['type'] = $data['infoOperation']['type'];
            $et->infoOperationResponse['transactionDate'] = $data['infoOperation']['transactionDate'];
            $et->infoOperationResponse['businessDate'] = $data['infoOperation']['businessDate'];
            $et->infoOperationResponse['infoAditional'] = $rsTransaccion;
            $et->infoOperationResponse['infoAditional']['originalTransactionID'] = $data['infoOperation']['originalTransactionID'];

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->pago : ' . $th->getMessage());
        }

        return $et;
    }
}
