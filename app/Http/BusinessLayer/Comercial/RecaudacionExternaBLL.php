<?php

namespace App\Http\BusinessLayer\Comercial;

use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\Helpers\EstadoTransaccion;
use App\Exports\ReporteGenericoCSVExport;
use App\Exports\ReporteGenericoCSVComaExport;
use App\Imports\CSVImport;
use App\Http\Repositories\General\CiclosRepository;
use App\Http\Repositories\Comercial\InstalacionRepository;
use App\Http\Repositories\Comercial\RecaudacionExternaRepository;
use App\Http\Repositories\Comercial\ServicioRecaudacionExternaRepository;
use App\Http\Repositories\Seguridad\UsuarioRepository;

class RecaudacionExternaBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $RecaudacionExternaRepository = new RecaudacionExternaRepository([
                'opcion' => 'CMB_TIPO_RECAUDACION_EXTERNA_OUT'
            ]);
            $rsTipoRecaudacionExterna = $RecaudacionExternaRepository->listar($idEmpresa, null);

            $CiclosRepository = new CiclosRepository([
                'opcion' => 'LISTAR'
            ]);
            $rsCiclos = $CiclosRepository->listar($idEmpresa, null);
            
            $et->data = [
                'listaTipoRecaudacionExterna' => $rsTipoRecaudacionExterna,
                'listaCiclos' => $rsCiclos
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $RecaudacionExternaRepository = new RecaudacionExternaRepository($data);

            $et->data = $RecaudacionExternaRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }

    public function exportar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {            
            $RecaudacionExternaRepository = new RecaudacionExternaRepository($data);
            $rsData = $RecaudacionExternaRepository->listar($idEmpresa, $idUsuario);

            if (empty($rsData)) {
                $et->existeError = true;
                $et->mensaje = EstadoTransaccion::$noExistenDatos;
                return $et;
            }
            
            switch ($data['opcion']) {
                case 'RECAUDACION_WESTERN_UNION':
                    $export = new ReporteGenericoCSVExport($rsData);
                    $nameFile = 'recaudacion_westernunion_'.date('YmdHis').'.csv';
                    
                    if (!Excel::store($export, $nameFile)) {
                        $et->existeError = true;
                        $et->mensaje = 'ERROR AL EXPORTAR ARCHIVO EXCEL';
                        return $et;
                    }

                    break;

                case 'RECAUDACION_BANCO_PICHINCHA':
                    $nameFile = 'recaudacion_bancopichincha_'.date('YmdHis').'.txt';
                    $fp = fopen(Storage::path($nameFile), 'w');

                    foreach ($rsData as $item) {
                        fputs($fp, $item->codigoOrientacion."\t");
                        fputs($fp, $item->cuenta."\t");
                        fputs($fp, $item->moneda."\t");
                        fputs($fp, $item->monto."\t");
                        fputs($fp, $item->formaCobro."\t");
                        fputs($fp, $item->tipoCuenta."\t");
                        fputs($fp, $item->nroCuenta."\t");
                        fputs($fp, $item->referencia."\t");
                        fputs($fp, $item->tipoCliente."\t");
                        fputs($fp, $item->nroCliente."\t");
                        fputs($fp, $item->nombre);

                        fputs($fp, "\r\n");
                    }
                    
                    fclose ($fp);

                    break;
                
                case 'RECAUDACION_BANCO_GUAYAQUIL':
                    $nameFile = 'recaudacion_bancoguayaquil_'.date('YmdHis').'.txt';
                    $fp = fopen(Storage::path($nameFile), 'w');

                    $formatoCabecera = "%2s%3s%5s%-5s%2s%8s%8s%08d%015s%68s";
                    $formatoDetalle = "%2s%2s%-15s%-40s%010s%8s%010s%010s%15s%08s%4s";
                    $espacios = ' ';

                    // Cabecera
                    $tipoRegistro = '01';
                    $identificacionArchivo = 'REC';
                    $codigoBanco = '00017';
                    $codigoEmpresa = 'CSA';
                    $contenidoArchivo = '01';
                    $fechaGeneracion = date('Ymd'); //Sin separadores
                    $fechaAplicacion = date('Ymd'); //Sin separadores
                    
                    $deudaTotal = 0;
                    $nroRegistros = 0;
                    foreach ($rsData as $item){
                        $deudaTotal += floatval($item->monto);
                        $nroRegistros++;
                    }
                    $deudaTotal = number_format($deudaTotal, 2, '', ''); // Sin punto decimal

                    fputs($fp, sprintf($formatoCabecera, $tipoRegistro, $identificacionArchivo,
                                        $codigoBanco, $codigoEmpresa, $contenidoArchivo, $fechaGeneracion,
                                        $fechaAplicacion, $nroRegistros, $deudaTotal, $espacios));
                    fputs($fp, "\r\n");

                    // Detalle
                    foreach ($rsData as $item) {
                        $tipoRegistro = '02';
                        $novedad = '01';
                        $cuenta = sprintf("%010d", $item->cuenta);
                        $valorConvenio = '0';
                        $referencia = ' ';
                        $fechaPeriodo = '0';
                        $montoFormated = number_format(floatval($item->monto), 2, '', ''); // Sin punto decimal
                        $montoMinimoFormated = number_format(floatval($item->montoMinimo), 2, '', ''); // Sin punto decimal

                        fputs($fp, utf8_encode(sprintf($formatoDetalle, $tipoRegistro, $novedad, $cuenta, 
                                                        utf8_decode($item->nombre), $montoFormated, 
                                                        $item->vencimiento, $montoMinimoFormated, 
                                                        $valorConvenio, $referencia, $fechaPeriodo, $espacios)));
                        fputs($fp, "\r\n");
                    }
                    
                    fclose ($fp);

                    break;

                case 'RECAUDACION_BANRED':
                    $export = new ReporteGenericoCSVComaExport($rsData);
                    $nameFile = 'recaudacion_banred_'.date('YmdHis').'.csv';
                    
                    if (!Excel::store($export, $nameFile)) {
                        $et->existeError = true;
                        $et->mensaje = 'ERROR AL EXPORTAR ARCHIVO CSV';
                        return $et;
                    }

                    break;
            }
            
            $et->data = [
                'nameFile' => $nameFile
            ];

        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->exportar : ' . $th->getMessage());
        }

        return $et;
    }

    public function importar($idEmpresa, $idAgencia, $idUsuario, $request)
    {
        $et = new EstadoTransaccion();

        try {
            $data = json_decode(json_encode($request->all()), true);
            $dataItems = Excel::toArray(new CSVImport, $request->file('archivoRecaudaciones'));

            DB::beginTransaction();

            // SE ELIMINA PRIMER ROW QUE ES EL ROW DE CABECERA EN EL EXCEL, EN PRIMERA SHEET
            array_shift($dataItems[0]);
            if ( count($dataItems[0]) > 0 ) {
                if ($data['opcion'] == 'RECAUDACION_BANRED') {
                    // OBTENER ID_USUARIO DE USUARIO BANRED
                    $UsuarioRepository = new UsuarioRepository([
                        'opcion' => 'BUSCAR_USUARIO',
                        'usuario' => 'BANRED'
                    ]);
                    $rsUsuario = $UsuarioRepository->listar($idEmpresa, $idUsuario);

                    // INGRESO DE PAGOS
                    foreach($dataItems[0] as $row)
                    {
                        // SE VERIFICA INFORMACION DE FACTURA DE LA INSTALACION
                        $InstalacionRepository = new InstalacionRepository([
                            'opcion'  => 'FACTURA_VIGENTE_INSTALACION',
                            'numeroCuenta' => $row[7]
                        ]);
                        $rsInstalacion = $InstalacionRepository->listar($idEmpresa, $idUsuario);
                        if (empty($rsInstalacion)) {
                            DB::rollBack();
                            $et->existeError = true;
                            $et->mensaje = 'NUMERO DE CUENTA NO ENCONTRADO [COLUMNA = H; VALOR = '.$row[7].']';
                            return $et;
                        }

                        // INGRESO DE PAGO
                        $RecaudacionExternaRepository = new RecaudacionExternaRepository([
                            'opcion' => 'EJECUTAR_PAGO_FACTURA',
                            'numeroFactura' => $rsInstalacion[0]->numeroFactura,
                            'valorRecaudacion' => $row[9]
                        ]);
                        $rsPago = $RecaudacionExternaRepository->guardar($idEmpresa, $idAgencia, $rsUsuario[0]->idUsuario);
                        
                        if( empty($rsPago[0]->id) || $rsPago[0]->respuesta == 0 ) {
                            DB::rollback();
                            $et->existeError = true;
                            $et->mensaje = !empty($rsPago[0]->mensaje) ? $rsPago[0]->mensaje : 'ERROR DE INGRESO DE PAGO';
                            
                            return $et;
                        }
            
                        $dataRequest = [
                            'opcion' => 'BANRED_GUARDAR_TRANSACCION_PAGO',
                            'idRecaudacion' => $rsPago[0]->id,
                            'idTransaccionEntidad' => $rsInstalacion[0]->numeroFactura.$rsPago[0]->id,
                            'tipoOperacionEntidad' => 'PAGO',
                            'metodoPagoEntidad' => 'EFECTIVO',
                        ];
                        $ServicioRecaudacionExternaRepository = new ServicioRecaudacionExternaRepository($dataRequest);
                        $ServicioRecaudacionExternaRepository->guardar($idEmpresa, $idAgencia, $idUsuario);
                    }
                }
            }

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->importar : ' . $th->getMessage());
        }

        return $et;
    }
}
