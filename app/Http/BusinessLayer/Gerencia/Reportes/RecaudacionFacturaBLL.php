<?php

namespace App\Http\BusinessLayer\Gerencia\Reportes;

use Illuminate\Support\Facades\DB;
use App\Helpers\EstadoTransaccion;
use App\Http\Repositories\Comercial\FacturaCabRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ReporteGenericoExport;

class RecaudacionFacturaBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $FacturaCabRepository = new FacturaCabRepository([
                'opcion' => 'CMB_TIPO_RPT_RECAUDACION_FACTURA'
            ]);
            $rsTipoReporteRecaudacion = $FacturaCabRepository->listar($idEmpresa, null);

            $et->data = [
                'listaTipoReporteRecaudacion' => $rsTipoReporteRecaudacion,
                'tipoReporteRecaudacionDefault' => 'RPT_FACTURAS_RECAUDACION'
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $FacturaCabRepository = new FacturaCabRepository($data);

            $et->data = $FacturaCabRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }
    
    public function exportar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {            
            $FacturaCabRepository = new FacturaCabRepository($data);
            $rsData = $FacturaCabRepository->listar($idEmpresa, $idUsuario);

            if (empty($rsData)) {
                $et->existeError = true;
                $et->mensaje = EstadoTransaccion::$noExistenDatos;
                return $et;
            }
            
            $export = new ReporteGenericoExport($rsData);

            $nameFile = 'recaudacion_'.date('Y-m-d His').'.xlsx';
            if (!Excel::store($export, $nameFile)) {
                $et->existeError = true;
                $et->mensaje = 'ERROR AL EXPORTAR ARCHIVO EXCEL';
                return $et;
            }
            $et->data = [
                'nameFile' => $nameFile
            ];

        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->exportar : ' . $th->getMessage());
        }

        return $et;
    }
}
