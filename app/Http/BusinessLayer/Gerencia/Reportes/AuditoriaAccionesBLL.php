<?php

namespace App\Http\BusinessLayer\Gerencia\Reportes;

use Illuminate\Support\Facades\DB;
use App\Helpers\EstadoTransaccion;
use App\Http\Repositories\Gerencia\Reportes\AuditoriaAccionesRepository;
use App\Http\Repositories\Seguridad\UsuarioRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ReporteGenericoExport;

class AuditoriaAccionesBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $UsuarioRepository = new UsuarioRepository([
                'opcion' => 'CMB_USUARIOS'
            ]);
            $rsListaUsuarios = $UsuarioRepository->listar($idEmpresa, null);

            $et->data = [
                'listaUsuarios' => $rsListaUsuarios
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $AuditoriaAccionesRepository = new AuditoriaAccionesRepository($data);

            $et->data = $AuditoriaAccionesRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }
    
    public function exportar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $AuditoriaAccionesRepository = new AuditoriaAccionesRepository($data);
            $rsData = $AuditoriaAccionesRepository->listar($idEmpresa, $idUsuario);

            if (empty($rsData)) {
                $et->existeError = true;
                $et->mensaje = EstadoTransaccion::$noExistenDatos;
                return $et;
            }

            $export = new ReporteGenericoExport($rsData);

            $nameFile = 'auditoria_'.date('Y-m-d His').'.xlsx';
            if (!Excel::store($export, $nameFile)) {
                $et->existeError = true;
                $et->mensaje = 'ERROR AL EXPORTAR ARCHIVO EXCEL';
                return $et;
            }
            $et->data = [
                'nameFile' => $nameFile
            ];

        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->exportar : ' . $th->getMessage());
        }

        return $et;
    }
}
