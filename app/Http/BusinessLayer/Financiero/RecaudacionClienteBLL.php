<?php

namespace App\Http\BusinessLayer\Financiero;

use Illuminate\Support\Facades\DB;
use App\Helpers\EstadoTransaccion;
use App\Http\Repositories\Financiero\RecaudacionClienteRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ReporteGenericoExport;

class RecaudacionClienteBLL
{
    public function index($idEmpresa)
    {
        $et = new EstadoTransaccion();

        try {
            $RecaudacionClienteRepository = new RecaudacionClienteRepository([
                'opcion' => 'CMB_TIPO_REPORTE_RECAUDACION'
            ]);
            $rsTipoReporteRecaudacion = $RecaudacionClienteRepository->listar($idEmpresa, null);

            $et->data = [
                'listaTipoReporteRecaudacion' => $rsTipoReporteRecaudacion,
                'tipoReporteRecaudacionDefault' => 'RPT_RECAUDACION_CLIENTES_POR_SECTOR'
            ];
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->index : ' . $th->getMessage());
        }

        return $et;
    }

    public function listar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            $RecaudacionClienteRepository = new RecaudacionClienteRepository($data);

            $et->data = $RecaudacionClienteRepository->listar($idEmpresa, $idUsuario);
        } catch (\Throwable $th) {
            throw new \Exception(' : ' . get_class($this) . '->listar : ' . $th->getMessage());
        }

        return $et;
    }

    public function guardar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {
            DB::beginTransaction();

            $RecaudacionClienteRepository = new RecaudacionClienteRepository($data);
            $et->data = $RecaudacionClienteRepository->guardar($idEmpresa, $idUsuario);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->guardar : ' . $th->getMessage());
        }

        return $et;
    }
    
    public function exportar($idEmpresa, $idUsuario, $data)
    {
        $et = new EstadoTransaccion();

        try {            
            $RecaudacionClienteRepository = new RecaudacionClienteRepository($data);
            $rsData = $RecaudacionClienteRepository->listar($idEmpresa, $idUsuario);

            if (empty($rsData)) {
                $et->existeError = true;
                $et->mensaje = EstadoTransaccion::$noExistenDatos;
                return $et;
            }
            
            $export = new ReporteGenericoExport($rsData);

            $nameFile = 'recaudacion_'.date('Y-m-d His').'.xlsx';
            if (!Excel::store($export, $nameFile)) {
                $et->existeError = true;
                $et->mensaje = 'ERROR AL EXPORTAR ARCHIVO EXCEL';
                return $et;
            }
            $et->data = [
                'nameFile' => $nameFile
            ];

        } catch (\Throwable $th) {
            DB::rollback();
            throw new \Exception(' : ' . get_class($this) . '->exportar : ' . $th->getMessage());
        }

        return $et;
    }
}
