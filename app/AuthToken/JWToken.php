<?php

namespace App\AuthToken;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Parser;
use Illuminate\Support\Facades\Cache;

class JWToken
{
	private $key = 'UsCzNQqTQIVXSBlRzbWoPGFnipURU4Fm';    // key para el firmado del token
    private $minutosValidez = 1440;                         // tiempo en minutos de validez del token
    private $emisorToken = 'http://www.emapadaule.gob.ec/';

    public function destruirToken($token, $idUsuario, $tiempo = 60)
    {
        Cache::put($token, $idUsuario, $tiempo);
    }

    public function generarToken($idEmpresa, $idUsuario, $idAgencia)
    {
		$signer = new Sha256();
        $builderToken = new Builder();

        $time = time();

        $token = $builderToken->setIssuer($this->emisorToken) // Configura el emisor (iss claim)
            ->setAudience($idUsuario) // Configura el receptor (aud claim)
            ->setId(md5($idUsuario . $time)) // Configura el id (jti claim), replica como item del header, ID unico del token. md5(cod_usuario + iat claim)
            ->setIssuedAt($time) // Configura el datetime en que el token fue emitido (iat claim)
            ->setNotBefore($time) // Configura el datetime en que el token puede ser usado (nbf claim)
            ->setExpiration($time + $this->minutosValidez*60) // Configura el datetime de expiracion del token (exp claim)
            ->set('data', ['idEmpresa' => $idEmpresa, 'idUsuario' => $idUsuario, 'idAgencia' => $idAgencia]) // Configura un nuevo claim, llamado "data", recordar que esto no va cifrado, ser cuidadoso con lo que se envía
            ->sign($signer, $this->key) // crea una firma usando $key como llave
            ->getToken(); // Pide el token generado

        $token->getHeaders(); // Obtiene el header del token
        $token->getClaims(); // Obtiene el claim del token
        return (string)$token;
	}

    static function informacionToken($request)
    {
        $parser = new Parser();

        $strToken = $request->header('Authorization');

        $token = $parser->parse($strToken); // Parsea desde una string

        $token->getHeaders(); // Obtiene todo el header del token
        $token->getClaims(); // Obtiene todos los claims del token
            
        $userData = $token->getClaim('data');

        return $userData;
    }
    
    static function getDateExpirationToken($strToken)
    {
        $parser = new Parser();

        $token = $parser->parse($strToken); // Parsea desde una string

        $token->getClaims(); // Obtiene todos los claims del token

        return $token->getClaim('exp');
    }

    public function validarToken($token)
    {
        $informacion = [];

        $parser = new Parser();
        $signer = new Sha256();
        $data = new ValidationData(); // Valida basado en la fecha y hora actual (iat, nbf and exp)
        
        try {
            $strToken = (string)$token;

            if (Cache::has($strToken)) {
                return $informacion['valido'] = false;
            }

            $token = $parser->parse($strToken); // Parsea desde una string
            $token->getHeaders(); // Obtiene todo el header del token
            $token->getClaims(); // Obtiene todos los claims del token
            
            $userData = $token->getClaim('data');
            $iatData = $token->getClaim('iat');

            $data->setIssuer($this->emisorToken);
            $data->setAudience($userData->idUsuario);
            $data->setId(md5($userData->idUsuario . $iatData));

            if ($token->validate($data) and $token->verify($signer, $this->key)){ // Si token es válido
                $informacion['valido'] = true;
                $informacion['userInfo'] = $token->getClaim('data');
            } else // Token no es válido
                $informacion['valido'] = false;
        }
        catch (\Exception $e){
            $informacion['valido'] = false;
        }
        return $informacion;
    }

    public function cifrarPassword($usrName, $usrPass)
	{
		try {
			// $cod_clave       = hash('sha256', $usrPass);
			// $cod_clave       = substr($cod_clave, 3, 40) . substr($cod_clave, 11, 30);
			// $cod_user        = hash('sha256', $usrName);
			// $cod_clave_final = $cod_clave . $cod_user;
            // $usrPass         = hash('sha256', $cod_clave_final);
            
            // Por ahora con md5
            $usrPass = md5($usrPass);
        } catch (\Throwable $th) {
			// $this->guardarErrorMongo($data, 'cifrarPassword', $th);
        }
        
		return $usrPass;
	}
}