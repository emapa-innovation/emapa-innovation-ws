<?php
// arreglo de opciones en formato legible para vuejs list
if(! function_exists('getMenuOpciones'))
{
    function getMenuOpciones(array $arrayOpciones)
    {
        $opcionesCollection = collect($arrayOpciones);
			
        $modulos = $opcionesCollection->whereNull('idOpcionPadre');
        
        $arrayMenu = [];
        foreach ($modulos->all() as $modulo) {
            $arrayModulo = (array)$modulo;
            
            // GET CHILDS TO OPTION
            $submodulos = $opcionesCollection->where('idOpcionPadre', $modulo->idOpcion);
            
            foreach ($submodulos->all() as $submodulo) {
                $arraySubmodulo = (array)$submodulo;
                
                // GET CHILDS TO OPTION
                $opcionesSubmodulo = $opcionesCollection->where('idOpcionPadre', $submodulo->idOpcion);
                foreach ($opcionesSubmodulo->all() as $opcion) {
                    $arraySubmodulo['children'][] = $opcion;
                }

                $arrayModulo['children'][] = $arraySubmodulo;
            }
            $arrayMenu[] = $arrayModulo;
        }

        return $arrayMenu;
    }
}

// Obtener un arreglo con los nombres de atributos que usará el Validator
// Permite que en el mensaje de validacion no se escapen los nombres de variables por CamelCase o SnakeCase
if(! function_exists('getAttributesValidation'))
{
    function getAttributesValidation(array $reglas)
    {
        $attributes = [];
        foreach (array_filter($reglas) as $key => $value) {
            $attributes[$key] = $key;
        }
        return $attributes;
    }
}

if(! function_exists('getId'))
{
    function getId(array $array)
    {
        return $array[0]->id;
    }
}

if(! function_exists('getRespuesta'))
{
    function getRespuesta(array $array)
    {
        return $array[0]->respuesta;
    }
}
if(! function_exists('getMensaje'))
{
    function getMensaje(array $array)
    {
        return $array[0]->mensaje;
    }
}

/**
 * Permite convertir en array los parametros recibidos por URL,
 * la cadena recibida debe ser del tipo:
 * clave_uno=valor_uno/clave_dos=valor_dos
 */

if(! function_exists('getParametros'))
{
    function getParametros($cadena)
    {
        try {
            $parametros = [];
            $array = explode('/', $cadena);
            foreach ($array as $value) {
                $tmp = explode('=', $value);
                $tmp[1] = trim($tmp[1]);
                if( empty($tmp[1]) ) {
                    $tmp[1] = NULL;
                }
                $parametros[$tmp[0]] = $tmp[1];
            }
        } catch (\Exception $e) {
            throw new \Exception(' : helper - getParametros ' . $e->getMessage());
        }
        return $parametros;
    }
}

/**
 * Busca dentro de un array el elemento enviado
 * @param array lista en la que se buscará
 * @param valor valor a buscar
 * @param indice en el que se buscará
 * @param indice que se devolverá
 */
if(! function_exists('obtenerValorDeArray'))
{
    function obtenerValorDeArray($array,$valor,$indiceBusqueda,$indiceDevuelve)
    {
        try{
            $resp=null;
                foreach ($array as $value) {
                #dd($value['cod_tipo']);
                   if(strtoupper($value[$indiceBusqueda])==strtoupper($valor)){
                         $resp = $value[$indiceDevuelve];
                        break;
                   }
                }
            return empty($resp)?1:(int)$resp;
        }catch(\Exception $e){
            throw new \Exception(' : helper - obtenerValorDeArray ' . $e->getMessage());
        }
       
    }
}

/**
 * Devuelve un array con el formato para los select
 * @param array lista de la cual se extraerá informacion
 * @param indiceValor índice del cual se extraerá información para el label 
 * @param indiceDescripcion índice del cual se extraerá información para el value 
 */

if(! function_exists('dataParaSelect'))
{
    function dataParaSelect($array,$indiceValor,$indiceDescripcion){
        if(count($array)>0){
            if(is_object($array[0])){
                $array = json_decode(json_encode($array),true);
            } 
        }
        $listaSelect = array();
        try{
            foreach ($array as $value) {
                $listaSelect[] = ['valor' => $value[$indiceValor],'descripcion'=>$value[$indiceDescripcion]];
            }
            return $listaSelect;
        }catch(\Exception $e){
            throw new \Exception(' : helper - dataParaSelect ' . $e->getMessage());
        }
    }
}


if(! function_exists('objetoParaComponentesVue'))
{
    function objetoParaComponentesVue($objeto, $indiceValor, $indiceDescripcion){
        try{
            $lista = [];
            if(is_object($objeto)) {
                $objeto = json_decode(json_encode($objeto), true);
            }
            foreach ($objeto as $value) {
                $lista[] = ['valor' => $value[$indiceValor],'descripcion'=>$value[$indiceDescripcion]];
            }
        } catch(\Exception $e) {
            throw new \Exception(' : helper - objetoParaComponentesVue ' . $e->getMessage());
        }
        return $lista;
    }
}
/**
 * Agrupa el array por una key dada
 *
 * Agrupa un array en arrays por una key dada o un conjunto de keys que compartan todos los arrays miembros
 *
 * Basado en {@author Jake Zatecky} {@link https://github.com/jakezatecky/arrayGroupBy arrayGroupBy()}.
 *
 * Esta variante permite que $key sea un closure
 *
 * @param array $array   El array sobre el cual se ejecutara el agrupamiento
 * @param mixed $key,... La key para agrupar o separara. Puede ser de tipo:
 *                       _string_, _integer_, _float_, _callable_.
 *
 *                       Si la key es un callback, debe devolver una key válida desde el array
 *
 *                       Si la key es _NULL_, se salta el elemento iterado
 *
 *                       ```
 *                       string|int callback ( mixed $item )
 *                       ```
 *
 * @return array|null Devuelve un array multidimensional o 'null' si '$key' no es válido
 */
if (!function_exists('arrayGroupBy')) {
    function arrayGroupBy(array $array, $key)
    {
        if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
            trigger_error('arrayGroupBy(): The key should be a string, an integer, or a callback', E_USER_ERROR);
            return null;
        }
        $func = (!is_string($key) && is_callable($key) ? $key : null);
        $_key = $key;
        // Load the new array, splitting by the target key
        $grouped = [];
        foreach ($array as $value) {
            $key = null;
            if (is_callable($func)) {
                $key = call_user_func($func, $value);
            } elseif (is_object($value) && isset($value->{$_key})) {
                $key = $value->{$_key};
            } elseif (isset($value[$_key])) {
                $key = $value[$_key];
            }
            if ($key === null) {
                continue;
            }
            $grouped[$key][] = $value;
        }
        // Construir de forma recursiva un agrupamiento anidado si se proveen más parámetros
        // Cada valor de array agrupado es agrupado acorde a la siguiente key secuencial
        if (func_num_args() > 2) {
            $args = func_get_args();
            foreach ($grouped as $key => $value) {
                $params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
                $grouped[$key] = call_user_func_array('arrayGroupBy', $params);
            }
        }
        return $grouped;
    }
}