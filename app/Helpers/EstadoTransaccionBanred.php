<?php

namespace App\Helpers;

class EstadoTransaccionBanred
{
    public $headerResponse = [
        'infoTouchPoint' => [
            'channelID'     => null,
            'terminalID'    => null,
            'institutionID' => null,
            'transactionID' => null,
            'subService'    => null
        ],
        'infoPerson' => [
            'documentType'  => null,
            'counterpart'   => null
        ]
    ];
    public $infoResponse = [
        'responseCode'      => '00',
        'descResponse'      => null,
        'descCounterpart'   => null,
        'status'            => 'OK|APROBADO|RECHAZADO'
    ];
    public $infoOperationResponse = [
        'type'              => null,
        'transactionDate'   => null,
        'businessDate'      => null,
        'paymentDueDate'    => null,
        'infoAditional' => [
            'typeInformation'           => null,
            'reference'                 => null,
            'amount'                    => null,
            'description'               => null,
            'originalTransactionID'     => null,
            'transactionAuthorizerID'   => null
        ]
    ];
    public static $noExistenDatos       = "No existen datos con el criterio seleccionado";
    public static $procesoExitoso       = "Proceso ejecutado exitosamente";
    public static $procesoErroneo       = "Hubo un error, comuníquese con su administrador de sistemas";
    public static $operacionNoPermitida = 'Operación no permitida';
    
    public static $transaccionExitosaCod        = '00';
    public static $transaccionExitosaDesc       = 'TRANSACCION EXITOSA';
    public static $contrapartidaIncorrectaCod   = '01';
    public static $contrapartidaIncorrectaDesc  = 'CONTRAPARTIDA NO VALIDA';
    public static $datosIncompletosCod          = '02';
    public static $datosIncompletosDesc         = 'DATOS INCOMPLETOS';
    public static $sinValoresPendientesCod      = '03';
    public static $sinValoresPendientesDesc     = 'SIN VALORES PENDIENTES DE PAGO';
    public static $datosIncorrectosCod          = '04';
    public static $datosIncorrectosDesc         = 'DATOS INCORRECTOS';
    public static $transaccionErroneaCod        = '07';
    public static $transaccionErroneaDesc       = 'TRANSACCION NO PROCESADA';
    public static $errorTecnicoCod              = '12';
    public static $errorTecnicoDesc             = 'ERROR TECNICO';
}
