<?php

namespace App\Helpers;

class EstadoTransaccionRecaudacion
{
    public $header = [
        'dataEntidad' => [
            'idCanal'     => null,
            'idTerminal'    => null,
            'idInstitucion' => null,
            'idTransaccion' => null
        ]
    ];
    public $body = [
        'dataResponse' => [
            'codigo'      => '00',
            'descripcion' => null
        ],
        'dataOperacion' => [
            'tipoTransaccion'   => null,
            'fechaTransaccion'  => null,
            'dataAdicional' => []
        ]
    ];
    public static $noExistenDatos       = "NO EXISTEN DATOS CON EL CRITERIO SELECCIONADO";
    public static $procesoExitoso       = "Proceso ejecutado exitosamente";
    public static $procesoErroneo       = "Hubo un error, comuníquese con su administrador de sistemas";
    public static $operacionNoPermitida = 'Operación no permitida';
    
    public static $transaccionExitosaCod        = '00';
    public static $transaccionExitosaDesc       = 'TRANSACCION EXITOSA';
    public static $contrapartidaIncorrectaCod   = '01';
    public static $contrapartidaIncorrectaDesc  = 'CONTRAPARTIDA NO VALIDA';
    public static $datosIncompletosCod          = '02';
    public static $datosIncompletosDesc         = 'DATOS INCOMPLETOS';
    public static $sinValoresPendientesCod      = '03';
    public static $sinValoresPendientesDesc     = 'SIN VALORES PENDIENTES DE PAGO';
    public static $datosIncorrectosCod          = '04';
    public static $datosIncorrectosDesc         = 'DATOS INCORRECTOS';
    public static $transaccionErroneaCod        = '07';
    public static $transaccionErroneaDesc       = 'TRANSACCION NO PROCESADA';
    public static $errorTecnicoCod              = '12';
    public static $errorTecnicoDesc             = 'ERROR TECNICO';
}
