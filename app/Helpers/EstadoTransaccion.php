<?php

namespace App\Helpers;

class EstadoTransaccion
{
    public $existeError                 = false;
    public $mensaje                     = "";
    public $data                        = null;
    public static $noExistenDatos       = "No existen datos con el criterio seleccionado";
    public static $procesoExitoso       = "Proceso ejecutado exitosamente";
    public static $procesoErroneo       = "Hubo un error, comuníquese con su administrador de sistemas";
    public static $operacionNoPermitida = 'Operación no permitida';
}
