<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ReporteCarteraDetalladaExport implements FromView, ShouldAutoSize
{
    protected $rows, $items;

    public function __construct(array $rows, array $items)
    {
        $this->rows = $rows;
        $this->items = $items;
    }

    public function view(): View
    {
        return view('Cartera.exports.detallada', [
            'rows' => $this->rows,
            'items' => $this->items
        ]);
    }
}
