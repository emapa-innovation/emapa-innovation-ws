<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Planilla de Consumo</title>

    <style type="text/css">
        p, h1, h2, h3, h4, h5, h6, table { 
            font-size    : 12px;
            font-family  : Arial, Helvetica, sans-serif;
        }
        .table-borderless table, 
        .table-borderless td, 
        .table-borderless th {
            border: 1px solid black;
            padding: 2px;
        }
    </style>
</head>
<body onload="init()">
    <table style="width:100%;">
        <tr>
            <td><img src="{{URL::asset('/image/emapa-logo.png')}}" width="110px"></td>
            <td style="width: 520px; text-align: center; font-size: 11px;">
                <b>Empresa Pública Municipal de Agua Potable y Alcantarillado de Daule EMAPA EP
                <br>
                Centro Comercial Yanco, locales 12 y 13, Av. Vicente Piedrahita #442 y Guayaquil</b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="width:100%; font-size: 11px; padding-top: 30px;">
                    <tr>
                        <td style="width: 40%;">
                            <b>Nombre: </b>{{ $data->cliente }}
                        </td>
                        <td style="width: 35%;">
                            <b>Cuenta: </b>{{ $data->cuenta }}
                        </td>
                        <td style="width: 25%;">
                            <b>Ciclo: </b>{{ $data->ciclo }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Cédula: </b>{{ $data->identificacion }}
                        </td>
                        <td>
                            <b>Medidor: </b>{{ $data->medidor }}
                        </td>
                        <td>
                            <b>Periodo Facturación: </b>{{ $data->periodo }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Teléfono: </b>{{ $data->telefono }}
                        </td>
                        <td>
                            <b>Clave Catastral: </b>{{ $data->claveCatastral }}
                        </td>
                        <td>
                            <b>Estado de Cuenta: </b>{{ $data->estadoCuenta }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <b>Dirección: </b>{{ $data->direccion }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table class="table-borderless" style="width:100%; padding-top: 30px; border-collapse: collapse;">
                    <tr style="background-color: steelblue;">
                        <th colspan="4"><font color="#ffffff">INFORMACIÓN DE FACTURA VIGENTE</font></th>
                    </tr>
                    <tr>
                        <td>
                            <b>Fecha de Factura</b>
                        </td>
                        <td>
                            {{ $data->fechaFactura }}
                        </td>
                        <td>
                            <b>Fecha de Vencimiento</b>
                        </td>
                        <td>
                            {{ $data->fechaVencimiento }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Fecha de Lectura Actual</b>
                        </td>
                        <td>
                            {{ $data->fechaLecturaActual }}
                        </td>
                        <td>
                            <b>Fecha y Valor de Último Pago</b>
                        </td>
                        <td>
                            {{ $data->fechaValorUltimoPago }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Consumo</b>
                        </td>
                        <td>
                            {{ $data->consumo }} m3
                        </td>
                        <td>
                            <b>Meses de Atraso</b>
                        </td>
                        <td>
                            {{ $data->mesesAtraso }}
                        </td>
                    </tr>
                    <tr style="background-color: steelblue;">
                        <th colspan="4"><font color="#ffffff">VALORES DE FACTURA</font></th>
                    </tr>
                    <tr>
                        <td colspan="3" style="background-color: lightblue;">
                            <b>Saldo Atrasado</b>
                        </td>
                        <td style="text-align: right;">
                            {{ $data->saldoAtrasado }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="background-color: lightblue;">
                            <b>Agua</b>
                        </td>
                        <td style="text-align: right;">
                            {{ $data->valorAgua }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="background-color: lightblue;">
                            <b>Alcantarillado</b>
                        </td>
                        <td style="text-align: right;">
                            {{ $data->valorAlcantarillado }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="background-color: lightblue;">
                            <b>Cargo Fijo</b>
                        </td>
                        <td style="text-align: right;">
                            {{ $data->valorCargoFijo }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <b>Subtotal</b>
                        </td>
                        <td style="text-align: right;">
                            {{ $data->valorSubtotal }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <b>I.V.A.</b>
                        </td>
                        <td style="text-align: right;">
                            {{ $data->valorIVA }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <b>Total</b>
                        </td>
                        <td style="text-align: right;">
                            {{ $data->valorTotal }}
                        </td>
                    </tr>
                    <tr style="background-color: #FF9966;">
                        <td colspan="3">
                            <b>Pago Realizado</b>
                        </td>
                        <td style="text-align: right;">
                            - {{ $data->pagado }}
                        </td>
                    </tr>
                    <tr style="background-color: #FACD5E;">
                        <td colspan="3">
                            <b>Saldo por pagar</b>
                        </td>
                        <td style="text-align: right;">
                            {{ $data->saldoXPagar }}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="chart_div" style="padding-top: 50px;">
                    <img src="{{ $chartConsumosBase64 }}" width="100%">
                </div>
            </td>
        </tr>
    </table>
</body>
</html>
