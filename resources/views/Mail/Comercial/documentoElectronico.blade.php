<table width="754" border="0">
	<h1 align="center" color="blue">DOCUMENTOS ELECTRÓNICOS</h1>
	<h3 align="center"><strong>{{ $nombreEmpresa }}</strong></h3>
    <tr>
        <td colspan="2"><p>Estimado(a):<br>
            <strong>{{ $razonSocialCliente }}</strong> </p>
        <p>Continuando con el compromiso de protección del medio ambiente y mantener un servicio de calidad, 
            adjuntamos su {{ $tipoComprobante }} ELECTRONICA No {{ $secuencialComprobante }}</p>
        <p><strong>Detalle resumen de Comprobante:</strong></p></td>
    </tr>
    <tr>
        <td colspan="2" align="center"><p><strong>No. de {{ $tipoComprobante }}:</strong> {{ $secuencialComprobante }}</p>
        <p><strong>Fecha de emisión:</strong> {{ $fechaEmisionComprobante }}</p>
        <p><strong>Valor del documento:</strong> {{ $valorComprobante }}</p>
        </td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2" align="center"><font style="font-size: 8pt; font-family: Tahoma" color="#000000">{{ $direccionMatrizEmpresa }}</font></td>
    </tr>
    <tr>
        <td colspan="2" align="center"><font style="font-size: 8pt; font-family: Tahoma" color="#000000">{{ $ciudadEmpresa }} - Ecuador</font></td>
    </tr>
</table>