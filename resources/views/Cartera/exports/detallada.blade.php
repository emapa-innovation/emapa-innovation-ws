@php
$arrayTotalItems = [];
$firstRowArray = isset($rows[0]) ? $rows[0] : [];
@endphp
<table>
    <thead>
    <tr>
    @foreach(array_keys((array)$firstRowArray) as $row)
        @php
            if ($row == 'serial_caf') {
                continue;
            }
        @endphp
        <th><b>{{ $row }}</b></th>
    @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($rows as $row)
        <tr>
        @foreach($row as $key => $field)
            @php
                if ($key == 'serial_caf') {
                    continue;
                }
                if ( array_search($key, array_column((array)$items, 'codigo')) !== false ) {
                    if (isset($arrayTotalItems[$key])) {
                        $arrayTotalItems[$key] += $field;
                    } else {
                        $arrayTotalItems[$key] = $field;
                    }
                }
            @endphp
            <td>{{ $field }}</td>
        @endforeach
        </tr>
    @endforeach
        <tr>
        @foreach(array_keys((array)$firstRowArray) as $row)
            @php
                if ($row == 'serial_caf') {
                    continue;
                }
            @endphp
            @if( array_search($row, array_column((array)$items, 'codigo')) !== false )
                @if($arrayTotalItems[$row] < 0)    
                    <td style="color: red;">
                @else
                    <td>
                @endif
                    <b>{{ $arrayTotalItems[$row] }}</b></td>
            @else
                <td>&nbsp;</td>
            @endif
        @endforeach
        </tr>
    </tbody>
</table>